package BESSEMER

public Object BessemerGlobals inherits KERNEL::Globals

	public		Object	AccountPermissions = BESSEMER::PERMISSIONS::NODE::AccountPermissions
	public		Object	AddHeaderSheet = BESSEMER::FILEHANDLER::AddHeaderSheet
	public		Object	AddRegularFile = BESSEMER::FILEHANDLER::AddRegularFile
	public		Object	AttributesConfig = BESSEMER::AttributesConfig
	public		Object	CategoryAttributes = BESSEMER::CategoryAttributes
	public		Object	ContactPermissions = BESSEMER::PERMISSIONS::NODE::ContactPermissions
	public		Object	DocPermissions = BESSEMER::PERMISSIONS::DocPermissions
	public		Object	FamilyPermissions = BESSEMER::PERMISSIONS::NODE::FamilyPermissions
	public		Object	NodePermissions = BESSEMER::PERMISSIONS::NODE::NodePermissions
	public		Object	PermissionsConfig = BESSEMER::PermissionsConfig
	public		Object	RelationshipPermissions = BESSEMER::PERMISSIONS::NODE::RelationshipPermissions
	public		Object	RequestHandlerGroup = BESSEMER::bessemerRequestHandlerGroup
	public		Object	SubAccountPermissions = BESSEMER::PERMISSIONS::NODE::SubAccountPermissions
	public		Dynamic	TimeLogFile
	public		Object	Timer = BESSEMER::Timer
	public		Object	UpdateFileHandler = BESSEMER::FILEHANDLER::UpdateFileHandler
	public		Object	Utils = BESSEMER::Utils

end
