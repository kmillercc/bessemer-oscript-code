package BESSEMER::WebnodeCmds

public Object RequestVaultDocument inherits BESSEMER::WebnodeCmds::WebnodeCmds

	override	Boolean	fEnabled = TRUE
	override	Boolean	fIncludeNextURL = TRUE
	override	String	fName = 'Request Vault Document'
	override	String	fQueryString = 'func=PhysicalObjects.SingleRequest&objID=%1'



	override function List _Nodetypes()

							return	{ \
										144 \
									}
						end


	override function String _QueryString(\
								Record	args, \	//	RequestHandler.fArgs.
								Dynamic	node )	//	WebNodes view row.

							if ( RecArray.IsColumn( args, 'prgCtx' ) )	

								Object prgCtx = args.prgCtx

								RecArray XRefPONode = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										"select XDataID from RM_DocXRef where DataID = :A1", \
										node.DataID \
									)

								if XRefPONode and length (XRefPONode) > 0
									return Str.Format ("func=PhysicalObjects.SingleRequest&objID=%1", XRefPONode[1][1])
								end
							end

							// best we can do is send the user to the po overview

							return Str.Format ("func=ll&objid=%1&objaction=PhysItemCopies", node.DataID)


						end


	override function Boolean _SubclassIsEnabled(\
							Object	prgCtx,\				//	Program session.
							Dynamic	node,\					//	WebNodes view row.
							Dynamic	parent = Undefined )	//	WebNodes view row.

							// does the node have an xref to a PONode?

							RecArray XRefPONode = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select XDataID from RM_DocXRef where DataID = :A1", \
									node.DataID \
								)

							return prgCtx.fDBConnect.CheckError (XRefPONode).OK and length (XRefPONode) > 0;

						end

end
