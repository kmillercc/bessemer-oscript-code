package BESSEMER

public Object Utils inherits BESSEMER::Root

	public function Assoc DAPIGetNode(Object prgCtx, DAPINODE parentNode, String path)

		// bullet-proofing just in case calling function passes in an empty path
		if !Length(Str.Trim(path))
			return Assoc{'ok': true, 'result': parentNode}			
		end

		// KM 11-26-18 CS16 caches these nodes based on path and sometimes returns the wrong node.
    	DAPINODE node = DAPI.GetNode(path, parentNode)
		if IsError(node)
			return .Problem(Str.Format("Could not get node with path with PARENTID %2", path, parentNode.pID))
		end

    	// the name equals the last element in path
    	String name = Str.Elements(path, ":")[-1]

		if (node.pParentID != parentNode.pID || node.pName != name)

			// logic here is that if the node isn't in the parent or the name doesn't match,
			// then it warrants further investigation. Note: the caller could have passed a path instead of just a name,
			// in which case the parent could actually be a grandparent, which would trigger this check. That's probably a good thing
			// name mismatch should always trigger the check
			Assoc status = ._GetNodeFromSql(prgCtx, parentNode, path)
			if !status.ok
				return .Problem(Str.Format("Could not get node in parent %1 with path %2", parentNode.pID, path))
			end	 

			node = status.result
		end	

		return Assoc{'ok': true, 'result': node}

    end


	public function RecArray FilterByPermissionToSee (Object prgCtx, RecArray recs)

				Integer CHUNKSIZE = 250

				if IsError(recs) || isUndefined(recs); return recs; end

				Integer started = Date.Tick()

				Integer threshold = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'threshold', 0)
				if not threshold
					threshold = 999999999
				end

				Integer responseLimit = 1000 * CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'responseLimit', 0)
				if not responseLimit
					responseLimit = 999999999
				end

				RecArray output = RecArray.Create (RecArray.FieldNames (recs))

				// permission check in chunks

				Integer chunkStart
				for (chunkStart = 1; chunkStart <= length (recs); chunkStart += CHUNKSIZE)

					// make a chunk
					RecArray chunk = RecArray.Create (RecArray.FieldNames (recs))

					Integer i
					for i = chunkStart to Math.Min (chunkStart + CHUNKSIZE - 1, length (recs))
						RecArray.InsertRecordAt (chunk, recs[i], length (chunk) + 1)
					end

					// filter the chunk
					$LLIAPI.NodeUtil.AppendPermissionsToRec ( \
							prgCtx.DSession(), \
							chunk \
						)
					Record rec
					for rec in chunk
						RecArray.InsertRecordAt (output, rec, length (output) + 1)
					end

					if length (output) > threshold
						return Undefined
					end
					if Date.Tick() - started > responseLimit
						return Undefined
					end
				end

				return output
			end


	public function Assoc RemoveACLs(DAPINODE node)

				Record nodeRight
				for nodeRight in DAPI.GetNodeRights (node)
					Integer removed = DAPI.DeleteNodeRight (node, nodeRight.RightID)

					if IsError (removed)
						return .Problem (Str.Format ("Error removing right %1 from node %2", nodeRight.RightID, node.pID), removed)
					end
				end		

				return .Success()
			end

	Script UpdatePermissions_OLDPreSalesForce

		// KM 2-5-2020 
		// this code is insane

		function Assoc UpdatePermissions ( \
				Object prgCtx, \
				DAPINode node, \
				Boolean quiet = false \
			)

			Assoc result = Operation (prgCtx, node)

			if quiet
				Assoc ok
				ok.ok = true
				return ok
			end

			return result
		end

		function Assoc Operation (Object prgCtx, DAPINode node )

			Frame timer = $BESSEMER.Timer.New()

			timer.start ("get attributes")


			// this item's cats/atts

			Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, node.pID, node.pVersionNum)
			if not attrData.Load()
				return .Problem (Str.Format ("Error loading attributes for node %1", node.pID))
			end


			// the bessemer attributes configuration

			Assoc result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx)
			if not result.OK
				return result
			end

			Assoc attributes = result.attributesByName


			String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
			if IsUndefined(fras)
				return .Problem ("fras attribute name not configured")
			end

			if not attributes.(fras) or length (attributes.(fras).children) < 4
				return .Problem ("unexpected fras configuration")
			end

			String family = attributes.(fras).children[1]
			String relationship = attributes.(fras).children[2]
			String account = attributes.(fras).children[3]
			String subaccount = attributes.(fras).children[4]


			String protocol = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol")
			if IsUndefined(protocol)
				return .Problem ("protocol attribute name not configured")
			end

			if not attributes.(protocol) or length (attributes.(protocol).children) < 2
				return .Problem ("unexpected protocol configuration")
			end

			String category = attributes.(protocol).children[1]
			String subcategory = attributes.(protocol).children[2]


			// interpret the permissions configuration table to choose which groups to assign

			timer.start ("interpret permissions table")

			List groups = {}

			Record config
			for config in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
					"select Field, Value, GroupID, Special from BESSEMER_Permissions" \
					+ " order by case Special when 0 then 999 else Special end" \
				)

				List values = {}
				Assoc a

				switch config.Field

					case "Family"

						for a in attrData.GetFromSet (fras, { \
								family, relationship, account, subaccount \
							})
							if a.(family) and not a.(relationship) and not a.(account) and not a.(subaccount)
								values = { @values, a.(family) }
							end
						end
						end

					case "Relationship"

						for a in attrData.GetFromSet (fras, { \
								family, relationship, account, subaccount \
							})
							if a.(relationship) and not a.(account) and not a.(subaccount)
								values = { @values, a.(relationship) }
							end
						end
						end

					case "Account"

						for a in attrData.GetFromSet (fras, { \
								family, relationship, account, subaccount \
							})
							if a.(account) and not a.(subaccount)
								values = { @values, a.(account) }
							end
						end
						end

					case "Subaccount"

						for a in attrData.GetFromSet (fras, { \
								family, relationship, account, subaccount \
							})
							if a.(subaccount)
								values = { @values, a.(subaccount) }
							end
						end
						end

					case "Category"

						for a in attrData.GetFromSet (protocol, { \
								category, subcategory \
							})
							if a.(category)
								values = { @values, a.(category) }
							end
						end
						end

					case "Subcategory"

						for a in attrData.GetFromSet (protocol, { \
								category, subcategory \
							})
							if a.(subcategory)
								values = { @values, a.(subcategory) }
							end
						end
						end

					case "Contact"

						values = attrData.Get ("Contact")
						end

					case "*"

						values = { "*" }
						end

					default
						return .Problem (Str.Format ("unknown field: %1", config.Field))
					end

				end


				// see if any values match 

				timer.start ("match groups")

				String value
				for value in values

					continueif not (IsDefined(value) and config.Value in { value, "*" })


					// add this group to the list of assignees

					if config.groupID > 0

						groups = List.SetAdd (groups, config.groupID)

					else

						// extract the groupname from "Addams [xyz12345]"

						String groupName

						Integer left = Str.RChr (value, "[")
						Integer right = Str.RChr (value, "]")

						if left and right and right - left > 1
							groupName = value[left + 1 : right - 1]
						else
							groupName = value
						end


						// get the group

						Assoc groupNamePrefix = Assoc.CreateAssoc ('')
						groupNamePrefix.(-1) = "F-"
						groupNamePrefix.(-2) = "R-"
						groupNamePrefix.(-3) = "A-"
						groupNamePrefix.(-4) = "S-"
						groupNamePrefix.(-5) = "C-"

						groupName = groupNamePrefix.(config.groupID) + groupName

						RecArray groupID = CAPI.Exec ( \
								prgCtx.fDBConnect.fConnection, \
								"select ID from KUAF where Name=:A1 and Type=:A2", groupName, UAPI.Group \
							)

						if not prgCtx.fDBConnect.CheckError (groupID).OK
							return .Problem ("Error associating KUAF")
						end

						if IsError(groupID) or length (groupID) == 0
							return .Problem (Str.Format ("No such group: %1", groupName))
						end

						groups = List.SetAdd (groups, groupID[1][1])
					end	
				end


				// if we're special we don't match more than this

				breakif groups and config.special > 0

			end


			// perform operations with elevated privs (if you can change the metadata you can change the perms)

			result = $LLIAPI.PrgSession.CreateNewNamed ( \
					$Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' ), \
					{ 'Admin', Undefined } )
			if not result.OK; return .Problem ("cannot obtain elevated privileges to update rights", result.errMsg); end

			Object adminPrgCtx = result.pSession

			node = DAPI.GetNodeByID (adminPrgCtx.DAPISess(), 0, node.pID)
			if not result.OK; return .Problem ("cannot reaccess node to update rights", node); end	


			timer.start ("update ACL")

			// remove ACL and add new groups

			Record nodeRight
			for nodeRight in DAPI.GetNodeRights (node)
				Integer removed = DAPI.DeleteNodeRight (node, nodeRight.RightID)

				if IsError (removed)
					return .Problem (Str.Format ("error removing right %1 from node %2", nodeRight.RightID, node.pID), removed)
				end
			end


			Integer group
			for group in groups
				Integer added = DAPI.AddNodeRight (node, group, $PSee | $PSeeContents | $PModify | $PEditAtts)

				if IsError (added)
					return .Problem (Str.Format ("error adding right %1 to node %2", group, node.pID), added)
				end
			end


			// apply same permissions to physical object, if any

			timer.start ("apply to PO")

			RecArray XRefPONode = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
					"select XDataID from RM_DocXRef where DataID = :A1", \
					node.pID \
				)
			if XRefPONode and length (XRefPONode) > 0

				DAPINode PONode = DAPI.GetNodeByID (adminPrgCtx.DAPISess(), 0, XRefPONode[1][1])
				if PONode

					for nodeRight in DAPI.GetNodeRights (PONode)
						DAPI.DeleteNodeRight (PONode, nodeRight.RightID)
					end

					for nodeRight in DAPI.GetNodeRights (node)
						DAPI.AddNodeRight (PONode, nodeRight.RightID, nodeRight.Permissions)
					end
				end
			end


			timer.Stop()

			$BESSEMER.Utils.LogTime (prgCtx, timer.totals)

			Assoc ok
			ok.ok = true
			return ok

		end

	endscript

	private function Assoc _GetNodeFromSql(Object prgCtx, DAPINODE parentNode, String path)

		CAPICONNECT dbConnection = prgCtx.fDBConnect.fConnection
		DAPINODE  node

    	String name
		for name in Str.Elements(path, ":")
			name = Str.Trim(name)
			continueif name == ""

			RecArray recs = CAPI.Exec(dbConnection, 
									"select DataID from Dtree where ParentID=:A1 and Name=:A2", 
									parentNode.pID, 
									name)

			if IsError(recs) || Length(recs) == 0
				// return an error so that the node might get created (if autoCreate option is on)
				return .Problem(Str.Format("Could not get node named %1 with parentID %2", name, parentNode.pID))
			end	

		 	node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, recs[1].DATAID)
			if IsError(node)
				// return the error so that the node might get created (if autoCreate option is on)
				return .Problem(Str.Format("Could not get node from dataID %1", recs[1].DATAID))
			 end

			// now, if we have more elements to check, the parentNode will be this object
			// if not, we exit out of the loop and return the last item
			parentNode = node
		end	

		return .Success(node)

 	end

end
