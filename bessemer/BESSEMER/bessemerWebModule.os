package BESSEMER

public Object bessemerWebModule inherits WEBDSP::WebModule

	override	Boolean	fEnabled = TRUE
	override	String	fModuleName = 'bessemer'
	override	String	fName = 'bessemer'
	override	List	fOSpaces = { 'bessemer' }
	override	String	fSetUpQueryString = 'func=bessemer.configure&module=bessemer&nextUrl=%1'
	override	List	fVersion = { '16', '2', 'r', '4' }

end
