package BESSEMER

public Object AttributesConfig inherits BESSEMER::TempCachedObject

	public		Dynamic	fResultNonSystem
	public		Dynamic	fResultSystem



	public function Assoc GetAttributes (Object prgCtx, Boolean includeSystem = false)

					if includeSystem

					      // get it first time. If it's an error, try again because it may have been fixed
					      // don't want to force Admin to restart when they're setting up system
					      if IsUndefined(.fResultSystem) || !.fResultSystem.ok 

					            .fResultSystem = ._GetAttributes (prgCtx, includeSystem)

					      end   

					      return .fResultSystem

					else

					      // get it first time. If it's an error, try again because it may have been fixed
					      // don't want to force Admin to restart when they're setting up system
					      if IsUndefined(.fResultNonSystem) || !.fResultNonSystem.ok 

					            .fResultNonSystem =   ._GetAttributes (prgCtx, includeSystem)

					      end   

					      return .fResultNonSystem                  


					end
			  end


	public function Assoc GetAttributesConfig(Object prgCtx, Assoc attributes, List contactCatSubcats, List fRASCatSubcats)

					//--- config
					// old stmt
					/*
					String stmt = "select attr.Attribute, attr.HeaderSheet, catSubcat.Category, catSubcat.Subcategory, catSubcat.Requiredness"\
							+ " from BESSEMER_MetadataAttributes attr" \
							+ " left join BESSEMER_MetadataCatSubcat catSubcat" \
							+ " on attr.Attribute = catSubcat.Attribute" \
							+ " order by attr.Ordering, attr.Attribute"
				    */

				    // these are all the regular cat subcat configurations
					String stmt = "SELECT attr.Attribute,"+\
								" attr.HeaderSheet,"+\
								" attr.Ordering,"+\
								" '{' || LISTAGG('{''' || catSubcat.Category || ''',''' || catSubcat.Subcategory || ''',' || catSubcat.Requiredness || '}', ',') WITHIN GROUP (ORDER BY catSubcat.Category, catSubcat.Subcategory) || '}' catSubcats"+\
								" FROM BESSEMER_MetadataAttributes attr,"+\
								" BESSEMER_MetadataCatSubcat catSubcat"+\
								" WHERE attr.Attribute = catSubcat.Attribute"+\
								" GROUP BY attr.Attribute,"+\
								" attr.HeaderSheet,"+\
								" attr.Ordering"+\		    
				    			" union all " +\
								"SELECT Attribute,"+\
							  	" HeaderSheet,"+\
							  	" Ordering,"+\
							  	" null as catSubcats"+\
								" FROM BESSEMER_MetadataAttributes"+\
								" where Attribute not in (select Attribute from BESSEMER_MetadataCatSubcat)"+\
								" ORDER BY Ordering, Attribute"

					RecArray configData = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt)
					if IsError(configData)
						return .Problem("Error getting Attributes from BESSEMER_MetadataAttributes table")
					end

					RecArray.Sort(configData, {3,1})		

					List config = {}

					Record configEntry
					for configEntry in configData
						Integer attrID = configEntry.Attribute
						Dynamic attribute = attributes.(attrID)

						Assoc entry = Assoc.CreateAssoc()
						entry.attribute = attribute
						entry.attributeID = attrID
						entry.headerSheet = configEntry.HeaderSheet

						List catSubcats = {}

						if IsDefined(configEntry.catSubcats)
							catSubcats = Str.StringToValue(configEntry.catSubcats)
						end

						if IsDefined(attribute)
							if attribute.Name == CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "contact")
								catSubCats = List.SetUnion(catSubCats, contactCatSubcats)
							elseif attribute.Name == CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
								catSubCats = List.SetUnion(catSubCats, fRASCatSubcats)		
							end				
						else
							EchoError("cannot find attribute for ", configEntry)
						end

						entry.catSubcats = catSubCats

						config = {@config, entry}
					end	

					return .Success(config)			  	

			  end

	/*

		            returns an assoc of the attributes defined in the configured category

		            keyed by ID, each entry has an assoc:

		            .name (FRAS)
		            .displayName (FRAS - Family...)
		            .ID
		            .type (text, integer, ...)
		            ,multiple (booleean)
		            .required (boolean)


		            Sets are ignored, except for the configured special attributes (Protocol, FRAS)
		            System attributes that are used by the system and not shown to the user during entry/config (Level) 
		              are ignored unless the "includeSystem" argument is passed

		            */
	private function Assoc _GetAttributes (Object prgCtx, Boolean includeSystem)

		          // our config

		          List acceptableTypes = \
		                { 'Date', 'Real', 'StringField', 'Integer', 'Boolean', 'StringPopup', 'StringMultiLine', 'IntegerPopup' }

		          List specialAttributes = { \
		                      CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'protocol'), \
		                      CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'fras') \
		                }

		          List systemAttributes = { \
		                      CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'level'), \
		                      CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'filedBy') \
		                }


		          Assoc attributes

		          // configured category

		          Integer categoryID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'category')
		          if not categoryID
		                return .Problem ("Category is not configured")
		          end

		          DAPINode category = DAPI.GetNodeById (prgCtx.DAPISess(), 0, categoryID)
		          Assoc categoryInfo = $LLIAPI.LLNodeSubsystem.GetItem (category.pSubtype).NodeFetchVersion (category)
		          if not categoryInfo.OK
		                return .Problem (categoryInfo.ErrMsg)
		          end
		          if not categoryInfo.Content
		                return .Problem ("Invalid category")
		          end

		          // attributes in the configured category
				  Assoc categoryData
				  for categoryData in categoryInfo.Content.Children

				    Assoc entry = Assoc.CreateAssoc()

				    entry.name = categoryData.DisplayName

				    String typeName = $LLIAPI.LLAttributeSubsystem.GetItem (categoryData.Type).OSParent.OSName

				    Boolean isAcceptableOrSpecial = typeName in acceptableTypes || entry.name in specialAttributes  

				    Boolean isNonSystemOrNeedSystem = includeSystem || !(entry.Name in systemAttributes) 

				    // KM 2-13-20 replace this ridiculously confusing "continueif not" statement
				    /*	
				    continueif not ( \
				          			(typeName in acceptableTypes or entry.name in specialAttributes) and (not (entry.Name in systemAttributes) or includeSystem) \
				    )
				    * this translates to continueif !(isAcceptableOrSpecial and (!isSystem || includeSystem))
				    * further reduces to !isAcceptableOrSpecial and !(isSystem || includeSystem)
				    * mathematical negation of that is isAcceptableOrSpecial or isSystemOrNeedSystem
				    */				
				    if isAcceptableOrSpecial && isNonSystemOrNeedSystem

				        entry.type = typeName         
				        entry.ID = categoryData.ID
				        entry.multiple = categoryData.MaxRows > 1
				        entry.required = categoryData.Required
				        entry.length = categoryData.Length
				        entry.display = categoryData.DisplayLen
				        entry.values = categoryData.ValidValues
				        entry.children = {}

				        entry.displayName = categoryData.DisplayName          
				        if categoryData.Children

				              entry.displayName += " ("

				              Assoc childAttribute
				              for childAttribute in categoryData.Children
				                    entry.displayName += childAttribute.DisplayName + ", "
				                    entry.children = { @entry.children, childAttribute.DisplayName }

				                    if childAttribute.required
				                          entry.required = true
				                    end
				              end

				              entry.displayName[-2:] = ")"
				        end

				        attributes.(entry.ID) = entry
				     end   
		          end


		          // add built-in attributes

		          Assoc entry

		          entry = Assoc.CreateAssoc()
		          entry.name = "File"
		          entry.type = "File"           
		          entry.ID = -2
		          entry.multiple = false
		          entry.required = false
		          entry.displayName = entry.name

		          attributes.(entry.ID) = entry


		          entry = Assoc.CreateAssoc()
		          entry.name = "Name"
		          entry.type = "StringField"          
		          entry.ID = -1
		          entry.multiple = false
		          entry.required = true
		          entry.displayName = entry.name
		          entry.display = 90
		          entry.length = 248 - 13  // we add a [###] thing

		          attributes.(entry.ID) = entry


		          Assoc ok
		          ok.ok = true
		          ok.attributes = attributes

		          Assoc attributesByName
		          for entry in attributes
		                attributesByName.(entry.name) = entry
		          end

		          ok.attributesByName = attributesByName
		          ok.categoryID = category.pID
		          ok.versionNumber = category.pVERSIONNUM

		          return ok

		    end

end
