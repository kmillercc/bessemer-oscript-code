package BESSEMER

public Object BessemerAdminIndexCallback inherits WEBADMIN::AdminIndexCallback

	override	Boolean	fEnabled = TRUE



	override function Assoc Execute( \
							Object	prgCtx, \
							Record	request )

							Assoc	retVal
							Assoc	sections


							Assoc	section_1 = ._NewSection()
							Assoc	section_1_1 = ._NewSection()
							Assoc	section_1_2 = ._NewSection()
							Assoc	section_1_3 = ._NewSection()
							//Assoc	section_1_4 = ._NewSection()
							Assoc	section_1_5 = ._NewSection()
							Assoc	section_1_6 = ._NewSection()
							Assoc	section_1_7 = ._NewSection()
							Assoc	section_1_8 = ._NewSection()
							Assoc	section_1_9 = ._NewSection()
							Assoc	section_1_10 = ._NewSection()
							//Assoc	section_1_11 = ._NewSection()
							Assoc	section_1_12 = ._NewSection()
							Assoc	section_1_13 = ._NewSection()

							String	scriptName = request.SCRIPT_NAME

							section_1.anchorName = "bessemer"
							section_1.heading = "Bessemer Administration"
							sections.( section_1.heading ) = section_1

							section_1_1.anchorHREF = scriptName + "?func=bessemer.dbCreate"
							section_1_1.heading = "Database Administration: Create Schema"
							section_1_1.text = "Creates the custom database tables for the Bessemer module"
							section_1.sections.( "X" + section_1_1.heading ) = section_1_1

							section_1_2.anchorHREF = scriptName + "?func=bessemer.dbDropConfirm"
							section_1_2.heading = "Database Administration: Drop Schema"
							section_1_2.text = "Removes all custom tables for the module from the database"
							section_1.sections.( "X" + section_1_2.heading ) = section_1_2

							section_1_3.anchorHREF = scriptName + "?func=bessemer.configStructureBuilder"
							section_1_3.heading = "Structure Builder: Configure Folders and Facets"
							section_1_3.text = "Sets parameters for processing the incoming feed, and creating virtual folders"
							section_1.sections.( section_1_3.heading ) = section_1_3

							section_1_5.anchorHREF = scriptName + "?func=bessemer.editCatSubcat"
							section_1_5.heading = "Filer: Configure Categories/Subcategories"
							section_1_5.text = "Edit the available category and subcategory metadata values"
							section_1.sections.( section_1_5.heading ) = section_1_5

							section_1_6.anchorHREF = scriptName + "?func=bessemer.configMetadata"
							section_1_6.heading = "Filer: Configure Metadata"
							section_1_6.text = "Choose the attributes used for metadata, and the conditions and order in which they appear"
							section_1.sections.( section_1_6.heading ) = section_1_6

							section_1_7.anchorHREF = scriptName + "?func=bessemer.configAttributes"
							section_1_7.heading = "Filer: Configure Attributes"
							section_1_7.text = "Choose the Content Server category that defines the metadata to be used by Filer, and the special attributes"
							section_1.sections.( section_1_7.heading ) = section_1_7

							section_1_8.anchorHREF = scriptName + "?func=bessemer.configProtocol"
							section_1_8.heading = "Filer: Configure Protocol"
							section_1_8.text = "Set the Family/Relationship/Account/Subaccount/Contact levels required according to category/subcategory"
							section_1.sections.( section_1_8.heading ) = section_1_8

							section_1_9.anchorHREF = scriptName + "?func=bessemer.utilities"
							section_1_9.heading = "Utilities & Settings"
							section_1_9.text = "Miscellaneous utilities and settings"
							section_1.sections.( "Z" + section_1_9.heading ) = section_1_9
		
							section_1_10.anchorHREF = scriptName + "?func=bessemer.configPermissions"
							section_1_10.heading = "Permissions Assign: Configure Fields and Groups"
							section_1_10.text = "Edit the table that defines the permission groups to be applied to repository items"
							section_1.sections.( section_1_10.heading ) = section_1_10

							/*		
							section_1_11.anchorHREF = scriptName + "?func=bessemer.permissionsResetJob"
							section_1_11.heading = "Permissions Assign: Reset Entire Repository"
							section_1_11.text = "Initiate a batch job that will update permissions on all items in the repository"
							section_1.sections.( section_1_11.heading ) = section_1_11
							*/

							section_1_12.anchorHREF = scriptName + "?func=bessemer.configLocations"
							section_1_12.heading = "Filer: Configure Locations"
							section_1_12.text = "Set the header sheet bucket folder; set the file system directory for file uploads"
							section_1.sections.( section_1_12.heading ) = section_1_12

							section_1_13.anchorHREF = scriptName + "?func=bessemer.configPresentation"
							section_1_13.heading = "Filer: Configure Presentation"
							section_1_13.text = "Set the screen widths of lists, the maximum number of items a list can show, and messages for oversized lists"
							section_1.sections.( section_1_13.heading ) = section_1_13


							retVal.ok = True
							retVal.sections = sections

							return retVal
						end

end
