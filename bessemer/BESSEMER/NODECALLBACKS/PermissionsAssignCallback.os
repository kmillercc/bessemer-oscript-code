package BESSEMER::NODECALLBACKS

public Object PermissionsAssignCallback inherits LLIAPI::NodeCallbacks

	override	Boolean	fEnabled = TRUE
	override	List	fSubTypes = { 144 }

	override function Assoc CBCategoriesUpdatePost( \
				Object		dapiCtx, \
				DAPINODE	node, \
				Assoc		updateInfo, \
				Dynamic		context )

		if dapiCtx.fPrgCtx.GetSessionProperty('skipPermAssignCallback')
			echo('Skipping Permissions Assign callback because it will be handled later as part of Filer')
			return Assoc{'ok': true} 
		end				

		return ._Bessemer(node.pID)

	end


	override function Assoc CBCreate( \
				Object		dapiCtx, \
				DAPINODE	node, \
				Assoc		createInfo, \
				Dynamic		context )

		// check Session Property that tells us whether to run this process
		if dapiCtx.fPrgCtx.GetSessionProperty('skipPermAssignCallback')
			echo('Skipping Permissions Assign callback because it will be handled later as part of Filer')
			return Assoc{'ok': true} 
		end

		return ._Bessemer(node.pID)
	end


	private function Assoc _Bessemer(Integer nodeID)

		echo('Running Permissions Assign callback')	

		Assoc result = $LLIAPI.PrgSession.CreateNewNamed ( \
				$Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' ), \
				{ 'Admin', Undefined } )

		if !result.OK
			return Assoc{'ok': false, 'errMsg': "Cannot obtain elevated privileges for permissions update: " + result.errMsg}
		end

		Object adminCtx = result.pSession

		DAPINODE node = DAPI.GetNodeByID (adminCtx.DAPISess(), 0, nodeID)
		if IsError(node)
			return Assoc{'ok': true, 'errMsg': "Cannot access node for permissions update"}
		end						

		Assoc attributesByName

		Assoc status = $BESSEMER.AttributesConfig.GetAttributes (adminCtx, true)
		if status.OK	
			attributesByName = status.attributesByName
		else
			return status
		end

		Frame attrData = $BESSEMER.CategoryAttributes.New (adminCtx, node.pID, node.pVersionNum)
		attrData.Load()

		Integer repositoryID = CAPI.IniGet (adminCtx.fDbConnect.fLogin, 'BESSEMER', 'repository')

		if IsDefined(repositoryID) and node.pParentID == repositoryID
			
			// now update permissions
			status = $Bessemer.DocPermissions.UpdatePermissions(adminCtx, node, attributesByName, attrData)

			if !status.ok
				Boolean ignoreErrors = CAPI.IniGet (adminCtx.fDbConnect.fLogin, 'BESSEMER', 'ignoreUserPermissionErrors', false)

				if !ignoreErrors
					return status
				end	
			end
		end

		return Assoc{'ok': true}

	end

end
