package BESSEMER::NODECALLBACKS

public Object UpdatePermissionsOld inherits LLIAPI::NodeCallbacks

	override	Boolean	fEnabled = FALSE
	override	List	fSubTypes = { 144 }



	override function Assoc CBCategoriesUpdatePost( \
							Object		dapiCtx, \
							DAPINODE	node, \
							Assoc		updateInfo, \
							Dynamic		context )

							return ._bessemer (dapiCtx.fPrgCtx, node)
						end


	override function Assoc CBCreate( \
							Object		dapiCtx, \
							DAPINODE	node, \
							Assoc		createInfo, \
							Dynamic		context )

							return ._bessemer (dapiCtx.fPrgCtx, node)

						end


	private function Assoc _bessemer ( \
								Object		prgCtx, \
								DAPINODE	node \
							)

							Integer repositoryID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'repository')

							if repositoryID and (node.pParentID == repositoryID or DAPI.GetParentNode (node).pParentID == repositoryID)

								return $BESSEMER.Utils.UpdatePermissions ( \
										prgCtx, \
										node, \
										CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'ignoreUserPermissionErrors', false) \
									)

							else

								Assoc ok
								ok.ok = true
								return ok

							end

						end

end
