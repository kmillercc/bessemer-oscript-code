package BESSEMER::NODECALLBACKS

public Object RenamePhysicalObject inherits LLIAPI::NodeCallbacks

	override	Boolean	fEnabled = TRUE
	override	List	fSubTypes = { 144 }



	override function Assoc CBRename( \
							Object		dapiCtx, \
							DAPINODE	node, \
							Dynamic		newName, \
							Dynamic		context )

							Assoc ok
							ok.ok = true

							Object prgCtx = dapiCtx.fPrgCtx


							// are we updating a document in the repository?

							Integer repositoryID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'repository')
							if not repositoryID
								return ok
							end

							if node.pParentID != repositoryID and DAPI.GetParentNode (node).pParentID != repositoryID
								return ok
							end


							// and does it have a xref?

							RecArray XRefPONode = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select XDataID from RM_DocXRef where DataID = :A1", \
									node.pID \
								)
							if IsError(XRefPONode) || length (XRefPONode) == 0
								return ok
							end


							// make sure the impossible doesn't happen so there's not an infinite loop

							Integer ID = XRefPONode[1][1]
							DAPINode PONode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, ID)
							if IsError(PONode)
								return ok
							end

							if PONode.pSubType == $TypeDocument  // never happen!?!
								return ok
							end

							DAPI.RenameNode (PONode, newName)


							return ok	
						end

end
