package BESSEMER::PERMISSIONS::NODE

public Object ContactPermissions inherits BESSEMER::PERMISSIONS::NODE::NodePermissions


	override function Assoc AddRelationshipToNode( Object prgCtx, DAPINODE node, String relationshipName )

				String groupName

				// check exclusion table
				if .IsExcludedCsContact(prgCtx, node.pName)

					// TODO - check on what we do with this situation
				else	
					groupName = ._GetGroupNameFromNodeName( "RS-", relationshipName )

					if IsDefined( groupName )

						Assoc status = .AddUpdateRightByGroupName( prgCtx, node, groupName )

						if !status.ok
							.Problem ( Str.Format ( "Could not add group %1 to node %2", groupName, node.pID ), status.errMsg )
						end

					else
						return .Problem ( Str.Format ( "Could not get relationship RS group name from: %1", relationshipName ) )
					end
				end	

				return .Success()

			end


	override function Assoc RemoveRelationshipFromNode( Object prgCtx, DAPINODE node, String relationshipName )

		String groupName

		// check exclusion table
		if .IsExcludedCsContact(prgCtx, node.pName)	

			// TODO - check on what we do with this situation

		else	
			groupName = ._GetGroupNameFromNodeName( "RS-", relationshipName )

			if IsDefined( groupName )
				Assoc status = .RemoveRightByGroupName( prgCtx, node, groupName )

				if !status.ok
					.Problem ( Str.Format ( "Could not remove group %1 from node %2", groupName, node.pID ), status.errMsg )
				end

			else
				return .Problem ( Str.Format ( "Could not get relationship RS group name from: %1", relationshipName ) )
			end

		end

		return .Success()

	end

end
