package BESSEMER::PERMISSIONS::NODE

public Object FamilyPermissions inherits BESSEMER::PERMISSIONS::NODE::NodePermissions


	public function Assoc AddFSGroupToFamily(Object prgCtx, DAPINODE fgNode)

				String familyName  = fgNode.pName

				// get the RS group		
				String fsName = ._GetGroupNameFromNodeName( "FS-", familyName )

				if IsDefined( fsName )
					Assoc status = .AddUpdateRightByGroupName( prgCtx, fgNode, fsName )
					if !status.ok
						.Problem ( Str.Format ( "Could not add group %1 to node %2", fsName, fgNode.pID ), status.errMsg )
					end

				else
					return .Problem ( Str.Format ( "Could not get family FS group name from: %1", familyName ) )
				end		

				return .Success()

			end


	override function Assoc AddRelationshipToNode( Object prgCtx, DAPINODE node, String relationshipName )

		return .Problem("Should not be adding RS groups to Family Nodes")

	end


	override function Assoc SetPermissionsOnCreate( Object prgCtx, DAPINODE node, String relationshipName = undefined )

				// update owner group
				Assoc status = ._UpdateOwnerGroup( prgCtx, node )
				if !status.ok
					return status
				end

				// remove inherited perms (not sure this is necessary)
				status = $BESSEMER.Utils.RemoveACLs( node )
				if !status.ok
					return status
				end

				// Add FS Group
				status = .AddFSGroupToFamily(prgCtx, node)
				if !status.ok
					return status
				end

				return .Success()

			end

end
