package BESSEMER::PERMISSIONS::NODE

public Object SubAccountPermissions inherits BESSEMER::PERMISSIONS::NODE::NodePermissions


	private function Assoc _GetGroupNamesFromSubAccount(Object prgCtx, String subName)

				List groups

				// *** get relationship name
				String	stmt = 'select RELATIONSHIP_FULL from BESSEMER_NODE_HIERARCHY where SUBACCOUNT_FULL=:A1'
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, subName)

				if IsNotError(recs) && Length(recs) > 0

					String rsName = ._GetGroupNameFromNodeName("RS-", recs[1].RELATIONSHIP_FULL)
					groups = List.SetAdd(groups, rsName)	

				else
					return .Problem(Str.Format("Could not find relationship from Account Name %1", subName))	
				end 

				return .Success(groups)

			end


	override function Assoc _GetRelationshipGroupNames( Object prgCtx, DAPINODE node )

				return ._GetGroupNamesFromSubAccount(prgCtx, node.pName)

			end

end
