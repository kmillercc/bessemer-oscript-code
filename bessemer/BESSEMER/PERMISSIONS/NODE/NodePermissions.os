package BESSEMER::PERMISSIONS::NODE

public Object NodePermissions inherits BESSEMER::PERMISSIONS::PermissionsRootObject

	public function Assoc SetPermissionsOnCreate(Object prgCtx, DAPINODE node, String relationshipName = undefined)

		// update owner group
		Assoc status = ._UpdateOwnerGroup(prgCtx, node)
		if !status.ok
			return status
		end

		// remove inherited perms (not sure this is necessary)
		status = $BESSEMER.Utils.RemoveACLs(node)
		if !status.ok
			return status
		end				

		// add permissions to the node if relationshipName is defined
		if IsDefined(relationshipName)
			status = .AddRelationshipToNode(prgCtx, node, relationshipName)
			if !status.ok
				return status
			end					
		end				

		return .Success()

	end

	public function Assoc ReplaceRelationshipGroup(Object prgCtx, DAPINODE node, String newRelationshipName, String oldRelationshipName)

		Assoc status

		String oldRsName = ._GetGroupNameFromNodeName("RS-", oldRelationshipName)
		if IsDefined(oldRsName)
			status = .RemoveRightByGroupName(prgCtx, node, oldRsName)
   			if !status.ok
   				return .Problem (Str.Format ("Could not remove group %1 from node %2", oldRsName, node.pID), status.errMsg)
   			end
   		else
   			return .Problem (Str.Format ("Cannot find group name to remove from %1", oldRelationshipName))
		end	

		String newRsName = ._GetGroupNameFromNodeName("RS-", newRelationshipName)
		if IsDefined(newRsName)
			// Now add the right
			status = .AddUpdateRightByGroupName(prgCtx, node, newRsName) 
			if !status.ok
				return .Problem(Str.Format("Unable to add node right to %1 Virtual Folder with dataid %2", node.pName, node.pID), status.errMsg)
			end
   		else
   			return .Problem (Str.Format ("Cannot find group name to add from %1", newRelationshipName))
		end		

		return Assoc{'ok': true}			
	end

	public function Assoc UpdatePermissions(Object prgCtx, DAPINODE node)

		List 	groupNames

		// convert relationship to group name			
		Assoc status = ._GetRelationshipGroupNames(prgCtx, node)
		if !status.ok
			return status
		else
			groupNames = status.result	
		end	

		// Loop through the groupnames and make sure the groups are added to node permissions
		String groupName
		for groupName in groupNames
			// Now add the right
			status = .AddUpdateRightByGroupName(prgCtx, node, groupName) 
			if !status.ok
				return .Problem(Str.Format("Unable to add node right to %1 Virtual Folder with dataid %2", node, node.pID), status.errMsg)
			end
		end	

		return .Success(groupNames)

	end


	private function Assoc _GetRelationshipGroupNames(Object prgCtx, DAPINODE node)

				List groupNames = {}

				return .Success(groupNames)

			end


	private function Assoc _UpdateOwnerGroup(prgCtx, node)	
				// make the G-Dorado-Global-Group the owner group
				String groupName = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'globalGroupName', 'G-Dorado-Global-Group')

				// Look for this group in KUAF
				Integer groupID = ._GetGroupIDByName(prgCtx, groupName)
				if IsDefined(groupID)

					if node.pGroupID != groupID		
						// set the owner group here
						node.pGroupPerm = $PSee | $PSeeContents
						node.pGroupId = groupID

						if IsError(DAPI.UpdateNode(node))
							return .Problem(Str.Format("Error updating node to owner group %1", groupID))
						end
					end		
				else
					return .Problem(Str.Format("Cannot find %1 to set as owner group.", groupName))	
				end

				return .Success()

			end

end
