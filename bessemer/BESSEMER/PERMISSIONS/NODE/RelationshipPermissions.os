package BESSEMER::PERMISSIONS::NODE

public Object RelationshipPermissions inherits BESSEMER::PERMISSIONS::NODE::NodePermissions


	override function Assoc UpdatePermissions( Object prgCtx, DAPINODE node, Boolean addToParent = false )

		List groupNames

		// call superclass
		Assoc status = .OSParent.UpdatePermissions(prgCtx, node)
		if !status.ok
			return status
		else
			groupNames = status.result	
		end

		// add to family group
		if addToParent
			DAPINODE parentNode = DAPI.GetParentNode(node)

			// TODO!!! check if this is in exclusion table
			if !IsError(parentNode)
				String groupName
				for groupName in groupNames
					status = .AddUpdateRightByGroupName(prgCtx, node, groupName)
					if !status.ok
						return .Problem(Str.Format("Unable to add node right %1 to Virtual Folder with dataid %2", groupName, parentNode.pID), status.errMsg)
					end
				end	
			else
				return .Problem(Str.Format("Unable to get parent node for %1", node.pID))	
			end						
		end

		return .Success()

	end


	override function Assoc _GetRelationshipGroupNames( Object prgCtx, DAPINODE node )

				List groups

				// get the RS group		
				String rsName = ._GetGroupNameFromNodeName("RS-", node.pName)
				if IsDefined(rsName)
					groups = List.SetAdd(groups, rsName)			
				else
					return .Problem (Str.Format ("Invalid relationship group name: %1", rsName))
				end					

				return .Success( groups )

			end

end
