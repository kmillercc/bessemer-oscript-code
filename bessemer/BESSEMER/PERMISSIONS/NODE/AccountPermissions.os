package BESSEMER::PERMISSIONS::NODE

public Object AccountPermissions inherits BESSEMER::PERMISSIONS::NODE::NodePermissions


	private function Assoc _GetGroupNamesFromAccount(Object prgCtx, String accName)

				List groups

				// *** get relationship name
				String	stmt = 'select RELATIONSHIP_FULL from BESSEMER_NODE_HIERARCHY where ACCOUNT_FULL=:A1'
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, accName)

				if IsNotError(recs) && Length(recs) > 0
					String rsName = ._GetGroupNameFromNodeName("RS-", recs[1].RELATIONSHIP_FULL)
					groups = List.SetAdd(groups, rsName)	
				else
					return .Problem(Str.Format("Could not find relationship from Account Name %1", accName))	
				end 

				return .Success( groups )

			end


	override function Assoc _GetRelationshipGroupNames( Object prgCtx, DAPINODE node )

				return ._GetGroupNamesFromAccount(prgCtx, node.pName)

			end

end
