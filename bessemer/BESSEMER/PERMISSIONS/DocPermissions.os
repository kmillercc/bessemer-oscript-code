package BESSEMER::PERMISSIONS

public Object DocPermissions inherits BESSEMER::PERMISSIONS::PermissionsRootObject


	public function Assoc AddRelationshipToDocsWithContact(Object prgCtx, String contactName, String relationshipName)

				Integer permMask = $PSee | $PSeeContents | $PModify | $PEditAtts

				// first get the docs
				Assoc status = ._GetDocsWithContact(prgCtx, contactName)
				if !status.ok
					return status
				end

				if Length(status.result)
					List docNodes = status.result
					DAPINODE node

					if .IsExcludedCsContact(prgCtx, contactName)

						String groupName = ._GetGroupNameFromNodeName("CS-", contactName)
						if IsUndefined(groupName)
							return .Problem (Str.Format ("Invalid contact name: %1", contactName))
						end

						// loop through the nodes and add the relationship
						for node in docNodes

							// Now add the right
							status = .AddUpdateRightByGroupName(prgCtx, node, groupName, permMask)
							if !status.ok
								return status
							end				
						end			
					else	
						// loop through the nodes and add the relationship
						for node in docNodes
							// Now add the right
							status = .AddRelationshipToNode(prgCtx, node, relationshipName, permMask)
							if !status.ok
								return status
							end				
						end					
					end

				end


				return .Success()
			end


	public function Assoc GetDocFSGroups(Object prgCtx, DAPINODE node, Assoc attributesByName, Frame attrData, Boolean overrideFSGroup = false)

		List groupNames

		// get name of fras set
		String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
		if IsUndefined(fras)
			return .Problem ("fras attribute name not configured")
		end		

		String fgAttrName = attributesByName.(fras).children[1]
		String relAttrName = attributesByName.(fras).children[2]

		List 	setRows = attrData.GetFromSet (fras, {fgAttrName, relAttrName})
		Assoc	setRow
		String	groupName

		// loop through the set rows and see if we have relationship and see if we have family group
		for setRow in setRows

			String family = setRow.(fgAttrName)
			String relationship = setRow.(relAttrName)

			if IsUndefined(relationship) && IsDefined(family)		

				if .IsFileableFamily(prgCtx, family) || overrideFSGroup

					// great we have a relationship -- add it to the list of names
					groupName = ._GetGroupNameFromNodeName("FS-", family)
					if IsUndefined(groupName)
						return .Problem (Str.Format ("Invalid family name: %1", family))
					end	

					groupNames = List.SetAdd(groupNames, groupName)

				else
					return .Problem(Str.Format("You can not file a document to family group %1", family))	

				end
			end

		end		

		return .Success(groupNames)

	end


	public function Boolean IsFileableFamily(Object prgCtx, String familyName)

				String ID = ._IDFromName(familyName)

				if IsDefined(ID)
					// check for exclusion table
					// TODO check here
					RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, "select FAMILY_NO from BESSEMER_FS_GROUPS where FAMILY_NO=:A1", ID)

					if IsNotError(recs) && Length(recs) > 0
						return false
					end		
				end

				return true				
			end


	public function Assoc RemoveRelationshipFromDocsWithContact(Object prgCtx, String contactName, String relationshipName)

				if !.IsExcludedCsContact(prgCtx, contactName)
					// first get the docs
					Assoc status = ._GetDocsWithContact(prgCtx, contactName)
					if !status.ok
						return status
					end

					if Length(status.result)
						// loop through the nodes and add the relationship
						List docNodes = status.result
						DAPINODE node
						for node in docNodes
							// Now remove the right
							status = .RemoveRelationshipFromNode(prgCtx, node, relationshipName)
							if !status.ok
								return status
							end				
						end	 
					end
				end

				return .Success()
			end


    public function Assoc UpdatePermissions(Object adminCtx, DAPINODE node, Assoc attributesByName, Frame attrData)
    
        Integer permsBitMask = $PSee | $PSeeContents | $PModify | $PEditAtts

        // Note: both the adminCtx and the DAPINODE obtained with AdminCtx must be passed in to work here
        
        // remove inherited permissions
        Assoc status = $BESSEMER.Utils.RemoveACLs(node)
        if !status.ok
                        return status                      
        end
        
        // now do the global groups first
        status = ._HandleGlobalGroups(adminCtx, node, attributesByName, attrData, permsBitMask)
        if !status.ok
            
            return status
        
        elseif status.isSpecial
                                        
            // this means we get a global group and then nothing else
            return Assoc{'ok': true}
            
        end
                                                        
        // Get the relationship group for the relationship on this node    
        status = ._GetFSAndRSGroupNamesToAdd(adminCtx, node, attributesByName, attrData)
        if !status.ok
			return status
        end        

        // Loop through the result
        String groupName
        for groupName in status.result

            // Look for this group in KUAF
            Integer groupID = ._GetGroupIDByName(adminCtx, groupName)
            if IsUndefined(groupID)
				return .Problem (Str.Format("Could not find group with name %1 for document permissions", groupName))
            end                                        

            // Now add the right
            if !._AddUpdateRight(adminCtx, node, groupName, groupID, permsBitMask)
                return .Problem(Str.Format("Unable to add group %1 to Document with dataid %2", groupName, node.pID))
            end

            // apply same permissions to physical object, if any
            RecArray xRecs = CAPI.Exec (adminCtx.fDBConnect.fConnection, "select XDataID from RM_DocXRef where DataID = :A1", node.pID)
            if IsNotError(xRecs) and Length (xRecs) > 0
                DAPINode PONode = DAPI.GetNodeByID (adminCtx.DAPISess(), 0, xRecs[1].XDataID)
                if IsNotError(PONode)
					DAPI.AddNodeRight (PONode, groupID, permsBitMask)
                end
            end
        end        
        
        return .Success()

	end
	
    private function Assoc _HandleGlobalGroups(Object adminCtx, DAPINODE node, Assoc attributesByName, Frame attrData, Integer permsBitMask)
                                            
        Boolean isSpecial = false
                
        // *** now get the global groups
        Assoc status = $BESSEMER.PermissionsConfig.GetGlobalGroups(adminCtx, attributesByName, attrData)
        if !status.ok
            return status
        end
                
        // now loop through the groups that need to be added
        Assoc group
        for group in status.result
            // set this flag to tell caller whether this would be the only group added
            // it's either already set to true (from previously in the loop) 
            // or this particular rec indicates that it should be
            isSpecial = isSpecial || IsFeature(group, 'special') && group.special > 0 
                    
            if !._AddUpdateRight(adminCtx, node, group.Name, group.ID, permsBitMask)
				return .Problem(Str.Format("Unable to add group %1 to Document with dataid %2", group.Name, node.pID))
            end                                        
        end

        return Assoc{'ok': TRUE, 'isSpecial': isSpecial}
                    
	end

	private function Boolean _AddUpdateRight(Object prgCtx, DAPINODE node, String groupName, Integer rightID, Integer permissions = $PSee | $PSeeContents)

				// *** does this node already have this user/group in the ACL
				String	stmt = 'select * from DTreeACL where DataID=:A1 and RightID=:A2 and ACLType=:A3'
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pId, rightId, DAPI.PERMTYPE_ACL)

				if Length(recs) == 0
					if IsError(DAPI.AddNodeRight (node, rightID, permissions))
						return false
					else
						EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Group %1 added to ACL of %2 [%3]", groupName, node.pName, node.pID))
					end
				end

				return true
			end


	private function String _GetContactFacetTableName(Object prgCtx)

				// default tableName
				String	tableName = "DFACET_ATTR_108811_12"

				// get the contact facet ID			
				Integer facetID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'contactFacet', '112112')

				// get the contact facet Node
				DAPINODE facetNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, facetID)

				// get the table from the node if we can		
				if IsNotError(facetNode) && IsDefined( facetNode.pExtendedData ) && Type( facetNode.pExtendedData ) == Assoc.AssocType && Assoc.IsKey( facetNode.pExtendedData, 'facetTableName' )
					tableName = facetNode.pExtendedData.facetTableName
				end

				return tableName
			end


	private function Assoc _GetDocsWithContact(Object prgCtx, String contactName)

				// default return value
				List	docNodes

				String tableName = ._GetContactFacetTableName(prgCtx)

				// now query to get the documents that have that contact
				String stmt = Str.Format("SELECT d.DATAID FROM dtreecore d INNER JOIN %1 a ON d.dataid = a.dataid WHERE a.facet = :A1 and d.subtype=144", tableName)
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, contactName)

				// loop through and build a list of nodes
				if IsNotError(recs)
					Record rec
					for rec in recs
						DAPINODE docNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.DATAID)

						if IsNotError(docNode)
							docNodes = {@docNodes, docNode}
						end	
					end
				end

				return .Success(docNodes)

			end


	private function Assoc _GetFrasAttributeFromAttrs(Object prgCtx, \
									  DAPINODE node, \
									  Assoc attributesByName, \
									  Frame attrData, \
									  Integer attrIndex)

				// get name of fras set
				String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
				if IsUndefined(fras)
					return .Problem ("fras attribute name not configured")
				end

				// now get relationship	
				if IsUndefined(attributesByName.(fras)) || Length(attributesByName.(fras).children) < 4
					return .Problem ("unexpected fras configuration")
				end

				String attrName = attributesByName.(fras).children[attrIndex]

				List attrVals
				Assoc a
				for a in attrData.GetFromSet (fras, { attrName })
					if IsDefined(a.(attrName)) 
						attrVals = List.SetAdd( attrVals, a.(attrName) )
					end
				end

				return .Success(attrVals)

			end


	private function Assoc _GetFSAndRSGroupNamesToAdd(Object prgCtx, DAPINODE node, Assoc attributesByName, Frame attrData)

		List groupNames		
		
		// get name of fras set
		String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
		if IsUndefined(fras)
			return .Problem ("fras attribute name not configured")
		end		

		String fgAttrName = attributesByName.(fras).children[1]
		String relAttrName = attributesByName.(fras).children[2]

		List 	setRows = attrData.GetFromSet (fras, {fgAttrName, relAttrName})
		Assoc	setRow
		String	groupName

		// loop throught the set rows and see if we have relationship and see if we have family group
		for setRow in setRows

			String family = setRow.(fgAttrName)
			String relationship = setRow.(relAttrName)

			if IsDefined(relationship)

				// great we have a relationship -- add it to the list of names
				groupName = ._GetGroupNameFromNodeName("RS-", relationship)
				if IsUndefined(groupName)
					return .Problem (Str.Format ("Invalid relationship name: %1", relationship))
				end	

				groupNames = List.SetAdd(groupNames, groupName)				

			elseif IsDefined(family)		

				if .IsFileableFamily(prgCtx, family)

					// great we have a relationship -- add it to the list of names
					groupName = ._GetGroupNameFromNodeName("FS-", family)
					if IsUndefined(groupName)
						return .Problem (Str.Format ("Invalid family name: %1", family))
					end	

					groupNames = List.SetAdd(groupNames, groupName)

				else
					return .Problem(Str.Format("You can not file a document to family group %1", family))	

				end
			else
				// this should not happen
				// do nothing. log it?
			end

		end		

		// get groups from the contacts
		Assoc status = ._GetGroupsFromDocContacts(prgCtx, node)
		if !status.ok
			return .Problem('Could not get relationships from contacts', status.errMsg)
		end

		groupNames = List.SetUnion(groupNames, status.result)				

		return .Success(groupNames)

	end


	private function Assoc _GetGroupsFromDocContacts(Object prgCtx, DAPINODE node)

				// default return value
				List	groups

				String tableName = ._GetContactFacetTableName(prgCtx)

				// now query to get contact names for that document
				String stmt = Str.Format("select a.FACET from %1 a INNER JOIN DTREECORE d ON a.dataid = d.dataid WHERE d.dataID = :A1", tableName)
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pID)

				// loop through and build a list of nodes
				if IsNotError(recs)
					Record rec
					for rec in recs

						String contactName = rec.FACET

						// make sure we are supposed to add relationships for this contact
						if .IsExcludedCsContact(prgCtx, contactName)

							// do the CS group
							String groupName = ._GetGroupNameFromNodeName("CS-", contactName)
							if IsUndefined(groupName)
								return .Problem (Str.Format ("Invalid contact name: %1", contactName))
							end					

							groups = List.SetAdd(groups, groupName)

						else	

							RecArray relationshipRecs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, \ 
																	'select distinct RELATIONSHIP from BESSEMER_FRAS_CONTACTS where CONTACT=:A1', \
																	contactName)
							if IsNotError(relationshipRecs)	&& Length(relationshipRecs) > 0

								Record relationshipRec
								for relationshipRec in 	relationshipRecs

									String groupName = ._GetGroupNameFromNodeName("RS-", relationshipRec.RELATIONSHIP)

									groups = List.SetAdd(groups, groupName)
								end	

							end
						end					
					end
				end

				return .Success(groups)
			end

end
