package BESSEMER::PERMISSIONS

public Object PermissionsRootObject inherits BESSEMER::TempCachedObject

	public		Assoc	fGroupIDCache
	public		Assoc	fNameIDCache



	public function Assoc AddRelationshipToNode(Object prgCtx, DAPINODE node, String relationshipName, Integer permissions = $PSee | $PSeeContents)

		// get the RS group		
		String rsName = ._GetGroupNameFromNodeName("RS-", relationshipName)
		if IsDefined(rsName)

			Assoc status = .AddUpdateRightByGroupName(prgCtx, node, rsName, permissions)
			if !status.ok
				.Problem (Str.Format ("Could not add group %1 to node %2", rsName, node.pID), status.errMsg)
			end

		else
			return .Problem (Str.Format ("Invalid relationship group name: %1", rsName))
		end

		return .Success()

	end


	public function Assoc AddUpdateRightByGroupName(Object prgCtx, DAPINODE node, String groupName, Integer permissions = $PSee | $PSeeContents)

		String statusStr = Str.Format('Group %1 Added', groupName)

		// Look for this group in KUAF
		Integer groupID = ._GetGroupIDByName(prgCtx, groupName)
		if IsUndefined(groupID)
			return .Problem (Str.Format("Could not find group with name %1", groupName))
		end

		// *** does this node already have this user/group in the ACL
		String	stmt = 'select * from DTreeACL where DataID=:A1 and RightID=:A2'
		RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pId, groupID)

		if Length(recs) == 0
			if IsError(DAPI.AddNodeRight (node, groupID, permissions))
				return .Problem("Unable to add node right to node with dataid %1", node.pID)
			else
				EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Group %1 added to ACL of $2 [%3]", groupName, node.pName, node.pID))
			end
		else
			statusStr = Str.Format('Group %1 Already Added', groupName)	
		end

		Assoc status = .Success()
		status.statusStr = statusStr

		return status

	end


	public function Boolean IsExcludedCsContact(Object prgCtx, String contactName)

					String ID = ._IDFromName(contactName)

					if IsDefined(ID)
						// check for exclusion table
						// TODO check here
						RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, "select CONTACT_NO from BESSEMER_CS_GROUPS where CONTACT_NO=:A1", ID)

						if IsNotError(recs) && Length(recs) > 0
							return true
						end		
					end

					return false				
				end


	override function Object New()

					Object o = Os.NewTemp(this)
					o.fGroupIDCache = Assoc.CreateAssoc()
					o.fNameIDCache = Assoc.CreateAssoc()

					return o
				end


	public function Assoc RemoveRelationshipFromNode(Object prgCtx, DAPINODE node, String relationshipName)

					// get the RS group		
					String rsName = ._GetGroupNameFromNodeName("RS-", relationshipName)
					if IsDefined(rsName)

						Assoc status = .RemoveRightByGroupName(prgCtx, node, rsName)
						if !status.ok
							.Problem (Str.Format ("Could not remove group %1 from node %2", rsName, node.pID), status.errMsg)
						end

					else
						return .Problem (Str.Format ("Invalid relationship group name: %1", rsName))
					end

					return .Success()

				end


	public function Assoc RemoveRightByGroupName(Object prgCtx, DAPINODE node, String groupName)

					String statusStr = Str.Format('Group %1 Removed', groupName)

					// Look for this group in KUAF
					Integer groupID = ._GetGroupIDByName(prgCtx, groupName)
					if IsUndefined(groupID)
						return .Problem (Str.Format("Could not find group with name %1", groupName))
					end

					// *** does this node already have this user/group in the ACL
					String	stmt = 'select * from DTreeACL where DataID=:A1 and RightID=:A2 and ACLType=:A3'
					RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pId, groupID, DAPI.PERMTYPE_ACL)

					if Length(recs) == 1
						if IsError(DAPI.DeleteNodeRight(node, groupID))
							return .Problem("Unable to add node right to node with dataid %1", node.pID)
						else
							EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Group %1 removed from ACL of $2 [%3]", groupName, node.pName, node.pID))
						end
					else
						statusStr = Str.Format('Group %1 Already Removed', groupName)	
					end

					Assoc status = .Success()
					status.statusStr = statusStr

					return status

				end


	private function Integer _GetGroupIDByName(Object prgCtx, String groupName)

				Integer id = undefined

				if IsFeature(.fGroupIDCache, groupName)
					id = .fGroupIDCache.(groupName)
				else	
					RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, "select ID from KUAF where Name=:A1 and Type=:A2", groupName, UAPI.Group)

					if IsNotError(recs) && Length(recs) > 0
						id = recs[1].ID
						.fGroupIDCache.(groupName) = id
					end
				end

				return id

			end


	private function String _GetGroupNameFromNodeName(String prefix, String nodeName)

					// get the relationship id
					String ID = ._IDFromName (nodeName)
					if IsDefined(ID)
						return prefix + ID
					end	

					return undefined

				end


	private function String _IDFromName(String name)


				String idStr = undefined

				if IsFeature(.fNameIDCache, name)
					idStr = .fNameIDCache.(name)
				else											
					Integer left = Str.RChr (name, "[")
					Integer right = Str.RChr (name, "]")

					if left and right and right - left > 1
						idStr = name[left + 1 : right - 1]
						.fNameIDCache.(name) = idStr
					end

				end

				return idStr	

			end

end
