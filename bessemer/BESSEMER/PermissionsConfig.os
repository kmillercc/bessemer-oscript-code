package BESSEMER

public Object PermissionsConfig inherits BESSEMER::TempCachedObject

	public		String	fALLConstant = 'ALL'
	public		String	fAccountAttrName
	public		List	fCatSubCatFieldList
	public		String	fCategoryAttrName
	public		String	fFamilyAttrName
	public		Assoc	fFieldNameMap
	public		List	fFrasFieldList
	public		String	fFrasHierarchySetName
	public		Boolean	fInitialized = FALSE
	public		List	fNonSpecialUserCats
	public		List	fNonSpecialUserDocumentSubcats
	public		String	fProtocolSetName
	public		String	fRelationshipAttrName
	public		Assoc	fSpecialUserCats
	public		String	fSubAccountAttrName
	public		String	fSubCategoryAttrName
	public		Assoc	fUserGroupsMappedToDocumentSubcats


	/* 

		// if the user is in any group of section 1 then they get their section 1 group catsubcats
		// if the user is not in any group of section 1 then they get all section 2 catsubcats
		// everyone gets their section 3 groups catsubcats

		[SpecialUserCats]  (formerly Section1)
		G-Dorado-DebitsandCredits-User=Debits and Credits
		G-Dorado-Custody-User=Custody

		[NonSpecialUserCats]  (formerly [Section2])
		1=Client Authorization Forms
		2=Correspondence
		3=Custody
		4=Documents
		5=Debits and Credits
		6=Insurance
		7=Investments
		8=Oil and Gas
		9=Real Estate
		10=Taxes

		[NonSpecialUserDocumentSubcats]  (formerly [Section2])
		1=Account Closing
		2=Account Profile
		3=Accounting-RecptReleases
		4=Analysis of Trust Terms
		5=Balance Sheet
		6=Court Proceedings
		7=Crummey Letter
		8=Disc. Payments-Special Inv.
		9=Estate Planning Working Docs
		10=Family Tree
		11=Fees
		12=Governing Documents
		13=Know Your Client-Personal Papers
		14=Loans
		15=Relationship Acceptance
		16=Relationship Plan
		17=Trust Administrative Review
		18=Tax Forms-Restricted
		19=Quiet Trust
		20=Omnibus

		
		[UserGroupsMappedToDocumentSubcats] (formerly [Section3])
		G-Dorado-DebitCard-User=Debit Card Agreements
		G-Dorado-LoanDoc-User=Loans-Bessemer
		G-Dorado-MailerDesk-User=Mailer Updates
		G-Dorado-TaxForms-User=Tax Forms-Restricted
		G-Dorado-Compliance-User=Compliance-Restricted
	*/
	
	public function Assoc GetAllowedCatSubCats(Object prgCtx)

					// catsubcat permissions are in three sections
					// UserGroupsMappedToSubCats has groups mapped to subcats
					// section 2 has cat/subcats
					// section 3 has group:cat/subcats
					//
					// if the user is in any group of section 1 then they get their section 1 group catsubcats
					// if the user is not in any group of section 1 then they get all section 2 catsubcats
					// everyone gets their section 3 groups catsubcats


					// groups the user belongs to
					List usersGroups = RecArray.ColumnToList (UAPI.MembershipListByID (prgCtx.USession().fSession, prgCtx.USession().fUserID, UAPI.GROUP), 'Name')
					List allowedCatNames = {}
					List allowedDocSubCats
					Boolean isSpecialUser = false

					// is the user in one of 1st section groups?
					Assoc prefValues = .fSpecialUserCats
					String prefName
					for prefName in Assoc.Keys(prefValues)
						if prefName in usersGroups
							allowedCatNames = List.SetAdd(allowedCatNames, prefValues.(prefName))
						end
					end

					// if the user is not in any of the groups from SpecialGroupsMappedToSubCats
					// then we will set all the cats/subcats from a static list from the ini file
					if Length(allowedCatNames) > 0
						isSpecialUser = true
					else	
						allowedCatNames = .fNonSpecialUserCats
						allowedDocSubCats = .fNonSpecialUserDocumentSubcats
					end

					// UserGroupsMappedToDocumentSubcats
					prefValues = .fUserGroupsMappedToDocumentSubcats
					for prefName in Assoc.Keys(prefValues)
						if prefName in usersGroups
							allowedDocSubCats = List.SetAdd(allowedDocSubCats, prefValues.(prefName))

							if isSpecialUser
								// Make sure Documents is there
								allowedCatNames = List.SetAdd(allowedCatNames, "Documents")					
							end
						end
					end				

					allowedCatNames = List.Sort(allowedCatNames)
					allowedDocSubCats = List.Sort(allowedDocSubCats)

					Assoc result = Assoc{'allowedCatNames': allowedCatNames, 'allowedDocSubCats': allowedDocSubCats}


					return .Success(result)

				end


    public function Assoc GetGlobalGroups(Object prgCtx, Assoc attributesByName, Frame attrData)
    
        List groups

        Assoc status = ._GetInfoFromAttributes(prgCtx, attributesByName)
        if !status.ok
                        return status      
        end

        String frasSetName = .fFrasHierarchySetName
        String familyAttrName = .fFamilyAttrName
        String relationshipAttrName = .fRelationshipAttrName
        String accountAttrName = .fAccountAttrName
        String subaccountAttrName = .fSubAccountAttrName
        String categoryAttrName = .fCategoryAttrName
        String subcategoryAttrName = .fSubCategoryAttrName
        String protocolSetName = .fProtocolSetName

        List frasSet = attrData.GetFromSet(frasSetName, {familyAttrName, relationshipAttrName, accountAttrName, subaccountAttrName})
        List protocolSet = attrData.GetFromSet (protocolSetName, {categoryAttrName, subcategoryAttrName})

        // now get our configuration from the bessemer_permissions table
        String stmt = "select b.field, b.value, b.groupid, b.special, k.name from bessemer_permissions b, kuaf k where b.groupid = k.id order by b.special desc"
        RecArray recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, stmt)
        if IsError(recs)
                        return .Problem("Could not get permissions configuration from bessemer_permissions table")
        end

        // loop through the result                             
        Record rec
        for rec in recs

                        Boolean foundMatch = false
                        Assoc group = Assoc{'ID': rec.groupid, 'Name': rec.name, 'Special': rec.special}

                        // the field we want to look at in the attributes
                        String lookAtField = .fFieldNameMap.(rec.Field)

                        if IsUndefined(lookAtField)
                                        return .Problem (Str.Format ("unknown field: %1", rec.Field))
                        end

                        // the value we want to look for in the attributes
                        String lookForValue = rec.Value

                        // if it's a fras field, then see if the document is filed in that fras element
                        if lookAtField in .fFrasFieldList
                                        if ._IsFiledIn(frasSet, lookAtField, lookForValue)
                                                        groups = List.SetAdd (groups, group)
                                                        foundMatch = true
                                        end

                        // same thing for category                                                           
                        elseif lookAtField in .fCatSubCatFieldList                                                                                

                                        if ._HasCatSubCat(protocolSet, lookAtField, lookForValue)
                                                        groups = List.SetAdd (groups, group)
                                                        foundMatch = true
                                        end                                                        

                        elseif lookAtField == .fALLConstant

                                        // this is for the global group that gets access to all
                                        groups = List.SetAdd (groups, group)
                                        foundMatch = true

                        end

                        // if this is a special group, then we exit with just the one group
                        if rec.special > 0 && foundMatch
                                        break
                        end

        end


        return .Success(groups)
end




	override function Object New()

					Object o = Os.NewTemp(this)

					String		iniFile = $Kernel.ModuleUtils.ModuleIniPath("bessemer")

					// our config
					FilePrefs 	prefs = Fileprefs.Open( iniFile )
					if IsError(prefs) || IsUndefined(prefs)
						echoError(Str.Format("Error opening bessemer.ini file prefs at %1", iniFile))
					end
					Fileprefs.Close( prefs )

					// groups mapped to special categories
					Assoc prefVals = FilePrefs.ListValues (prefs, 'SpecialUserCats')
					if IsError(prefVals)
						echoError(Str.Format("Error getting values for SpecialUserCats from bessemer.ini file (%1)", iniFile))
					else
						o.fSpecialUserCats = prefVals			
					end

					// groups mapped to special document subcats
					prefVals = FilePrefs.ListValues (prefs, 'UserGroupsMappedToDocumentSubcats')
					if IsError(prefVals)
						echoError(Str.Format("Error getting values for UserGroupsMappedToDocumentSubcats from bessemer.ini file (%1)", iniFile))
					else
						o.fUserGroupsMappedToDocumentSubcats = prefVals			
					end		

					// users who aren't in any special groups will get these below
					prefVals = FilePrefs.ListValues (prefs, 'NonSpecialUserCats')
					if IsError(prefVals)
						echoError(Str.Format("Error getting values for NonSpecialUserCats from bessemer.ini file (%1)", iniFile))
					else
						o.fNonSpecialUserCats = Assoc.Items(prefVals)			
					end		

					// 
					prefVals = FilePrefs.ListValues (prefs, 'NonSpecialUserDocumentSubcats')
					if IsError(prefVals)
						echoError(Str.Format("Error getting values for NonSpecialUserCats from bessemer.ini file (%1)", iniFile))
					else
						o.fNonSpecialUserDocumentSubcats = Assoc.Items(prefVals)			
					end		

					echo("fSpecialUserCats", o.fSpecialUserCats)
					echo("fUserGroupsMappedToDocumentSubcats", o.fUserGroupsMappedToDocumentSubcats)
					echo("fNonSpecialUserCats", o.fNonSpecialUserCats)
					echo("fNonSpecialUserDocumentSubcats", o.fNonSpecialUserDocumentSubcats)

					return o

				end


	private function Assoc _GetInfoFromAttributes(Object prgCtx, Assoc attributesByName)

					if !.fInitialized

						String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
						if IsUndefined(fras)
							return .Problem ("fras attribute name not configured")
						end

						.fFrasHierarchySetName = fras

						if not attributesByName.(fras) or length (attributesByName.(fras).children) < 4
							return .Problem ("unexpected fras configuration")
						end

						.fFamilyAttrName = attributesByName.(fras).children[1]
						.fRelationshipAttrName = attributesByName.(fras).children[2]
						.fAccountAttrName = attributesByName.(fras).children[3]
						.fSubAccountAttrName = attributesByName.(fras).children[4]

						// also set an ordered list so we loop through these attributes in the right order in _IsFiledIn function
						.fFrasFieldList = {.fFamilyAttrName, .fRelationshipAttrName, .fAccountAttrName, .fSubAccountAttrName}

						String protocol = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol")
						if IsUndefined(protocol)
							return .Problem ("protocol attribute name not configured")
						end

						.fProtocolSetName = protocol

						if not attributesByName.(protocol) or length (attributesByName.(protocol).children) < 2
							return .Problem ("unexpected protocol configuration")
						end

						.fCategoryAttrName = attributesByName.(protocol).children[1]
						.fSubCategoryAttrName = attributesByName.(protocol).children[2]

						// also set cat subcat for _HasCatSubCat function
						.fCatSubCatFieldList = {.fCategoryAttrName, .fSubCategoryAttrName}

						// build our assoc that maps the table field name to the attributes name
						// quotes are unnecessary, but do make it stand out that these are string literals
						Assoc fieldNameMap
						fieldNameMap.("Family") = .fFamilyAttrName
						fieldNameMap.("Relationship") = .fRelationshipAttrName
						fieldNameMap.("Account") = .fAccountAttrName
						fieldNameMap.("Subaccount") = .fSubAccountAttrName
						fieldNameMap.("Category") = .fCategoryAttrName
						fieldNameMap.("Subcategory") = .fSubCategoryAttrName
						fieldNameMap.("*") = .fALLConstant

						.fFieldNameMap = fieldNameMap

						.fInitialized = true

					end	

					return .Success()
				end


	private function Boolean _HasCatSubCat(List protocolSet, String lookAtField, String lookForValue)

					Assoc a

					for a in protocolSet
						String thisVal = a.(lookAtField)

						if thisVal == lookForValue
							// if we got here, it means we found the value
							return true
						end
					end

					return false

				end


	private function Boolean _IsFiledIn(List frasSet, String lookAtField, String lookForValue)

					Assoc a

					for a in frasSet
						String thisVal = a.(lookAtField)

						if thisVal == lookForValue
							// we have. now make sure that the other values lower in the hierarchy are undefined
							String key
							Boolean foundLevel = false
							for key in .fFrasFieldList
								if foundLevel && IsDefined(a.(key))
									 break
								end
								foundLevel = (key == lookAtField)
							end	

							// if we got here, it means we found the value in the Family, Relationship, Account, or Subaccount
							return true
						end
					end

					return false

				end

end
