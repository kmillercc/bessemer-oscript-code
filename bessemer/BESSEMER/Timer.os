package BESSEMER

/**
 * 
 * Object myTimer = $blah.Timer.New()
 * 
 * // time an activity
 * myTimer.Start ('jumping up and down')
 * ...
 * myTimer.Stop()
 * 
 * 
 * // time a different activity
 * myTimer.Start ('running in circles')
 * ...
 * 
 * // implicitly switch to a new activity
 * myTimer.Start ('something else')
 * ...
 * 
 * // resume an earlier activity
 * myTimer.Start ('jumping up and down')
 * 
 * // report timings
 * String message
 * for message in myTimer.Messages()
 * 	echo (message)
 * end
 * 
 * // do your own analysis
 * echo (myTimer.totals)
 * 
 *  
 *  
 *  
 *  
 *  
 */
public Object Timer inherits KERNEL::#'Object'#

	public		Integer	_clock
	public		String	_name
	public		Dynamic	totals




	public function List Messages (String format = "%1: %2 millisecs")

							List messages

							String name
							for name in Assoc.Keys (.totals)
								messages = { @messages, Str.Format (format, name, .totals.(name)) }
							end

							return messages

						end


	public function Start (String name)

							.Stop ()

							._name = name
							._clock = Date.Tick()

						end


	public function Void Stop

							if ._name
								if .totals.(._name)
									.totals.(._name) += Date.Tick() - ._clock
								else
									.totals.(._name) = Date.Tick() - ._clock
								end
							end

						end

	Script Timer






												this.totals = Assoc.CreateAssoc()











	endscript

end
