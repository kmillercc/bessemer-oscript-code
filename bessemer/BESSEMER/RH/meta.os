package BESSEMER::RH

public Object meta inherits BESSEMER::RH::LLRequestHandlers

	public		Integer	ID
	public		Dynamic	attrData
	public		String	documentID
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'ID', 2, Undefined, FALSE }, { 'type', -1, Undefined, FALSE }, { 'nextURL', -1, Undefined, TRUE, 690 } }
	public		String	nextURL
	public		Dynamic	node
	public		String	type



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.fHTMLFile = "meta" + request.type + ".html"

							.ID = request.ID

							.node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, request.ID)
							if not .node
								.fError = Str.Format ("error accessing item: %1", .node)
								return undefined
							end		

							.attrData = $BESSEMER.CategoryAttributes.New (prgCtx, request.ID, .node.pVersionNum)
							if not .attrData.Load()
								.fError = "error accessing attributes"
								return undefined
							end


							// get the docID from the name 

							Integer left = Str.RChr (.node.pName, "[")
							Integer right = Str.RChr (.node.pName, "]")

							if left and right and right - left > 1
								.documentID = .node.pName[left + 1 : right - 1]
							end


							.nextURL = request.nextURL

							return undefined	
						end


	public function List Populated()

							List populated

							Object login = .PrgSession().fDbConnect.fLogin

							List exclude = { \
									"Vault", \
									CAPI.IniGet (login, 'BESSEMER', 'protocol'), \
									CAPI.IniGet (login, 'BESSEMER', 'hierarchy'), \
									CAPI.IniGet (login, 'BESSEMER', 'level'), \
									CAPI.IniGet (login, 'BESSEMER', 'contact') \
								}

							Object a = .attrData.attrData

							List category
							for category in Assoc.Keys (a.fDefinitions)
								Assoc attribute
								for attribute in a.fDefinitions.(category).Children

									continueif attribute.DisplayName in exclude

									Dynamic value = a.fData.(category).Values[1].(attribute.ID).Values[1]
									continueif not value
									continueif type (value) == Assoc.AssocType  // set

									populated = { @populated, attribute.DisplayName }
								end
							end

							return populated

						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "ID", IntegerType, Undefined, FALSE }, \
									{ "type", stringType, Undefined, FALSE }, \
									{ "nextURL", StringType, Undefined, true, $WebDsp.kNextURL } \
								}
						end


	public function List X (String name)

							switch Str.Lower (name)

								case 'name'
									return { .node.pName }
									end

								case '<now>'
									return { Date.Now() }
									end

								case '<family>', '<relationship>', '<account>', '<subaccount>'

									Assoc result = $BESSEMER.AttributesConfig.GetAttributes (.fPrgCtx)
									if not result.OK; return {}; end
									Assoc attributes = result.attributesByName	

									String fras = CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
									if IsUndefined(fras) or not attributes.(fras) or length (attributes.(fras).children) < 4; return {}; end

									String family       = attributes.(fras).children[1]
									String relationship = attributes.(fras).children[2]
									String account      = attributes.(fras).children[3]
									String subaccount   = attributes.(fras).children[4]


									List values

									Assoc data
									for data in .attrData.GetFromSet (fras, { family, relationship, account, subaccount })

										switch name

											case '<family>'
												if data.(family) and not data.(relationship)
													values = { @values, data.(family) }
												end
												end

											case '<relationship>'
												if data.(relationship) and not data.(account)
													values = { @values, data.(relationship) }
												end
												end

											case '<account>'
												if data.(account) and not data.(subaccount)
													values = { @values, data.(account) }
												end
												end

											case '<subaccount>'
												if data.(subaccount)
													values = { @values, data.(subaccount) }
												end
												end
										end

									end

									return values

									end

								case '<protocol>'

									Assoc result = $BESSEMER.AttributesConfig.GetAttributes (.fPrgCtx)
									if not result.OK; return {}; end
									Assoc attributes = result.attributesByName	

									String protocol = CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol")
									if IsUndefined(protocol) or not attributes.(protocol) or length (attributes.(protocol).children) < 2; return {}; end

									String category    = attributes.(protocol).children[1]
									String subcategory = attributes.(protocol).children[2]


									List values

									Assoc data
									for data in .attrData.GetFromSet (protocol, { category, subcategory })

										values = { @values, Str.Format ("%1 / %2", data.(category), data.(subcategory)) }	
									end

									return values

									end

								default
									return .attrData.Get (name)
									end

							end

						end

end
