package BESSEMER::RH

public Object Filer inherits BESSEMER::RH::LLRequestHandlers

	public		Assoc	fAttributes
	public		Assoc	fCatSubcats
	public		List	fConfig
	public		List	fContactCatSubcats
	public		Assoc	fData
	override	Boolean	fEnabled = TRUE
	public		List	fFRASCatSubcats
	public		String	fFile
	public		String	fHeaderCheckedString
	public		Dynamic	fLogFile
	public		String	fMode
	public		String	fNextUrl
	public		Integer	fNodeID
	public		Assoc	fProtocol
	override	List	fPrototype = { { 'operation', -1, Undefined, TRUE, 'add' }, { 'loadFrom', 2, Undefined, TRUE, Undefined }, { 'sheet', 5, Undefined, TRUE, FALSE }, { 'file', -1, Undefined, TRUE, Undefined }, { 'nextURL', -1, Undefined, TRUE, 690 } }
	public		String	fUploadCheckedString



	override function Dynamic Execute( Dynamic ctxIn, DynamicctxOut, Record	request )

				Object		prgCtx	= .PrgSession()

				.fNextUrl = request.nextUrl

				// what we get from the request
				String operation = request.operation
				Integer nodeID = request.loadFrom
				Boolean sheet = request.sheet
				String fileName = request.file


				Boolean isLoadFrom = IsDefined(nodeID)

				// set the mode based on request arguments
				if !.SetMode(operation, nodeID, sheet, fileName)
					return undefined
				end

				// set whether the header should be checked in the form or the upload
				if .fMode == 'repeatfiler' || RecArray.IsColumn (.fArgs, 'upload')
					.fUploadCheckedString = 'CHECKED'
				else	
					.fHeaderCheckedString = 'CHECKED'
				end

				// if we're going to load a file, ensure it exists
				if IsDefined(fileName)
					if !._CheckFile(prgCtx, fileName)
						return undefined
					end
					.fFile = fileName		
				end

				//--- attributes
				if !._SetAttributes(prgCtx)
					return undefined
				end

				//--- categories/subcategories
				if !._SetCatsSubcats(prgCtx)
					return undefined
				end

				// existing data to populate page
				if isLoadFrom
					Boolean isUpdate = operation == "update"
					if !._SetDataFromNode(prgCtx, nodeID, isUpdate)
						return undefined
					end	

				elseif !._SetDataFromRequest(request)
					return undefined

				end

				if !._SetConfig(prgCtx)
					return undefined
				end

				return undefined
			end


	public function Boolean SetMode(String operation, Integer nodeID, Boolean isSheet, String fileName)

				// what mode?

				/*
				             loadfrom  operation  sheet   file
				             (none)    ("add")    (none)  (none)
				Modes
				filer                  add
				refiler      loadfrom  update     
				resheet      loadfrom  update     sheet
				repeatfiler  loadfrom  add
				repeatsheet  loadfrom  add        sheet
				standalone             add                file

				*/

				Boolean isLoadFrom = IsDefined(nodeID)
				Boolean hasFile = IsDefined(fileName)

				// load from should not have file		
				if isLoadFrom
					switch operation
						case "add"
							if isSheet
								.fMode = "repeatsheet"
							else
								.fMode = "repeatfiler"	
							end
						end

						case "update"
							if isSheet
								.fMode = "resheet"
							else
								.fMode = "refiler"	
							end				
						end	
					end					

				elseif operation == "add"

					if hasFile
						.fMode = "standalone"
					else
						.fMode = "filer"
					end			

				else				
					.fError = "unknown argument combination"
					return false			
				end

				/*
					if not request.loadFrom and request.operation == "add" and not request.sheet and not request.file
						.mode = "filer"

					elseif request.loadFrom and request.operation == "update" and not request.sheet and not request.file
						.mode = "refiler"

					elseif request.loadFrom and request.operation == "update" and request.sheet and not request.file
						.mode = "resheet"

					elseif request.loadFrom and request.operation == "add" and not request.sheet and not request.file
						.mode = "repeatfiler"

					elseif request.loadFrom and request.operation == "add" and request.sheet and not request.file
						.mode = "repeatsheet"

					elseif not request.loadFrom and request.operation == "add" and not request.sheet and request.file
						.mode = "standalone"

					else
						.fError = "unknown argument combination"
						return
					end 
		 		*/

				return true
			end


	override function void SetPrototype()

				.fPrototype =\
					{\
						{ "operation", StringType, Undefined, true, "add" }, \
						{ "loadFrom", IntegerType, Undefined, true, Undefined }, \
						{ "sheet", BooleanType, Undefined, true, false }, \
						{ "file", StringType, Undefined, true, Undefined }, \
						{ "nextURL", StringType, Undefined, true, $WebDsp.kNextURL } \
					}
			end


	override function SubclassExecuteComponents()

				String modeDisplay = "Filer"
				String gif = 'icon_add-create.gif'

				if .fMode == 'refiler'
					modeDisplay =  "Refiler"
					gif = 'icon_edit.gif'
				end

				Object guiComponents = .fGUIComponents
				Object	title = guiComponents.title
				Object	livelink = guiComponents.livelink
				Object 	pageManager = .GetPageManager()

				//	guiComponents.search.SetIsEnabled (false)

				title.SetPictogram (.Img() + gif)

				title.SetTitle1 (modeDisplay)
				//	title.SetPictogramAlt (modeDisplay)

				livelink.SetContentHTMLFile( .ModHTMLPrefix(), .fHTMLFile )

				pageManager.SetContentComponents( { livelink } )

				pageManager.SetTitleString (modeDisplay)

			end


	private function Boolean _CheckFile(Object prgCtx, String filename)

					String path = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'uploadFolderPath')
					if IsUndefined(path)
						.fError = "upload directory not configured"
						return false
					end

					if !File.Exists (path + File.Separator() + filename)
						.fError = Str.Format ("No such file '%1' in upload directory", fileName)
						return false
					end	

					return true	

				end


	private function List _ConvertValue (String typeStr, Dynamic value)

				switch typeStr

					case 'Boolean'
						return { true }
					end

					case 'Date'
						return { Date.StringToDate (value, '%m/%d/%Y') }
					end

					case 'Integer', 'IntegerPopup'
						return { Str.StringToInteger (value) }
					end

					case 'Real'
						return { Str.StringToReal (value) }
					end

					default
						return { value }
					end
				end

			end


	private function Boolean _SetAttributes(Object prgCtx)

				Assoc result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx)
				if !result.OK
					.fError = result.ErrMsg
					return false
				end	

				.fAttributes = result.attributes

				return true
			end


	private function Boolean _SetCatsSubcats(Object prgCtx)

			    Assoc catsSubCats
			    Assoc protocol
				List contactCatSubcats
				List frasCatSubcats


				Assoc status = $Bessemer.PermissionsConfig.GetAllowedCatSubCats(prgCtx)
				if !status.ok
					.fError = status.errMsg
					return false
				end

				// this is the list of allowed category names
				List allowedCatNames = status.result.allowedCatNames

				// this is the list of allowed document subcategory names
				List allowedDocSubCats = status.result.allowedDocSubCats

				Record rec
				String stmt = "select * from BESSEMER_CategorySubcategory where SystemOnly = 0 order by Category, Subcategory" 
				RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt)

				if IsError(recs)
					.fError = "Error getting config from BESSEMER_CategorySubcategory table"
					return false
				end

				for rec in recs
					String catName = rec.Category
					String subcatName = rec.Subcategory

					if (catName in allowedCatNames)

						// set up a list for this category name key in the Assoc
						List thisCatList = IsFeature(catsSubCats, catName) ? catsSubCats.(catName) : {}

						// Special handling for Documents category -- this is the only one where the subcategories are restricted
		                if IsDefined(subCatName) 
			            	if catName != "Documents" || subcatName in allowedDocSubCats
								thisCatList = List.SetAdd(thisCatList, subcatName)
						    end				 
						end		 

						// save the list into Assoc
						catsSubCats.(catName) = thisCatList

						// save a list of contact categories and subcategoried with optional permission flag (0) 
						contactCatSubcats = List.SetAdd(contactCatSubcats, { catName, subcatName, 0 })

						// save a list of fras categories and subcategoried with optional permission flag
						frasCatSubcats  = List.SetAdd(frasCatSubcats, { catName, subcatName, 0 } )

						// save the protocol with some permisions  bit mask business
						List protocolKey = { catName, subcatName }
						protocol.(protocolKey) = { 	rec.FRAS & 16 ? true : false, \
													rec.FRAS & 8 ? true : false, \
													rec.FRAS & 4 ? true : false, \
													rec.FRAS & 2 ? true : false, \
													rec.FRAS & 1 ? true : false }


					end


				end	

				// set the features based on what we came up with	
				.fContactCatSubcats = contactCatSubcats
				.fFRASCatSubcats = frasCatSubcats
				.fCatSubcats = catsSubCats
				.fProtocol = protocol

				return true		
			end


	private function Boolean _SetConfig(Object prgCtx)

				Assoc status = $Bessemer.AttributesConfig.GetAttributesConfig(prgCtx, .fAttributes, .fContactCatSubcats, .ffRASCatSubcats)

				if status.ok
					.fConfig = status.result
				else		
					.fError = status.errMsg
					return false
				end																	 															 

				return true
			end


	private function Boolean _SetDataFromNode(Object prgCtx, Integer nodeID, Boolean isUpdate)

				Assoc data

				//--- from something being loaded

				.fNodeID = nodeID

				DAPINode node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, nodeID)
				if IsError(node) && isUpdate
					.fError = "Unable to access document: " + Str.String (node)
					return false
				end

				Frame attrdata = $BESSEMER.CategoryAttributes.New (prgCtx, nodeID, node.pVersionNum)
				if not attrdata.Load()
					.fError = "Unable to load existing data"
					return false
				end

				// can't refile ones marked as System Entered

				if .fMode == "refiler" and attrdata.Get ("System Entered") and attrdata.Get ("System Entered")[1]
					.fError = "Can't refile a System Entered document"
					return false
				end		


				// handle any special attributes			

				Assoc attribute
				for attribute in .fAttributes

					if attribute.name == CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol")

						continueif length (attribute.children) != 2

						String categoryName = attribute.children[1]
						String subcategoryName = attribute.children[2]

						List protocols = attrdata.GetFromSet (attribute.name, { \
								categoryName, \
								subcategoryName \
							})


						// users should not be able to refile a document that has a system category/subcategory set

						Assoc protocol
						for protocol in protocols
							if not (Assoc.IsKey (.fCatSubcats, protocol.(categoryName)) and \
									protocol.(subcategoryName) in .fCatSubcats.(protocol.(categoryName)))
								.fError = "Document has been system-generated; cannot edit"
								return undefined
							end
						end 


						data.(attribute.ID) = protocols

					elseif attribute.name == CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")

						continueif length (attribute.children) != 4

						data.(attribute.ID) = attrdata.GetFromSet (attribute.name, { \
								attribute.children[1], \
								attribute.children[2], \
								attribute.children[3], \
								attribute.children[4] \
							})

					elseif attribute.name == "Name"

						// strip [ID]

						String name = node.pName

						Integer left = Str.RChr (name, "[")
						Integer right = Str.RChr (name, "]")

						if left and right and right - left > 1
							name[left : right] = ''
						end

						data.(attribute.ID) = { Str.Trim (name) }

					else

						data.(attribute.ID) = attrdata.Get (attribute.name)
					end
				end

				.fData = data
				return true

			end


	private function Boolean _SetDataFromRequest(Record request)

				Assoc data

				// we're not loading an existing file, so accept values from the request
				Assoc attribute
				for attribute in .fAttributes

					String parameterName = 'x' + Str.Collapse (attribute.name)

					continueif not RecArray.IsColumn (request, parameterName)

					if attribute.name == CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol") or \
							attribute.name == CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")

						List values = Str.Elements (request.(parameterName), ",")
						if length (values) != length (attribute.children)
							.fError = "invalid value for parameter: " + parameterName
							return false
						end

						Assoc valuesAssoc

						Integer i
						for i = 1 to length (attribute.children)  // and values
							valuesAssoc.(attribute.children[i]) = values[i]
						end

						data.(attribute.ID) = { valuesAssoc }

					else

						data.(attribute.ID) = ._ConvertValue (attribute.type, request.(parameterName))

					end

				end

				.fData = data		
				return true
			end

end
