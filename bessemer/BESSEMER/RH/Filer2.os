package BESSEMER::RH

public Object Filer2 inherits BESSEMER::RH::LLRequestHandlers

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'operation', -1, Undefined, FALSE }, { 'nodeID', 2, Undefined, TRUE, 0 }, { 'file', -1, Undefined, TRUE, Undefined }, { 'headerSheet', 5, Undefined, TRUE, FALSE }, { 'nextURL', -1, Undefined, TRUE, 690 } }



	override function Dynamic Execute(Dynamic ctxIn, Dynamic ctxOut, Record	request )

				Object		prgCtx	= .PrgSession()

				Frame 	timer = $BESSEMER.Timer.New()

				// defined attributes by name
				Assoc status = $BESSEMER.AttributesConfig.GetAttributes (prgCtx, true)
				if status.OK

					Assoc attributesByName = status.attributesByName
					Integer categoryID = status.categoryID

					//--- perform operation
					if request.operation == "update"

						.StartTransaction()
						._DoUpdate(prgCtx, request, categoryID, attributesByName)
						.EndTransaction()

					elseif request.operation == 'add' && request.headerSheet 

		 				.StartTransaction()
						._DoCreateHeaderSheet(prgCtx, request, categoryID, attributesByName, timer)
						.EndTransaction()

					elseif request.operation == 'add'	

		 				.StartTransaction()			
						._DoCreateRegularFile(prgCtx, request, categoryID, attributesByName, timer)
						.EndTransaction()
					else
						.fError = Str.Format ("Unknown operation: %1", request.operation)
						return undefined

					end

				else	
					.fError = status.ErrMsg
					return undefined
				end		

				._LogTime (prgCtx, timer.totals)

				return undefined
			end


	override function void SetPrototype()

					.fPrototype =\
						{\
							{ "operation", StringType, Undefined, false }, \
							{ "nodeID", IntegerType, Undefined, true, 0 }, \
							{ "file", StringType, Undefined, true, Undefined }, \
							{ "headerSheet", BooleanType, Undefined, true, false }, \
							{ "nextURL", StringType, Undefined, true, $WebDsp.kNextURL } \
						}
				end


	private function Void _DoCreateHeaderSheet(Object prgCtx, Record request, Integer categoryID, Assoc attributesByName, Frame timer)

					// header sheets location ("buckets")
					Integer bucketFolderID = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'bucket')
					if IsDefined(bucketFolderID)

						// now add the file			
						Assoc	status = $Bessemer.AddHeaderSheet.AddFile(prgCtx, request, categoryID, attributesByName, bucketFolderID, timer)
						if !status.ok
							.fError	= status.errMsg
						else
							DAPINODE node = status.result

							// show meta
							.fLocation = Str.Format ("%1?func=bessemer.meta&ID=%2&type=%3&nextUrl=%4", \
									.URL(), \
									node.pID, \
									"sheet", \
									Web.Escape (request.nextUrl))


						end

					else	
						.fError = "Bucket folder is not configured"
					end		



				end


	private function Void _DoCreateRegularFile(Object prgCtx, Record request, Integer categoryID, Assoc attributesByName, Frame timer)

					// create in the repository
					Integer repositoryID = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'repository')
					if IsDefined(repositoryID)

						// now add the file			
						Assoc	status = $Bessemer.AddRegularFile.AddFile(prgCtx, request, categoryID, attributesByName, repositoryID, timer)
						if !status.ok
							.fError	= status.errMsg
						else
							DAPINODE node = status.result

							// show meta
							.fLocation = Str.Format ("%1?func=bessemer.meta&ID=%2&type=%3&nextUrl=%4", \
									.URL(), \
									node.pID, \
									"document", \
									Web.Escape (request.nextUrl))


						end

					else	
						.fError = "Repository is not configured"
					end				

				end


	private function Void _DoUpdate(Object prgCtx, Record request, Integer categoryID, Assoc attributesByName)

					// get the node	
					DAPINODE node = DAPI.GetNodeById (prgCtx.DAPISess(), 0, request.nodeID)

					if IsNotError(node)

						// now update			
						Assoc	status = $Bessemer.UpdateFileHandler.UpdateFile(prgCtx, request, categoryID, attributesByName, node)
						if !status.ok
							.fError	= status.errMsg
						else

							// show meta
							.fLocation = Str.Format ("%1?func=bessemer.meta&ID=%2&type=%3&nextUrl=%4&redo", \
										.URL(), \
										node.pID, \
										"document", \
										Web.Escape (request.nextUrl))

						end			

					else	
						.fError = "Cannot access item: " + Str.String (node)
					end

				end


	private function _IDName (Integer ID)

					String name = Str.Format ("%1", ID)
					while length (name) < 9
						name = "0" + name
					end

					return "3" + name

				end


	private function _LogTime ( Object prgCtx, Assoc times )

					Integer totalTime

					String key
					for key in Assoc.Keys (times)
						totalTime += times.(key)
					end

					if totalTime < CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'maxFilerTime', 1000) * 1000
						return undefined // too fast to worry about
					end

					if not $BESSEMER.TimeLogFile  // first time here?
						String dir = Str.Format ("%1%2logs%3filer%4", $Kernel.SystemPreferences.GetPrefGeneral( "OTHOME" ), File.Separator(), File.Separator(), File.Separator())
						File.Create (dir)	
						$BESSEMER.TimeLogFile = File.Open (Str.Format ("%1FilerLog%2.txt", dir, System.ThreadIndex()), File.AppendMode)
					end

					if $BESSEMER.TimeLogFile

						String message = Str.Format ("Total=%1", totalTime)

						for key in Assoc.Keys (times)
							message += Str.Format (",%1=%2", key, times.(key))
						end

						File.Write ($BESSEMER.TimeLogFile, \
								Str.Format ("%1 %2 %3 %4", \
									Date.DateToString (Date.Now(), "%Y-%m-%d %H:%M:%S"), \
									prgCtx.USession().fUserID, \
									prgCtx.USession().fUserName, \
									message \
								) \
							)
					end
				end

end
