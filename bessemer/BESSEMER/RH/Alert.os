package BESSEMER::RH

public Object Alert inherits BESSEMER::RH::LLRequestHandlers

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'message', -1, '', FALSE }, { 'nextURL', -1, '', TRUE, 690 } }
	public		String	message
	public		String	nextURL



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							.message = request.message
							.nextURL = request.nextURL

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "message", StringType, "", FALSE }, \
									{ "nextURL", StringType, "", TRUE, $WebDsp.kNextURL } \
								}
						end

end
