package BESSEMER::RH::JSON

public Object loadFRASCancestry inherits BESSEMER::RH::JSON::loadFRASChierarchy

	override Script Execute







												function Dynamic Execute( \
														Dynamic		ctxIn, \
														Dynamic		ctxOut, \
														Record		request \
													)

													Object prgCtx = .PrgSession()
													Integer i	
													Frame timer = $BESSEMER.Timer.New()

													List levels = { "families", "relationships", "accounts", "subaccounts", "contacts" }


													// populate features with accessible/not-accessible for each node

													for i = 1 to 5
														.(levels[i]) = Assoc.CreateAssoc()
													end


													// what kind of fras are we?  set level in <<at>>

													Integer at = request.fras in { "family", "relationship", "account", "subaccount" }
													if not at
														.error = "invalid fras parameter"
														return undefined
													end


													// find the ancestry of the item by making a query that joins at each level

													String sqlStr = "select"
													for i = 1 to at
														sqlStr += Str.Format (" DTree%1.Name name%1, DTree%1.DataID id%1,", i)
													end

													sqlStr += " 0 from DTree DTree0"

													for i = 1 to at
														sqlStr += Str.Format (" join DTree DTree%1 on DTree%1.ParentID = DTree%2.DataID and DTree%1.SubType = %3", \
																i, i-1, $TypeVirtualFolder )
													end

													sqlStr += Str.Format (" where DTree0.DataID = :A1 and DTree%1.Name = :A2", at)

													timer.Start ('ancestry query')	
													RecArray ancestry = CAPI.Exec (prgCtx.fDBConnect.fConnection, sqlStr, RootID ("hierarchy"), request.item)
													if not .CheckError (ancestry); return undefined; end
													timer.Stop ()

													if length (ancestry) == 0
														.error = "no ancestry for item"
														return undefined
													end

													// (ancestry[1] now has: name1=AddamsFamily, id1=123, name2=Morticia, id2=456, 3..., 4... etc.)


													// store ancestry names with accessibility

													for i = 1 to at - 1
														String name = ancestry[1].(Str.Format ("name%1", i))

														// can this user access the item with this name in My...

														DAPINode rootNode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, RootID (levels[i]), false)

														Boolean isFamily = (i == 1)

														.(levels[i]).(name) = rootNode and \
																IsNotError (DAPI.GetNode (name, rootNode, false)) and \
																(!isFamily || .IsFileableFamily(name))
																// name != 'PROSPECTIVE [000]'  // special case, hard coded (KM 3-9-20 now replaced with IsFileableFamily)
													end

												/************************** descendants code was here ***********************/	

													.timings = timer.totals	

													return undefined
												end


												/**************** contacts code was here **************/



												function Integer RootID (String level)

													Object prgCtx = .PrgSession()

													Assoc roots

													roots.families 		= "myFamilies"
													roots.relationships = "myRelationships"
													roots.accounts 		= "myAccounts"
													roots.subaccounts 	= "mySubaccounts"	
													roots.contacts 		= "myContacts"
													roots.hierarchy		= "hierarchyRoot"

													if not roots.(level)
														.error = "invalid level"
														return Undefined
													end

													Integer ID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', roots.(level))
													if IsUndefined(ID)
														.error = level + " not configured"
														return Undefined
													end

													return ID
												end



												/************ AccessibleByName code was here ***************/











	endscript

end
