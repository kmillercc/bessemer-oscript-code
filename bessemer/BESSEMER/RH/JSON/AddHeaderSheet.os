package BESSEMER::RH::JSON

/**
 * 
 * creates a header sheet
 * 
 * metadata is supplied as arguments whose names are "x" prefixing attribute names
 * 
 *  
 *  
 *  
 *  
 *  
 */
public Object AddHeaderSheet inherits BESSEMER::RH::JSON::JSON

	public		String	barcode
	override	Boolean	fEnabled = TRUE
	override	String	fHTMLMimeType = 'text/plain'
	public		Integer	id
	public		String	name



	override Script Execute






												function Dynamic Execute( \
													Dynamic		ctxIn, \
													Dynamic		ctxOut, \
													Record		request )

													.StartTransaction()

													AddHeaderSheet (request)

													.EndTransaction()

													return undefined

												end



												function AddHeaderSheet (Record request)

													Object prgCtx = .PrgSession()


													// header sheets location ("buckets")

													Integer bucketFolderID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'bucket')
													if not bucketFolderID
														.error = "Bucket folder is not configured"
														return undefined
													end

													DAPINode bucketFolder = DAPI.GetNodeById (prgCtx.DAPISess(), 0, bucketFolderID )
													if IsError(bucketFolder)
														.error = "bucketFolder is not accessible: " + Str.String (bucketFolder)
														return undefined
													end


													// defined attributes

													Assoc result

													result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx, true)
													if not result.OK
														.error = result.ErrMsg
														return undefined
													end

													Assoc attributes = result.attributesByName


													Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, bucketFolderID, 0)
													if not attrData.Load()
														.error = "Error accessing bucketFolder attributes"
														return undefined
													end


													// check config

													String specialAttribute
													for specialAttribute in { "fras", "level", "protocol", "filedBy" }

														if not CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', specialAttribute)
															.error = specialAttribute + " not configured"
															return undefined
														end

														if not attributes.(CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', specialAttribute))
															.error = "no attribute/set for " + specialAttribute
														end
													end


													//--- process attributes

													attrData.SetValue(CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'filedBy'), { prgCtx.USession().fUsername })


													Assoc attribute
													for attribute in attributes

														String parameterName = 'x' + Str.Collapse (attribute.name)

														continueif not RecArray.IsColumn (request, parameterName)


														if attribute.name == CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'fras')

															Assoc valuesAssoc

															Integer i
															for i = 1 to length (attribute.children)
																valuesAssoc.(attribute.children[i]) = {}
															end

															String valueSet
															for valueSet in RecArray.IsColumn (request, parameterName + "_list") ? \
																	request.(parameterName + "_list") : { request.(parameterName) }

																List values = Str.Elements (valueSet, CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'separator', ','))

																for i = 1 to length (attribute.children)
																	valuesAssoc.(attribute.children[i]) = { @valuesAssoc.(attribute.children[i]), i <= length (values) ? values[i] : '' }
																end
															end

															attrData.SetInSet (attribute.name, valuesAssoc)


															// compute levels

															List levelValues = List.Allocate (length (valuesAssoc[1]))

															for i = 1 to length (levelValues)
																String level
																for level in attribute.children

																	if valuesAssoc.(level) and valuesAssoc.(level)[i]
																		levelValues[i] = level
																	end
																end
															end

															if not attrData.SetValue(CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'level'), levelValues)
																.error = 'missing Level attribute'
																return undefined
															end


														elseif attribute.name == CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'protocol')

															Assoc valuesAssoc

															Integer i
															for i = 1 to length (attribute.children)
																valuesAssoc.(attribute.children[i]) = {}
															end

															String valueSet
															for valueSet in RecArray.IsColumn (request, parameterName + "_list") ? \
																	request.(parameterName + "_list") : { request.(parameterName) }

																List values = Str.Elements (valueSet, CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'separator', ','))

																for i = 1 to length (attribute.children)
																	valuesAssoc.(attribute.children[i]) = { @valuesAssoc.(attribute.children[i]), i <= length (values) ? values[i] : '' }
																end
															end

															attrData.SetInSet (attribute.name, valuesAssoc)


														else

															attrData.SetValue(attribute.name, Values (attribute.type, request, parameterName))

														end
													end


													//--- perform operation

													if not request.xName
														.error = "Name must be supplied"
														return undefined
													end	


													DAPINode newNode

													Assoc createInfo
													createInfo.attrDataObj = attrData.attrData


													// allocate and create folder with metadata assigned

													Object llnode = $LLIAPI.LLNodeSubsystem.GetItem ($TypeFolder)

													result = llnode.NodeAlloc (bucketFolder, request.xName)
													if not result.OK
														.error = result.errMsg
														return undefined
													end

													result = llnode.NodeCreate (result.node, bucketFolder, createInfo)
													if not result.OK
														.error = result.errMsg
														return undefined
													end

													newNode = result.node

													.id = newNode.pID


													// rename using ID

													String barcode = Str.Format ("%1", newNode.pID)
													while length (barcode) < 9
														barcode = "0" + barcode
													end

													barcode = "3" + barcode

													.barcode = barcode

													Integer renamed = DAPI.RenameNode (newNode, Str.Format ("%1 [%2]", request.xName, barcode))
													if IsError (renamed)
														.error = "Error renaming: " + Str.String (renamed)
														return undefined
													end

													.name = newNode.pName


													// set barcode into attribute

													String barcodeAttribute = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'barcode')
													if barcodeAttribute
														attrData = $BESSEMER.CategoryAttributes.New (prgCtx, newNode.pID, newNode.pVersionNum)
														attrData.Load()
														if attrData.SetValue(barcodeAttribute, { barcode })
															attrData.Save()
														end
													end


													// remove assigned groups permissions
													Assoc status = $BESSEMER.Utils.RemoveACLs(newNode)
													if !status.ok
														.error = status.errMsg
														return undefined
													end											

												end



												function List Values (String typeStr, Record data, String name)

													List raw = RecArray.IsColumn (data, name + "_list") ? data.(name + "_list") : { data.(name) }

													Integer i
													for i = 1 to length (raw)

														switch typeStr

															case 'Boolean'
																raw[i] = true
																end

															case 'Date'
																raw[i] = Date.StringToDate (raw[i], '%m/%d/%Y')
																end

															case 'Integer', 'IntegerPopup'
																raw[i] = Str.StringToInteger (raw[i])
																end

															case 'Real'
																raw[i] = Str.StringToReal (raw[i])
																end
														end
													end

													return raw

												end











	endscript

end
