package BESSEMER::RH::JSON

/**
 * 
 * returns names of items in a MyMyMy folder, filtered by substring match
 * 
 * returns
 * 
 * [a,b,c] - for items a, b, and c
 *  or
 * null - if threshold exceeded
 * 
 *  
 *  
 *  
 *  
 *  
 */
public Object lookupFRASC inherits BESSEMER::RH::JSON::JSON

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'frasc', -1, Undefined, FALSE }, { 'lookup', -1, Undefined, TRUE, '' }, { 'wantgui', 5, Undefined, FALSE, FALSE } }
	public		Dynamic	items




	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()


							// we need to return a list with an identified subset
							// 
							// the list contains all items the user can browse
							// the subset are those items the user has explicit permission to access
							//
							// the list is obtain from the hierarchy
							// the subset is obtained from mymymy


							// what kind of frasc are we to use?

							Assoc roots
							roots.family 		= "myFamilies"
							roots.relationship 	= "myRelationships"
							roots.account 		= "myAccounts"
							roots.subaccount 	= "mySubaccounts"	
							roots.contact 		= "myContacts"

							String myRootName = roots.(request.frasc)
							if IsUndefined(myRootName)
								.error = "invalid frasc argument"
								return undefined
							end


							// find myfrasc

							Integer myRootID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', myRootName)
							if IsUndefined(myRootID)
								.error = myRootName + " not configured"
								return undefined
							end


							// get names of every mymymy that's visible

							RecArray records

							String sqlStr = "select Name, DataID, OwnerID, PermID from DTree where ParentID = :A1 and SubType = :A2"

							if request.lookup
								records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										sqlStr + " and UPPER (DTree.Name) like :A3", \
										myRootID, $TypeVirtualFolder, '%' + Str.Upper (request.lookup) + '%' \
									)
							else
								records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										sqlStr, \
										myRootID, $TypeVirtualFolder \
									)
							end

							if not prgCtx.fDBConnect.CheckError (records).OK; return undefined; end

							// check permissions

							records = $BESSEMER.Utils.FilterByPermissionToSee (prgCtx, records)
							if IsError(records) || IsUndefined(records) // time out
								.items = Undefined
								return undefined
							end

							List visibleNames = RecArray.ColumnToList (records, "Name")


							// subaccounts and contacts don't need further checking, so we return them now

							if request.frasc in {"subaccount", "contact"}
								.items = Assoc.CreateAssoc()

								String name
								for name in visibleNames
									.items.(name) = true
								end

								return undefined
							end


							// get names of every fra entry that's browseable from the hierarchy root

							Integer root = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "hierarchyRoot")
							if not root
								.error = "hierarchyRoot not configured"
								return undefined
							end


							switch request.frasc

								case "family"
									sqlStr = "select DTree.Name, DTree.DataID, DTree.OwnerID from DTree" \
										+ " where DTree.SubType = :A1" \
										+ " and DTree.ParentID = :A2"

									if request.lookup
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr + " and UPPER (DTree.Name) like :A3", \
												$TypeVirtualFolder, root, '%' + Str.Upper (request.lookup) + '%' \
											)
									else
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr, \
												$TypeVirtualFolder, root \
											)
									end
								end

								case "relationship"
									sqlStr = "select DTree.Name, DTree.DataID, DTree.OwnerID from DTree" \
										+ " join DTree family on DTree.ParentID = family.DataID" \
										+ " where DTree.SubType = :A1" \
										+ " and family.SubType = :A2" \
										+ " and family.ParentID = :A3"

									if request.lookup
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr + " and UPPER (DTree.Name) like :A4", \
												$TypeVirtualFolder, $TypeVirtualFolder, root, '%' + Str.Upper (request.lookup) + '%' \
											)
									else
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr, \
												$TypeVirtualFolder, $TypeVirtualFolder, root \
											)
									end
								end

								case "account"
									sqlStr  = "select DTree.Name, DTree.DataID, DTree.OwnerID from DTree" \
										+ " join DTree relationship on DTree.ParentID = relationship.DataID" \
										+ " join DTree family on relationship.ParentID = family.DataID" \
										+ " where DTree.SubType = :A1" \
										+ " and relationship.SubType = :A2" \
										+ " and family.SubType = :A3" \
										+ " and family.ParentID = :A4"

									if request.lookup
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr + " and UPPER (DTree.Name) like :A5", \
												$TypeVirtualFolder, $TypeVirtualFolder, $TypeVirtualFolder, root, '%' + Str.Upper (request.lookup) + '%' \
											)
									else
										records = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												sqlStr, \
												$TypeVirtualFolder, $TypeVirtualFolder, $TypeVirtualFolder, root \
											)
									end
								end

								default
									.fError = "unknown frasc"
									return undefined
								end
							end

							if not prgCtx.fDBConnect.CheckError (records).OK; return undefined; end

							// check permissions

							records = $BESSEMER.Utils.FilterByPermissionToSee (prgCtx, records)
							if IsUndefined(records) // time out
								.items = Undefined
								return undefined
							end

							List browsableNames = RecArray.ColumnToList (records, "Name")

							Boolean 	isFamily = Str.Lower(request.frasc) == 'family'

							.items = Assoc.CreateAssoc()
							String name
							for name in browsableNames

								Boolean isVisible = (name in visibleNames)		

								// KM 3-9-20 replaced this previous code: .items.(name) = not not (name in names)
								.items.(name) = isVisible && (!isFamily || .IsFileableFamily(name))

							end


							// special case, hard coded
							// KM 3-9-20 now no need for this
							/*						
							if Assoc.IsKey (.items, 'PROSPECTIVE [000]')
								.items.('PROSPECTIVE [000]') = false
							end
							*/

							return undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "frasc", StringType, Undefined, false }, \
									{ "lookup", StringType, Undefined, true, "" }, \
									{ "wantgui", BooleanType, Undefined, false, false } \
								}
						end

end
