package BESSEMER::RH::JSON

/**
 * * 
 * for a given node in the f/r/a/s hierarchy returns ancestry and descendants, each with accessible indicator, and contacts
 * * 
 *  
 *  
 *  
 *  
 *  
 *  
 */
public Object loadFRASChierarchy inherits BESSEMER::RH::JSON::JSON

	public		Dynamic	accounts
	public		Dynamic	contacts
	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'loadfrasc.html'
	override	List	fPrototype = { { 'fras', -1, Undefined, FALSE }, { 'item', -1, Undefined, FALSE }, { 'wantgui', 5, Undefined, FALSE, FALSE } }
	public		Dynamic	families
	public		Dynamic	relationships
	public		Dynamic	subaccounts
	public		Dynamic	timings



	override Script Execute




				            function Dynamic Execute( \
				                        Dynamic           ctxIn, \
				                        Dynamic           ctxOut, \
				                        Record            request \
				                  )

				                  Object prgCtx = .PrgSession()
				                  Integer i   
				                  Frame timer = $BESSEMER.Timer.New()

				                  List levels = { "families", "relationships", "accounts", "subaccounts", "contacts" }


				                  // populate features with accessible/not-accessible for each node

				                  for i = 1 to 5
				                        .(levels[i]) = Assoc.CreateAssoc()
				                  end


				                  // what kind of fras are we?  set level in <<at>>

				                  Integer at = request.fras in { "family", "relationship", "account", "subaccount" }
				                  if not at
				                        .error = "invalid fras parameter"
				                        return undefined
				                  end


				                  // find the ancestry of the item by making a query that joins at each level

				                  String sqlStr = "select"
				                  for i = 1 to at
				                        sqlStr += Str.Format (" DTree%1.Name name%1, DTree%1.DataID id%1,", i)
				                  end

				                  sqlStr += " 0 from DTree DTree0"

				                  for i = 1 to at
				                        sqlStr += Str.Format (" join DTree DTree%1 on DTree%1.ParentID = DTree%2.DataID and DTree%1.SubType = %3", \
				                                    i, i-1, $TypeVirtualFolder )
				                  end

				                  sqlStr += Str.Format (" where DTree0.DataID = :A1 and DTree%1.Name = :A2", at)

				                  timer.Start ('ancestry query')     
				                  RecArray ancestry = CAPI.Exec (prgCtx.fDBConnect.fConnection, sqlStr, RootID ("hierarchy"), request.item)
				                  if not .CheckError (ancestry); return undefined; end
				                  timer.Stop ()

				                  if length (ancestry) == 0
				                        .error = "no ancestry for item"
				                        return undefined
				                  end

				                  // (ancestry[1] now has: name1=AddamsFamily, id1=123, name2=Morticia, id2=456, 3..., 4... etc.)


				                  // store ancestry names with accessibility

				                  for i = 1 to at - 1
				                        String name = ancestry[1].(Str.Format ("name%1", i))

				                        // can this user access the item with this name in My...

				                        DAPINode rootNode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, RootID (levels[i]), false)

				                        Boolean isFamily = (i == 1)

				                        .(levels[i]).(name) = rootNode and \
				                                    IsNotError (DAPI.GetNode (name, rootNode, false)) and \
				                                    !isFamily || .IsFileableFamily(name)
				                                    //name != 'PROSPECTIVE [000]'  // special case, hard coded (KM 3-9-20 now replaced with IsFileableFamily)
				                  end


				                  // walk down hierarchy for descendants

				                  Integer descendant
				                  for descendant = at + 1 to 4

				                        // get list of names of all browsable items at this level from subtree of hierarchy

				                        sqlStr = Str.Format ("select DTree%1.Name, DTree%1.DataID, DTree%1.OwnerID, DTree%1.PermID from DTree DTree%1", descendant)

				                        for i = descendant - 1 downto at
				                              sqlStr += Str.Format (" join DTree DTree%1 on DTree%1.DataID = DTree%2.ParentID and DTree%2.SubType = %3", \
				                                          i, i+1, $TypeVirtualFolder \
				                                    )
				                        end

				                        timer.Start ('browsable query (' + levels[descendant] + ')')
				                        RecArray records = CAPI.Exec ( \
				                                    prgCtx.fDBConnect.fConnection, \
				                                    sqlStr + Str.Format (" where DTree%1.DataID = :A1", at), \
				                                    ancestry[1].(Str.Format ("id%1", at)) \
				                              )
				                        timer.Stop()

				                        timer.Start ('browsable filter (' + levels[descendant] + ')')
				                        RecArray filteredRecords = $BESSEMER.Utils.FilterByPermissionToSee (prgCtx, records)
				                        timer.Stop()

				                        if IsError(filteredRecords) || IsUndefined(filteredRecords)  // exceeded time or size limit

				                              .(levels[descendant]) = Undefined

				                        else

				                              List browsable = RecArray.ColumnToList (filteredRecords, "Name")

				                              // populate level feature with accessibility of each item

				                              timer.Start ('accessibility (' + levels[descendant] + ')')

				                              List accessible = AccessibleByName ( \
				                                          RootID (levels[descendant]), \
				                                          browsable \
				                                    )

				                              String name
				                              for name in browsable
				                                    .(levels[descendant]).(name) = name in accessible > 0
				                              end

				                              timer.Stop()
				                        end
				                  end

				                  timer.Start ('contacts')
				                  .contacts = Contacts (request.fras, request.item)
				                  timer.Stop()

				                  .timings = timer.totals      

				                  return undefined
				            end



				            function Assoc Contacts (String fras, String item)

				                  Object prgCtx = .PrgSession()


								//Integer threshold = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'threshold', 0)

				                // get the associated contacts for the given criteria

				                  String sqlCondition

				                  switch fras

				                        case "family"
				                              sqlCondition = " where Family = :A1 and Relationship IS NULL and Account IS NULL and Subaccount IS NULL "
				                              end

				                        case "relationship"
				                              sqlCondition = " where Relationship = :A1 and Account IS NULL and Subaccount IS NULL "
				                              end

				                        case "account"
				                              sqlCondition = " where Account = :A1 and Subaccount IS NULL "
				                              end

				                        case "subaccount"
				                              sqlCondition = " where Subaccount = :A1 "
				                              end

				                        default // never happen
				                              sqlCondition = " where 0 == 1 "
				                              end
				                  end

				                  RecArray contactNames = CAPI.Exec ( \
				                              prgCtx.fDBConnect.fConnection, \
				                              "select Contact from BESSEMER_FRAS_Contacts " + sqlCondition, \
				                              item \
				                        )

				                  if not prgCtx.fDBConnect.CheckError (contactNames).OK
				                        return Undefined
				                  end


				                  Assoc contacts

				                  String contact
				                  for contact in AccessibleByName (RootID ("contacts"), RecArray.ColumnToList (contactNames, "Contact"))
				                        contacts.(contact) = true
				                  end

				                  return contacts
				            end



				            function Integer RootID (String level)

				                  Object prgCtx = .PrgSession()

				                  Assoc roots

				                  roots.families          = "myFamilies"
				                  roots.relationships = "myRelationships"
				                  roots.accounts          = "myAccounts"
				                  roots.subaccounts       = "mySubaccounts" 
				                  roots.contacts          = "myContacts"
				                  roots.hierarchy         = "hierarchyRoot"

				                  if not roots.(level)
				                        .error = "invalid level"
				                        return Undefined
				                  end

				                  Integer ID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', roots.(level))
				                  if IsUndefined(ID)
				                        .error = level + " not configured"
				                        return Undefined
				                  end

				                  return ID
				            end



				            function List AccessibleByName (Integer rootID, List names)

				            	  List retList

				                  if IsUndefined(names)
				                        return {}
				                  end

				                  String nameList = ""
				                  String name
				                  for name in names
				                        nameList += "'" + Str.ReplaceAll (name, "'", "''") + "',"
				                  end
				                  nameList = nameList[:-2]  // trailing comma


				                  Object prgCtx = .PrgSession()

				                  String stmt = Str.Format("select Name, DataID from DTree where ParentID = :A1 and Name in (%1)", nameList)

				                  RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, rootID)

				                  if IsNotError(recs)

				                  	Assoc status = $LLIAPI.NodeUtil.AppendPermissionsToRec(prgCtx.DSession(), recs)
									if status.ok
										retList = RecArray.ColumnToList (status.recs, "Name") 	
									end

				                  end

				                  return retList
				            end







	endscript


	override function void SetPrototype()

		                .fPrototype =\
		                      {\
		                            { "fras", StringType, Undefined, false }, \
		                            { "item", StringType, Undefined, false }, \
		                            { "wantgui", BooleanType, Undefined, false, false } \
		                      }
		          end

end
