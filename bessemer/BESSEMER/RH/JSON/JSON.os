package BESSEMER::RH::JSON

public Object JSON inherits BESSEMER::RH::LLRequestHandlers

	public		String	error
	override	String	fHTMLMimeType = 'application/json'



	public function Boolean IsFileableFamily(String familyName)

				Object prgCtx = .PrgSession()

				return $Bessemer.DocPermissions.IsFileableFamily(prgCtx, familyName)

			end

end
