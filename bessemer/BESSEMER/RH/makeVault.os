package BESSEMER::RH

public Object makeVault inherits BESSEMER::RH::LLRequestHandlers

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'ID', 2, Undefined, FALSE }, { 'nextURL', -1, Undefined, TRUE, 690 } }


	Script CreatePhysicalObject






												// this is a stripped down version of the ScannedItemsProcessor

												function CreatePhysicalObject (DAPINode document)

													Object prgCtx = .PrgSession()


													// where to make Physical Object

													Integer physicalItemsFolderID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'physicalItemsFolder', 0)
													DAPINode physicalItemsFolder = DAPI.GetNodeById (prgCtx.DAPISess(), 0, physicalItemsFolderID)
													if IsError(physicalItemsFolder); return $BESSEMER.Utils.Problem ("physical items folder not defined"); end


													// if something already exists with the name, assume it's been created already, and just OK it

													if DAPI.GetNode (document.pName, physicalItemsFolder)
														goto OK
													end


													// type of Physical Object to make

													Integer physicalItemTypeID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'physicalItemType', 0)
													DAPINode physicalItemType = DAPI.GetNodeById (prgCtx.DAPISess(), 0, physicalItemTypeID)
													if IsError(physicalItemType); return $BESSEMER.Utils.Problem ("physical item type not defined"); end

													if Type (physicalItemType.pExtendedData) != Assoc.AssocType; return $BESSEMER.Utils.Problem ("invalid physical item type"); end

													Integer physObjSubType

													if IsFeature (physicalItemType.pExtendedData,'Box') and physicalItemType.pExtendedData.Box
														physObjSubType = 424
													elseif IsFeature (physicalItemType.pExtendedData ,'Container') and physicalItemType.pExtendedData.Container
														physObjSubType =  412
													else
														physObjSubType = 411
													end


													// PO details

													String homeLocation = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'POLocation', '')

													String locatorType = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'LocatorType', '')


													//--- create Physical Object

													// uniqueID is the xyz12345 from "Addams [xyz12345]"

													String poUniqueID

													Integer left = Str.RChr (document.pName, "[")
													Integer right = Str.RChr (document.pName, "]")

													if left and right and right - left > 1
														poUniqueID = document.pName[left + 1 : right - 1]
													end


													Assoc nodeInfo

													nodeInfo.request = Assoc.CreateAssoc()
													nodeInfo.request.poLocation = homeLocation
													nodeInfo.request.selectedMedia = physicalItemTypeID
													nodeInfo.request.LocatorType = locatorType
													nodeInfo.request.pouniqueid = poUniqueID


													// get subaccount

													Assoc result = GetSubaccount (document)
													if not result.OK; return result; end

													String subaccount = result.subaccount
													if subaccount

														String property = CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "subaccountProperty")
														if property

															nodeInfo.request.("mtField_" + property) = subaccount
														end
													end


													// alloc and create

													Object llnode = $LLIAPI.LLNodeSubsystem.GetItem (physObjSubType)

													result = llnode.NodeAlloc (physicalItemsFolder, document.pName)
													if not result.OK; return $BESSEMER.Utils.Problem ("error allocating physical item", result.errMsg); end

													result = llnode.NodeCreate (result.node, physicalItemsFolder, nodeInfo)
													if not result.OK; goto OK; end  // "unique ID already in use"?
													if not result.OK; return $BESSEMER.Utils.Problem ("error creating physical item", result.errMsg); end

													DAPINode PONode = result.node


													// create cross reference

													Assoc xRefInfo

													xRefInfo.Node = document
													xRefInfo.xNode = PONode

													result = $RECMAN.RIMSUtils.CreateXReference (prgCtx, xRefInfo)
													if not result.OK; return result; end


													// set permissions same as document (performed with elevated privs)

													result = $LLIAPI.PrgSession.CreateNewNamed ( \
															$Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' ), \
															{ 'Admin', Undefined } )
													if not result.OK; return $BESSEMER.Utils.Problem ("cannot obtain elevated privileges to update rights", result.errMsg); end

													PONode = DAPI.GetNodeByID (result.pSession.DAPISess(), 0, PONode.pID)
													if not result.OK; return $BESSEMER.Utils.Problem ("cannot reaccess node to update rights", PONode); end	

													PONode.pUserID = 1000
													Integer updated = DAPI.UpdateNode (PONode)
													if IsError (updated); return $BESSEMER.Utils.Problem ("error assigning PO ownership", updated); end

													Assoc status = $BESSEMER.Utils.RemoveACLs(PONode)
													if !status.ok
														return status		
													end	

													Record nodeRight
													for nodeRight in DAPI.GetNodeRights (document)
														if IsError (DAPI.AddNodeRight (PONode, nodeRight.RightID, nodeRight.Permissions))
															return $BESSEMER.Utils.Problem (Str.Format ("error adding right to node %1", PONode.pID))
														end
													end

												OK:
													Assoc ok
													ok.ok = true
													return ok

												end



												function Assoc GetSubaccount (DAPINode node)

													Object prgCtx = .PrgSession()


													// cats atts values

													Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, node.pID, node.pVersionNum)
													if not attrData.Load()
														return $BESSEMER.Utils.Problem (Str.Format ("Error loading attributes for node %1", node.pID))
													end


													// we have to get the name of the fras attribute set, then the name of the 4th attribute in that set

													String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
													if IsUndefined(fras); return $BESSEMER.Utils.Problem ("fras not configured"); end

													Assoc result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx)
													if not result.OK; return result; end

													Assoc attributes = result.attributesByName

													if not attributes.(fras) or length (attributes.(fras).children) != 4; return $BESSEMER.Utils.Problem ("unexpected fras configuration"); end

													String subaccount = attributes.(fras).children[4]

													List subaccountValues = attrData.GetFromSet (fras, { subaccount } )

													Assoc ok
													ok.ok = true
													ok.subaccount = subaccountValues and length (subaccountValues) ? subaccountValues[1].(subaccount) : Undefined

													return ok
												end












	endscript

	override Script Execute







												function Dynamic Execute( \
													Dynamic		ctxIn, \
													Dynamic		ctxOut, \
													Record		request )

													//Object prgCtx = .PrgSession()

													.StartTransaction()

													VaultHeaderSheetOperations (request)

													.EndTransaction()

													return Undefined
												end



												function VaultHeaderSheetOperations (Record request)

													Object		prgCtx	= .PrgSession()

													DAPINode node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, request.ID)
													if IsError(node)
														.fError = Str.Format ("error accessing item: %1", node)
														return undefined
													end		

													Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, node.pID, node.pVersionNum)
													if not attrData.Load()
														.fError = "error accessing attributes"
														return undefined
													end


													// not for System Entered documents

													if attrData.Get ("System Entered") and attrData.Get ("System Entered")[1]
														.fError = "You can't create a Vault Header Sheet for a System-Entered document"
														return undefined
													end		


													// a sub-account is required for Vault header sheets else we send the user to refile it so they can add one

													if not HasSubAccount (attrData)

														.fLocation = Str.Format ("%1?func=bessemer.alert&message=%2&nextURL=%3", \
																.URL(), \
																Web.Escape ("A Vault Header Sheet cannot be created unless the document is associated with a Sub-Account. Click OK to select a Sub-Account"), \
																Web.Escape (.URL() + Str.Format ("?func=bessemer.filer&loadFrom=%1&operation=update&vaultness&nexturl=%2", request.ID, \
																		Web.Escape (.URL() + Str.Format ("?func=bessemer.makeVault&ID=%1", request.ID)) \
																	)) \
															)

														return undefined
													end


													// similarly, Page Count

													if not attrData.Get ("Page Count") or not attrData.Get ("Page Count")[1]

														.fLocation = Str.Format ("%1?func=bessemer.alert&message=%2&nextURL=%3", \
																.URL(), \
																Web.Escape ("A Vault Header Sheet cannot be created unless the document has a Page Count value. Click OK to set Page Count"), \
																Web.Escape (.URL() + Str.Format ("?func=bessemer.filer&loadFrom=%1&operation=update&vaultness&nexturl=%2", request.ID, \
																		Web.Escape (.URL() + Str.Format ("?func=bessemer.makeVault&ID=%1", request.ID)) \
																	)) \
															)

														return undefined
													end


													if not attrData.Get("Vault") or not attrData.Get("Vault")[1]

														if not attrData.SetValue ("Vault", { true })
															.fError = 'unable to set Vault'
															return undefined
														end


														// update cats/atts

														Assoc attributesUpdated = $LLIAPI.LLNodeSubsystem.GetItem (node.pSubtype).NodeCategoriesUpdate (node, attrData.attrData)
														if not attributesUpdated.ok
															.fError = "Error updating attributes: " + attributesUpdated.errMsg
															return undefined
														end
													end


													Assoc poCreated = .CreatePhysicalObject (node)
													if not poCreated.OK
														.fError = "Error creating physical object: " + poCreated.errMsg
														return undefined
													end


													// show meta

													.fLocation = Str.Format ("%1?func=bessemer.meta&ID=%2&type=vault&nextUrl=%3", \
															.URL(), \
															node.pID, \
															Web.Escape (request.nextUrl) \
														)



													return Undefined
												end



												function Boolean HasSubAccount (Frame attrData)

													Assoc result = $BESSEMER.AttributesConfig.GetAttributes (.fPrgCtx)
													if not result.OK; return Undefined; end
													Assoc attributes = result.attributesByName	

													String fras = CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras")
													if IsUndefined(fras) or not attributes.(fras) or length (attributes.(fras).children) < 4; return Undefined; end

													String subaccount = attributes.(fras).children[4]

													Assoc data
													for data in attrData.GetFromSet (fras, { subaccount })

														if data.(subaccount)
															return true
														end
													end

													return false

												end











	endscript


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "ID", IntegerType, Undefined, FALSE }, \
									{ "nextURL", StringType, Undefined, true, $WebDsp.kNextURL } \
								}
						end

end
