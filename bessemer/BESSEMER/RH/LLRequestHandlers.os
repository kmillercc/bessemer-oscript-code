package BESSEMER::RH

public Object LLRequestHandlers inherits WEBLL::LLRequestHandler

	override	String	fFuncPrefix = 'bessemer'
	override	String	fHTMLLetter



	public function Dynamic ConfigurationSetting (String configurationName, Dynamic defaultValue = Undefined)

				return CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', configurationName, defaultValue)

			end


	public function IsBad(Dynamic val)

				return IsError(val) || IsUndefined(val)

			end

end
