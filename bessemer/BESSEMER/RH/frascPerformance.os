package BESSEMER::RH

public Object frascPerformance inherits BESSEMER::RH::LLRequestHandlers

	override	Boolean	fEnabled = TRUE
	public		Dynamic	results



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							RecArray results = RecArray.Create ({"root","method","count","time"})

							Integer threshold = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'threshold', 0)

							String root
							for root in { "myFamilies", "myRelationships", "myAccounts", "mySubaccounts", "myContacts" }

								// find myfrasc

								Integer ID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', root)
								if IsUndefined(ID)
									.fError = root + " not configured"
									return undefined
								end	

								DAPINode rootNode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, ID)
								if IsError(rootNode)
									.fError = "missing " + root
									return undefined
								end


								// time dapi.listsubnodes

								Integer timer = Date.Tick()
								List nodes = DAPI.ListSubNodes (rootNode)
								RecArray.AddRecord ( results, { root, "DAPI.ListSubNodes", length (nodes), Date.Tick() - timer })


								// time nodeutil.listnodes

								Boolean permFirst
								for permFirst in { true, false }

									timer = Date.Tick()		
									RecArray contents = $LLIAPI.NodeUtil.ListNodes ( \
											.PrgSession(), \
											"ParentID = :A1", \
											{ rootNode.pID }, \
											"DTree", \
											{ "DataID", "PermID", "Name" }, \
											$PSee, \
											permFirst) \
										.contents

									RecArray.AddRecord ( results, { root, "$LLIAPI.NodeUtil.ListNodes (" + Str.String (permFirst) + ")", length (contents), Date.Tick() - timer })
								end


								// time llnode.nodelistcontentsbypage

								timer = Date.Tick()		
								RecArray contents = $LLIAPI.LLNodeSubsystem.GetItem (rootNode.pSubtype).NodeListContentsByPage ( \
										rootNode, \
										0, \
										threshold) \
									.contents

								RecArray.AddRecord ( results, { root, "NodeListContentsByPage", length (contents), Date.Tick() - timer })


								// time dapi.listcontents

								timer = Date.Tick()
								contents = DAPI.ListContents (prgCtx.DAPISess(), 0, ID, "DTree")
								RecArray.AddRecord ( results, { root, "DAPI.ListContents", length (contents), Date.Tick() - timer })

							end

							if request.sql
								Integer timer = Date.Tick()

								Object db = .PrgSession().fDbConnect

								db.ResetTransactionsIfNecessary()
								db.StartTrans()
								RecArray r = CAPI.Exec (.PrgSession().fDbConnect.fConnection, "select " + request.sql)
								db.CheckError (r)
								db.EndTrans (FALSE)

								RecArray.AddRecord ( results, { "", "sql", r ? length (r) : r, Date.Tick() - timer })
							end

							.results = results

							return undefined

						end

end
