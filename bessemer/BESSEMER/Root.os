package BESSEMER

/**
 * 
 *  This is a good place to put documentation about your OSpace.
 *  
 *  
 */
public Object Root

	public		Object	Globals = BESSEMER::BessemerGlobals




	public function Assoc Problem (String message, Dynamic result = undefined, String errDetail = Undefined)

				Assoc rtn

				rtn.OK = false
				rtn.errMsg = message
				if IsDefined (errDetail)
					rtn.errMsg += Str.Format(" (%1)", errDetail)
				end

				if IsDefined(result)
					if Type(result) == ErrorType
						rtn.apiError = result
					else
						rtn.errMsg += Str.Format(" (%1)", Str.String(result))
					end

				end

				return rtn

			end

	/**
			 *  Content Server Startup Code
			 */
	public function Void Startup()

				//
				// Initialize globals object
				//

				Object	globals = $Bessemer = .Globals.Initialize()

				globals.DocPermissions = globals.DocPermissions.New()
				globals.AccountPermissions = globals.AccountPermissions.New()
				globals.ContactPermissions = globals.ContactPermissions.New()
				globals.DocPermissions = globals.DocPermissions.New()
				globals.RelationshipPermissions = globals.RelationshipPermissions.New()
				globals.SubAccountPermissions = globals.SubAccountPermissions.New()

				globals.PermissionsConfig = globals.PermissionsConfig.New()
				globals.AttributesConfig = globals.AttributesConfig.New() 

				//
				// Initialize objects with __Init methods
				//

				$Kernel.OSpaceUtils.InitObjects( globals.f__InitObjs )

				Echo( "Finished Bessemer Startup" )
				Echo( "*********************************************************************************************************************************" )

			end


	public function Assoc Success(Dynamic result = undefined)
				Assoc rtn
				rtn.ok = true
				rtn.result = result
				rtn.errMsg = undefined
				rtn.apiError = undefined

				return rtn
			end
			
	public function void Scratch()
		
		Integer permsMask = $PSee | $PSeeContents | $PModify | $PEditAtts
		
		echo(permsMask)
	end		

end
