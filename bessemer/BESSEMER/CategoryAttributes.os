package BESSEMER

public Object CategoryAttributes inherits BESSEMER::Root

	public		Dynamic	attrData


	Script #'0 Test'#



						Object prgCtx = $prgsessions[1]

						Frame categoryAttribute = .New (prgCtx, 32686, 0)

						if not categoryAttribute.Load()
							echo ('Load failed')
						end

						assoc f
						f.Family='myfam'
						f.Account='myacc'

						assoc c
						c.account='xxxxxxxxxxxxxxxxxxxxx'

						echo( categoryAttribute.ChangeSet(f,c))

						echo (1)




	endscript


	public function Boolean ChangeSet (Assoc find, Assoc change )

				List category
				for category in Assoc.Keys (.attrData.fDefinitions)
					if .FindAndChange(.attrData.fDefinitions.(category), .attrData.fData.(category), find, change)		
						return true
					end
				end

				return false

			end


	public function Boolean FindAndChange (Assoc definition, Assoc data, Assoc find, Assoc change)

				if not definition.children; return Undefined; end  // safety

				// make references to the data values for all the "find"&"change" attributes

				Assoc dataValuesByName

				Assoc attribute
				for attribute in definition.Children

					if attribute.children  

						// recurse into set

						if FindAndChange (attribute, data.Values[1].(attribute.ID), find, change)
							return true
						end

					elseif attribute.DisplayName in Assoc.Keys (find) or attribute.DisplayName in Assoc.Keys (change) 

						// keep the data for later checking

						List dataValues = {}

						Assoc dataValue
						for dataValue in data.Values
							dataValues = { @dataValues, dataValue.(attribute.ID) }
						end

						dataValuesByName.(attribute.DisplayName) = dataValues

					end
				end


				// check that all attributes exist

				String name

				for name in Assoc.Keys (find)
					if not dataValuesByName.(name); return false; end
				end

				for name in Assoc.Keys (change)
					if not dataValuesByName.(name); return false; end
				end


				// check each set for all "find" values, and change them if all there

				Boolean changed = false

				// each set

				Integer i
				for i = 1 to length (dataValuesByName[1])  // all same length

					Boolean shouldChange = true

					// all finds

					String findName
					for findName in Assoc.Keys (find)

						if dataValuesByName.(findName)[i].Values[1] != find.(findName)
							shouldChange = false
							break
						end
					end

					if shouldChange

						// all changes

						String changeName
						for changeName in Assoc.Keys (change)
							dataValuesByName.(changeName)[i].Values[1] = change.(changeName)  // pass by ref!
						end

						changed = true
					end
				end

				return changed

			end


	public function Dynamic Get (String attributeName)

				List category
				for category in Assoc.Keys (.attrData.fDefinitions)

					Assoc attribute
					for attribute in .attrData.fDefinitions.(category).Children
						continueif attribute.DisplayName != attributeName

						return .attrData.fData.(category).Values[1].(attribute.ID).Values
					end
				end

				return Undefined
			end

	/*

			returns lists of tuples given "setName" and List of attribute names

			Note: this assumes the Set is multivalued NOT the attributes!

			*/
	public function List GetFromSet ( String 	setName, List	attributeNames )	

				// find the IDs of the attributes (and the category they're in)
				Integer setID = 0

				List category
				for category in Assoc.Keys (.attrData.fDefinitions)

					setID = ._FindName(.attrData.fDefinitions.(category), setName)

					breakif IsDefined (setID)
				end

				if not setID; echo ('no set named: ', setName); return Undefined; end


				Assoc attributeIDs

				String attributeName
				for attributeName in attributeNames

					attributeIDs.(attributeName) = ._FindName(.attrData.fDefinitions.(category), attributeName)
					if not attributeIDs.(attributeName); echo ('no attribute named: ', attributeName); return Undefined; end
				end


				// find the value entry for the set

				Assoc setEntry = ._FindValueEntry(.attrData.fData.(category).Values, setID)
				if IsUndefined(setEntry)
					 echo ('no entry for attribute named: ', setName)
					 return Undefined
				end  // never happen


				// load up values

				List values

				Integer i
				for i = 1 to length (setEntry.Values)

					Assoc valuesAssoc = Assoc.CreateAssoc()

					for attributeName in attributeNames
						Integer attributeID = attributeIDs.(attributeName)

						valuesAssoc.(attributeName) = setEntry.Values[i].(attributeID).Values[1]
					end

					values = { @values, Assoc.Copy (valuesAssoc) }
				end

				return values

			end


	public function Boolean Load ()

				return .attrData.dbGet().OK

			end


	public function New (Object prgCtx, Integer item, Integer version )

				Frame me = Frame.New ( { { "fParent", this } } )

				me.attrData = $LLIAPI.AttrData.New (prgCtx, item, version)

				return me
			end


	public function Boolean Save ()

				return .attrData.dbPut().OK

			end


	public function Boolean SetValue (String attributeName, List values)
				// find the ID of the attribute

				Integer attribute

				List assignedCategory
				for assignedCategory in Assoc.Keys (.attrData.fDefinitions)

					attribute = ._FindName(.attrData.fDefinitions.(assignedCategory), attributeName)
					breakif IsDefined (attribute)
				end

				if not attribute; echo ('no attribute named: ', attributeName); return false; end  // no such attribute


				// find the value entry

				Assoc entry = ._FindValueEntry(.attrData.fData.(assignedCategory).Values, attribute)
				if IsUndefined(entry); echo ('no entry for attribute named: ', attributeName); return false; end  // never happen


				// cleanup values, replacing empty string by undefined, and update
				Integer i
				for i = 1 to length (values)
					if values[i] == ''
						values[i] = Undefined
					end
				end


				// add audit info with before and after if changes were significant

				if IsDefined (entry.Values[1]) and IsDefined (values[1]) and entry.Values[1] != values[1]

					RecArray.AddRecord (.attrData.fValueChanges, { \
							Str.Format ("%1[%2].%3[%4]", \
									this.attrData.fDefinitions.(assignedCategory).DisplayName, \
									this.attrData.fDefinitions.(assignedCategory).ID, \
									attributeName, \
									attribute \
								), \
							entry.Values[1], \
							values[1], \
							assignedCategory[1], \
							attribute \
						})
				end


				// and make change

				entry.Values = values

				return true

			end

	/*

			populates attrData given "setName" and data Assoc with key=attributeName, value=List of values

			(lists must all be that same length)


			Note: this assumes the Set is multivalued NOT the attributes!

			*/
	public function Boolean SetinSet (String setName, Assoc values)

				// find the IDs of the attributes (and the category they're in)

				Integer setID = 0

				List category
				for category in Assoc.Keys (.attrData.fDefinitions)

					setID = ._FindName(.attrData.fDefinitions.(category), setName)

					breakif IsDefined (setID)
				end

				if not setID; echo ('no set named: ', setName); return false; end


				Assoc attributeIDs

				String attributeName
				for attributeName in Assoc.Keys (values)

					attributeIDs.(attributeName) = ._FindName(.attrData.fDefinitions.(category), attributeName)
					if not attributeIDs.(attributeName); echo ('no attribute named: ', attributeName); return false; end
				end


				// find the length of the values

				Integer valuesLength = 0

				List valuesList
				for valuesList in values
					if valuesLength == 0
						valuesLength = length (valuesList)

					elseif length (valuesList) != valuesLength
						echo ("inconsistent value length")
						return false
					end
				end	


				// find the value entry for the set

				Assoc setEntry = ._FindValueEntry(.attrData.fData.(category).Values, setID)
				if IsUndefined(setEntry); echo ('no entry for attribute named: ', setName); return false; end  // never happen


				// trim the set to just one, then pad out to proper length

				setEntry.Values = { setEntry.Values[1] }

				Integer i
				for i = 2 to valuesLength
					setEntry.Values = { @setEntry.Values, $Kernel.DataTools.DeepCopy (setEntry.Values[1]) }
				end


				// populate

				for attributeName in Assoc.Keys (values)
					Integer attributeID = attributeIDs.(attributeName)

					for i = 1 to valuesLength			
						List newValues = { values.(attributeName)[i] }

						// clean any empty strings to be undefined instead
						Integer j
						for j = 1 to length (newValues)
							if newValues[j] == ''
								newValues[j] = Undefined
							end

							// audit the change

							if newValues[j]

								RecArray.AddRecord (.attrData.fValueChanges, { \
										Str.Format ("%1[%2].%3[%4].%5[%6]", \
												this.attrData.fDefinitions.(category).DisplayName, \
												this.attrData.fDefinitions.(category).ID, \
												setName, \
												setID, \
												attributeName, \
												attributeIDs.(attributeName) \
											), \
										Undefined, \
										newValues[j], \
										category[1], \
										attributeIDs.(attributeName) \
									})

							end
						end

						setEntry.Values[i].(attributeID).Values = newValues
					end
				end

				return true

			end


	private function Integer _FindName (Assoc definition, String name)

				if definition.children

					Assoc child
					for child in definition.children
						Integer found = _FindName (child, name)
						if found; return found; end
					end		
				end

				if Str.CmpI (definition.displayName, name) == 0
					return definition.ID
				end

				return Undefined
			end


	private function Assoc _FindValueEntry (List entries, Integer ID)

				Assoc entry
				for entry in entries

					// attribute here?	

					if Assoc.IsKey (entry, ID)
						return entry.(ID)
					end


					// maybe there's a set; recurse in them

					Integer entryID
					for entryID in Assoc.Keys (entry)

						if entry.(entryID).Values and Type (entry.(entryID).Values[1]) == Assoc.AssocType
							Assoc found = _FindValueEntry(entry.(entryID).Values, ID)
							if found; return found; end
						end
					end

				end

				return Undefined
			end

end
