package BESSEMER::ADMINRH::FILER

public Object configLocations2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'bucket_ID', 2, '', FALSE }, { 'physicalItemType_ID', 2, '', FALSE }, { 'POLocation', -1, '', FALSE }, { 'LocatorType', -1, '', FALSE }, { 'uploadFolderPath', -1, '', FALSE }, { 'subaccountProperty', -1, '', FALSE }, { 'physicalItemsFolder_ID', 2, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"bucket_ID", \
									"physicalItemType_ID", \
									"POLocation", \
									"LocatorType", \
									"uploadFolderPath", \
									"subaccountProperty", \
									"physicalItemsFolder_ID" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config[-3:] == '_ID' ? config[:-4] : config, \
										request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							.fLocation = .URL() + "?func=admin.index"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'bucket_ID', IntegerType, '', FALSE }, \
									{ 'physicalItemType_ID', IntegerType, '', FALSE }, \
									{ 'POLocation', StringType, '', FALSE }, \
									{ 'LocatorType', StringType, '', FALSE }, \
									{ 'uploadFolderPath', StringType, '', FALSE }, \
									{ 'subaccountProperty', StringType, '', FALSE }, \
									{ 'physicalItemsFolder_ID', IntegerType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
