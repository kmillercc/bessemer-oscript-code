package BESSEMER::ADMINRH::FILER

public Object reorderAttribute inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'off', 2, '', FALSE }, { 'ID', 2, '', FALSE } }


	override Script Execute







												function Dynamic Execute( \
													Dynamic		ctxIn, \
													Dynamic		ctxOut, \
													Record		request )

													Object		prgCtx	= .PrgSession()

													.StartTransaction()

													RecArray config = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
															"select Attribute, Ordering from BESSEMER_MetadataAttributes order by Ordering" \
														)
													if not .CheckError (config); goto done; end


													// find the item

													Record entry
													for entry in config
														breakif entry.Attribute == request.ID
													end

													if entry.Attribute != request.ID
														.fError = "Unknown attribute"
														goto done
													end

													Record item = entry


													// find the items next to this one that we might swap with

													Record previous
													Record next

													for entry in config
														if entry == item
															next = entry  // flag
															continue
														end

														if next
															next = entry
															break
														end

														previous = entry
													end


													// switcheroo

													if request.off == -1
														if previous
															Swap (prgCtx, previous, item)
														end

													elseif request.off == 1
														if next
															Swap (prgCtx, item, next)
														end

													else
														.fError = "Invalid offset argument"
														goto done
													end	


													.fLocation = .URL() + "?func=bessemer.configMetadata"


												done:
													.EndTransaction()

													return Undefined
												end



												function Swap (Object prgCtx, Record r1, Record r2)

													Integer r1NewOrdering = r2.Ordering
													Integer r2NewOrdering = r1.Ordering

													if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
															"update BESSEMER_MetadataAttributes" \
															+ " set Ordering = :A1" \
															+ " where Attribute = :A2", \
															r1NewOrdering, \
															r1.Attribute \
														))
														return undefined
													end

													if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
															"update BESSEMER_MetadataAttributes" \
															+ " set Ordering = :A1" \
															+ " where Attribute = :A2", \
															r2NewOrdering, \
															r2.Attribute \
														))
														return undefined
													end

												end











	endscript


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'off', IntegerType, '', FALSE }, \
									{ 'ID', IntegerType, '', FALSE } \
								}
						end

end
