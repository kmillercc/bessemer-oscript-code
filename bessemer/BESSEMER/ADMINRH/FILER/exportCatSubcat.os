package BESSEMER::ADMINRH::FILER

public Object exportCatSubcat inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String filePath = $Kernel.ModuleUtils.FindModulePath ($Kernel.ModuleSubsystem.GetRefItem (this).ModuleName()) + \
									"catSubcat.txt"

							File f = File.Open (filePath, File.WriteMode)
							if IsError(f) || IsUndefined(f)
								.fError = "Error opening catSubcat file: " + Str.String (f)
								return undefined
							end

							Record data
							for data in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_CategorySubcategory order by Category, Subcategory" \
								)
								File.Write (f, Str.Format ("%1%2%3%4%5", \
										data.Category, \
										Str.Tab(), \
										data.Subcategory ? data.Subcategory : '', \
										Str.Tab(), \
										data.SystemOnly ? '1' : '0' \
									))
							end

							File.Close (f)

							.fLocation = .Url() + "?func=bessemer.operationComplete&operation=" + \
									Web.Escape ("Categories / subcategories exported") + \
									"&nextFunc=admin.index"

							return undefined		
						end

end
