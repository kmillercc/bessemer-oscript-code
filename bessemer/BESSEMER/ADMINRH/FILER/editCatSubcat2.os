package BESSEMER::ADMINRH::FILER

public Object editCatSubcat2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'data', -18, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.StartTransaction()

							// capture all that exist

							Assoc existing
							Record r
							for r in CAPI.Exec (prgCtx.fDBConnect.fConnection, "select * from BESSEMER_CategorySubcategory")
								existing.({r.Category, r.Subcategory}) = r.systemOnly
							end


							// delta against what comes from the page

							String category
							for category in Assoc.Keys (request.data)

								String subcategory
								for subcategory in request.data.(category)

									// hack: name has setting in it
									String systemStr = " -- System only"

									Integer systemOnly = 0
									if subcategory[- length (systemStr):] == systemStr
										subcategory = subcategory[: - length (systemStr) - 1]
										systemOnly = 1
									end

									if not Assoc.IsKey (existing, { category, subcategory })

										if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												"insert into BESSEMER_CategorySubcategory (Category, Subcategory, SystemOnly) values (:A1, :A2, :A3)", \
												category, \
												subcategory, \
												systemOnly \
											))
											goto done
										end

									else  // already exists

										if existing.({ category, subcategory }) != systemOnly  // needs updating

											if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
													"update BESSEMER_CategorySubcategory set SystemOnly = :A1 where Category = :A2 and Subcategory = :A3", \
													systemOnly, \
													category, \
													subcategory \
												))
												goto done
											end

										end
									end

									Assoc.Delete (existing, { category, subcategory })

								end
							end

							// whatever is left has to be deleted

							List catSubcat
							for catSubcat in Assoc.Keys (existing)

								if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										"delete from BESSEMER_CategorySubcategory where Category = :A1 and Subcategory = :A2", \
										catSubcat[1], \
										catSubcat[2] \
									))
									goto done
								end

							end

							.fLocation = .URL() + "?func=admin.index"


						done:
							.EndTransaction()

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'data', Assoc.AssocType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
