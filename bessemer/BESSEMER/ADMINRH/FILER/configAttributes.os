package BESSEMER::ADMINRH::FILER

public Object configAttributes inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		String	barcode
	public		String	categoryPath
	public		Integer	category_ID
	public		String	contact
	override	Boolean	fEnabled = TRUE
	public		String	filedBy
	public		String	fras
	public		String	level
	public		String	protocol



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"category_ID", \
									"protocol", \
									"fras", \
									"level", \
									"contact", \
									"barcode", \
									"filedBy" \
								}

								if config[-3:] == '_ID'  // a setting with a node path

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config[:-4])
									continueif not .(config)

									DAPINode node = DAPI.GetNodeById (prgCtx.DAPISess(), 0, .(config))
									continueif IsError(node)

									.(config[:-4] + "Path") = $LLIAPI.LLNodeSubsystem.GetItem (node.pSubtype).NodePath (node).Path

								else

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config)

								end

							end

							return Undefined
						end

end
