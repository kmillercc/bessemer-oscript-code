package BESSEMER::ADMINRH::FILER

public Object configPresentation2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'familyWidth', 2, '', FALSE }, { 'relationshipWidth', 2, '', FALSE }, { 'accountWidth', 2, '', FALSE }, { 'subaccountWidth', 2, '', FALSE }, { 'contactWidth', 2, '', FALSE }, { 'familyAutoload', 5, '', FALSE }, { 'relationshipAutoload', 5, '', FALSE }, { 'accountAutoload', 5, '', FALSE }, { 'subaccountAutoload', 5, '', FALSE }, { 'contactAutoload', 5, '', FALSE }, { 'backgroundColor', -1, '', FALSE }, { 'itemBackgroundColor', -1, '', FALSE }, { 'categoryWidth', 2, '', FALSE }, { 'subcategoryWidth', 2, '', FALSE }, { 'frasEntries', 2, '', FALSE }, { 'contactsEntries', 2, '', FALSE }, { 'threshold', -1, '', FALSE }, { 'responseLimit', -1, '', FALSE }, { 'exceededMessage', -1, '', FALSE }, { 'emptyMessage', -1, '', FALSE }, { 'insufficientMessage', -1, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							if request.threshold == ''
								request.threshold = '0'
							end

							request.threshold = Str.StringToInteger (request.threshold)
							if IsUndefined (request.threshold)
								.fError = "Non-numeric input"
								return undefined
							end


							if request.responseLimit == ''
								request.responseLimit = '0'
							end

							request.responseLimit = Str.StringToInteger (request.responseLimit)
							if IsUndefined (request.responseLimit)
								.fError = "Non-numeric input"
								return undefined
							end


							String config

							for config in { \
									"familyAutoload", \
									"relationshipAutoload", \
									"accountAutoload", \
									"subaccountAutoload", \
									"contactAutoload" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config, \
										RecArray.IsColumn (request, config) and request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							for config in { \
									"familyWidth", \
									"relationshipWidth", \
									"accountWidth", \
									"subaccountWidth", \
									"contactWidth", \
									"categoryWidth", \
									"subcategoryWidth", \
									"frasEntries", \
									"contactsEntries", \
									"threshold", \
									"responseLimit", \
									"backgroundColor", \
									"itemBackgroundColor", \
									"exceededMessage", \
									"emptyMessage", \
									"insufficientMessage" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config, \
										request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							.fLocation = .URL() + "?func=admin.index"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'familyWidth', IntegerType, '', FALSE }, \
									{ 'relationshipWidth', IntegerType, '', FALSE }, \
									{ 'accountWidth', IntegerType, '', FALSE }, \
									{ 'subaccountWidth', IntegerType, '', FALSE }, \
									{ 'contactWidth', IntegerType, '', FALSE }, \
									{ 'familyAutoload', BooleanType, '', FALSE }, \
									{ 'relationshipAutoload', BooleanType, '', FALSE }, \
									{ 'accountAutoload', BooleanType, '', FALSE }, \
									{ 'subaccountAutoload', BooleanType, '', FALSE }, \
									{ 'contactAutoload', BooleanType, '', FALSE }, \
									{ 'backgroundColor', StringType, '', FALSE }, \
									{ 'itemBackgroundColor', StringType, '', FALSE }, \
									{ 'categoryWidth', IntegerType, '', FALSE }, \
									{ 'subcategoryWidth', IntegerType, '', FALSE }, \
									{ 'frasEntries', IntegerType, '', FALSE }, \
									{ 'contactsEntries', IntegerType, '', FALSE }, \
									{ 'threshold', StringType, '', FALSE }, \
									{ 'responseLimit', StringType, '', FALSE }, \
									{ 'exceededMessage', StringType, '', FALSE }, \
									{ 'emptyMessage', StringType, '', FALSE }, \
									{ 'insufficientMessage', StringType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
