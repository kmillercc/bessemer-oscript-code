package BESSEMER::ADMINRH::FILER

public Object configLocations inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		String	POLocation
	public		String	bucketPath
	public		Integer	bucket_ID
	override	Boolean	fEnabled = TRUE
	public		String	locatorType
	public		String	physicalItemTypePath
	public		Integer	physicalItemType_ID
	public		String	physicalItemsFolderPath
	public		Integer	physicalItemsFolder_ID
	public		String	subaccountProperty
	public		String	uploadFolderPath



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"bucket_ID", \
									"physicalItemType_ID", \
									"POLocation", \
									"LocatorType", \
									"uploadFolderPath", \
									"subaccountProperty", \
									"physicalItemsFolder_ID" \
								}

								if config[-3:] == '_ID'  // a setting with a node path

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config[:-4])
									continueif not .(config)

									DAPINode node = DAPI.GetNodeById (prgCtx.DAPISess(), 0, .(config))
									continueif IsError(node)

									.(config[:-4] + "Path") = $LLIAPI.LLNodeSubsystem.GetItem (node.pSubtype).NodePath (node).Path

								else

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config)

								end

							end

							return Undefined
						end

end
