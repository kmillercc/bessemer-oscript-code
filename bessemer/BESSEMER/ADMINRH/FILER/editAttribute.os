package BESSEMER::ADMINRH::FILER

public Object editAttribute inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Integer	attribute
	public		Dynamic	categoriesSubcategories
	public		Dynamic	config
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'ID', 2, '', FALSE } }
	public		Integer	headerSheet
	public		String	name



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							// cats / subcats

							.categoriesSubcategories = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_CategorySubcategory where SystemOnly = 0 order by Category, Subcategory" \
								)
							if not .CheckError (.categoriesSubcategories); return undefined; end


							// configuration

							.attribute = request.ID


							// name

							Assoc result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx)
							if not result.OK
								.fError = result.errMsg
								return undefined
							end

							Assoc attributes = result.attributes

							if not Assoc.IsKey (attributes, request.ID)
								.fError = "Unknown attribute"
								return undefined
							end

							.name = attributes.(request.ID).displayName


							// header sheet

							RecArray attributeInfo = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select HeaderSheet from BESSEMER_MetadataAttributes where Attribute = :A1", \
									request.ID \
								)
							if not .CheckError (attributeInfo); return undefined; end

							if length (attributeInfo) == 0
								.fError = "Attribute not configured"
								return undefined
							end

							.headerSheet = attributeInfo[1][1]


							// cat/subcat config

							.config = {}

							RecArray catSubcats = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_MetadataCatSubcat" \
									+ " where Attribute = :A1" \
									+ " order by Category, Subcategory", \
									request.ID \
								)
							if not .CheckError (catSubcats); return undefined; end

							Record catSubcat
							for catSubcat in catSubcats
								.config = { @.config, { catSubcat.Category, catSubcat.Subcategory, catSubcat.Requiredness } }
							end

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'ID', IntegerType, '', FALSE } \
								}
						end

end
