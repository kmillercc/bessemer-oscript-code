package BESSEMER::ADMINRH::FILER

public Object configAttributes2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'category_ID', 2, '', FALSE }, { 'protocol', -1, '', FALSE }, { 'fras', -1, '', FALSE }, { 'level', -1, '', FALSE }, { 'contact', -1, '', FALSE }, { 'barcode', -1, '', FALSE }, { 'filedBy', -1, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"category_ID", \
									"uploadFolderPath", \
									"protocol", \
									"fras", \
									"level", \
									"contact", \
									"barcode", \
									"filedBy" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config[-3:] == '_ID' ? config[:-4] : config, \
										request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							.fLocation = .URL() + "?func=admin.index"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'category_ID', IntegerType, '', FALSE }, \
									{ 'protocol', StringType, '', FALSE }, \
									{ 'fras', StringType, '', FALSE }, \
									{ 'level', StringType, '', FALSE }, \
									{ 'contact', StringType, '', FALSE }, \
									{ 'barcode', StringType, '', FALSE }, \
									{ 'filedBy', StringType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
