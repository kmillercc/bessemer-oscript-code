package BESSEMER::ADMINRH::FILER

public Object configProtocol2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'cs_list', 2, '', FALSE, Undefined, TRUE }, { 'family_list', 5, '', FALSE, Undefined, TRUE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.StartTransaction()

							;Integer i = 0

							Record categorySubcategory
							for categorySubcategory in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_CategorySubcategory where SystemOnly = 0 order by Category, Subcategory")
								i += 1

								Integer setting = \
										  (request.(Str.Format ('family%1', i)) ? 16 : 0) \
										+ (request.(Str.Format ('relationship%1', i)) ? 8 : 0) \
										+ (request.(Str.Format ('account%1', i)) ? 4 : 0) \
										+ (request.(Str.Format ('subaccount%1', i)) ? 2 : 0) \
										+ (request.(Str.Format ('contact%1', i)) ? 1 : 0)

								Integer updated

								if categorySubcategory.Subcategory
									updated = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										"update BESSEMER_CategorySubcategory set FRAS = :A1 where Category = :A2 and Subcategory = :A3", \
										setting, \
										categorySubcategory.Category, \
										categorySubcategory.Subcategory \
									)
								else
									updated = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										"update BESSEMER_CategorySubcategory set FRAS = :A1 where Category = :A2 and Subcategory IS NULL", \
										setting, \
										categorySubcategory.Category \
									)
								end

								if not .CheckError (updated); goto done; end

							end	

							.fLocation = .URL() + "?func=admin.index"


						done:
							.EndTransaction()

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'cs_list', IntegerType, '', FALSE, Undefined, TRUE }, \
									{ 'family_list', BooleanType, '', FALSE, Undefined, TRUE }, \
									$WebDsp.kCheckPost \
								}
						end

end
