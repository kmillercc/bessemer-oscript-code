package BESSEMER::ADMINRH::FILER

public Object importCatSubcat inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

				Object		prgCtx	= .PrgSession()

				String filePath = $Kernel.ModuleUtils.FindModulePath ($Kernel.ModuleSubsystem.GetRefItem (this).ModuleName()) + \
						"catSubcat.txt"

				File f = File.Open (filePath, File.ReadMode)
				if IsError(f) || IsUndefined(f)
					.fError = "Error opening catSubcat file: " + Str.String (f)
					return undefined
				end

				.StartTransaction()

				if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, "delete from BESSEMER_CategorySubcategory"))
					goto done
				end

				String line
				for (line = File.Read (f); line != File.E_Eof; line = File.Read (f))

					List parts = Str.Elements (line, Str.Tab)

					if length (parts) == 2  // backwards compatible
						parts = { @parts, '0' }

					elseif length (parts) != 3
						.fError = "invalid file"
						goto done
					end

					if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
							"insert into BESSEMER_CategorySubcategory (Category, Subcategory, SystemOnly) values (:A1, :A2, :A3)", \
							parts[1], \
							parts[2], \
							parts[3] == '1' ? 1 : 0 \
						))
						goto done
					end

				end

			done:
				.EndTransaction()

				File.Close (f)

				.fLocation = .Url() + "?func=bessemer.operationComplete&operation=" + \
						Web.Escape ("Categories / subcategories imported") + \
						"&nextFunc=admin.index"

				return undefined		
			end

end
