package BESSEMER::ADMINRH::FILER

public Object editAttribute2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'attribute', 2, '', FALSE }, { 'headerSheet', 2, '', FALSE } }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.StartTransaction()

							// reset

							if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"delete from BESSEMER_MetadataCatSubcat" \
									+ " where Attribute = :A1", \
									request.attribute \
								))
								goto done
							end


							// update the cats/subcats 

							;Integer i = 0

							Record categorySubcategory
							for categorySubcategory in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_CategorySubcategory where SystemOnly = 0 order by Category, Subcategory")
								i += 1

								String csi = Str.Format ('cs%1', i)
								continueif not RecArray.IsColumn (request, csi)
								continueif request.(csi) < "0" or request.(csi) > "1"

								if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
										"insert into BESSEMER_MetadataCatSubcat (Attribute, Category, Subcategory, Requiredness) values (:A1, :A2, :A3, :A4)", \
										request.attribute, \
										categorySubcategory.Category, \
										categorySubcategory.Subcategory, \
										Str.StringToInteger (request.(csi)) \
									))
									goto done
								end

							end


							// header sheet

							if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"update BESSEMER_MetadataAttributes set HeaderSheet = :A1 where Attribute = :A2", \
									request.headerSheet, \
									request.attribute \
								))
								goto done
							end


							.fLocation = .URL() + "?func=bessemer.configMetadata"


						done:
							.EndTransaction()

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'attribute', IntegerType, '', FALSE }, \
									{ 'headerSheet', IntegerType, '', FALSE } \
								}
						end

end
