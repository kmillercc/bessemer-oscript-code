package BESSEMER::ADMINRH::FILER

public Object configMetadata inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	data
	override	Boolean	fEnabled = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							Assoc result = $BESSEMER.AttributesConfig.GetAttributes (prgCtx)
							if not result.OK
								.fError = result.errMsg
								return undefined
							end

							Assoc attributes = result.attributes


							// get the current config and update it to have only the current attributes

							.StartTransaction()

							Assoc configAttributes

							Record config 
							for config in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select Attribute from BESSEMER_MetadataAttributes" \
								)

								// delete this entry if attribute no longer used

								if not (config.Attribute in Assoc.Keys (attributes))

									if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"delete from BESSEMER_MetadataCatSubcat where Attribute = :A1", \
											config.Attribute \
										))
										goto done
									end

									if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"delete from BESSEMER_MetadataAttributes where Attribute = :A1", \
											config.Attribute \
										))
										goto done
									end

								else

									configAttributes.(config.Attribute) = true
								end
							end


							// add in the attributes not already in the config

							Integer attributeID
							for attributeID in Assoc.Keys (attributes)		

								if not Assoc.IsKey (configAttributes, attributeID)

									RecArray max = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"select max (Ordering) from BESSEMER_MetadataAttributes")
									if not .CheckError (max); goto done; end

									if not max[1][1]  // e.g. NULL
										max[1][1] = 0
									end

									if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"insert into BESSEMER_MetadataAttributes (Attribute, Ordering) values (:A1, :A2)", \
											attributeID, \
											1 + max[1][1] \
										))
										goto done
									end

								end
							end


							// get the config for presentation

							RecArray configData = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select attr.Attribute, attr.HeaderSheet, catSubcat.Category, catSubcat.Subcategory, catSubcat.Requiredness"\
									+ " from BESSEMER_MetadataAttributes attr" \
									+ " left join BESSEMER_MetadataCatSubcat catSubcat" \
									+ " on attr.Attribute = catSubcat.Attribute" \
									+ " order by attr.Ordering, attr.Attribute" \
								)
							if not .CheckError (configData); goto done; end

							.data = {}

							Assoc entry

							for config in configData

								if config.Attribute != entry.attribute

									entry = Assoc.CreateAssoc()

									entry.attribute = config.Attribute
									entry.name = attributes.(config.Attribute).name
									entry.displayName = attributes.(config.Attribute).displayName
									entry.headerSheet = config.HeaderSheet
									entry.catSubcats = {}

									.data = { @.data, entry }
								end

								if config.Category
									entry.catSubcats = { @entry.catSubcats, { config.Category, config.Subcategory, config.Requiredness } }
								end
							end


						done:
							.EndTransaction()		

							return Undefined

						end

	Script _ConfigureAttribute






												function Boolean ConfigureAttribute (Assoc entry)

													return \
														entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras", '') and \
														entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "contact", '')

												end











	endscript

	Script _EditAttribute






												function Boolean EditAttribute (Assoc entry)

													return entry.name != "Name" \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "protocol") \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "contact") \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "fras") \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "level") \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "filedBy")

												end











	endscript

	Script _ReorderAttribute






												function Boolean ReorderAttribute (Assoc entry)

													return  entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "level") \
															and entry.name != CAPI.IniGet (.fPrgCtx.fDbConnect.fLogin, 'BESSEMER', "filedBy")

												end











	endscript

end
