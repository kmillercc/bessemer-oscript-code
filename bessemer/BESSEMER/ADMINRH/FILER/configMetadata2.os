package BESSEMER::ADMINRH::FILER

public Object configMetadata2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'data', -18, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							Integer order = 0

							.StartTransaction()

							if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, "delete from BESSEMER_MetadataConfiguration"))
								goto done
							end

							String attribute
							for attribute in Assoc.Keys (request.data)

								order += 1

								if not request.data.(attribute)
									if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"insert into BESSEMER_MetadataConfiguration (Attribute, Order) values (A1, :A2)", \
											attribute, \
											order \
										))
										goto done
									end

									continue
								end

								List categoryItems
								for categoryItems in request.data.(attribute)

									if not .CheckError (CAPI.Exec (prgCtx.fDBConnect.fConnection, \
											"insert into BESSEMER_MetadataConfiguration (Attribute, Category, Subcategory, Order) values (:A1, :A2, :A3, :A4)", \
											Attribute, \
											categoryItems[1], \
											categoryItems[2], \
											Order \
										))
										goto done
									end
								end
							end

							.fLocation = .URL() + "?func=admin.index"


						done:
							.EndTransaction()

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'data', Assoc.AssocType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
