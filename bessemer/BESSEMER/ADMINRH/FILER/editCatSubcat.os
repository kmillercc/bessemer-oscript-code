package BESSEMER::ADMINRH::FILER

public Object editCatSubcat inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	data
	override	Boolean	fEnabled = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.data = Assoc.CreateAssoc()

							Record data
							for data in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_CategorySubcategory order by Category, Subcategory" \
								)
								if not .data.(data.Category)
									.data.(data.Category) = {}
								end

								// hack: encode in name
								if data.SystemOnly
									data.Subcategory += " -- System only"
								end

								.data.(data.Category) = { @.data.(data.Category), data.Subcategory }
							end

							return Undefined
						end

end
