package BESSEMER::ADMINRH::FILER

public Object configPresentation inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Boolean	accountAutoload = FALSE
	public		Integer	accountWidth
	public		String	backgroundColor
	public		Integer	categoryWidth
	public		Boolean	contactAutoload = FALSE
	public		Integer	contactWidth
	public		Integer	contactsEntries
	public		String	emptyMessage
	public		String	exceededMessage
	override	Boolean	fEnabled = TRUE
	public		Boolean	familyAutoload = FALSE
	public		Integer	familyWidth
	public		Integer	frasEntries
	public		String	insufficientMessage
	public		String	itemBackgroundColor
	public		Boolean	relationshipAutoload = FALSE
	public		Integer	relationshipWidth
	public		Integer	responseLimit
	public		Boolean	subaccountAutoload = FALSE
	public		Integer	subaccountWidth
	public		Integer	subcategoryWidth
	public		Integer	threshold



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config

							for config in { \
									"familyWidth", \
									"relationshipWidth", \
									"accountWidth", \
									"subaccountWidth", \
									"contactWidth", \
									"categoryWidth", \
									"subcategoryWidth" \
								}

								.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config, 150)

							end

							for config in { \
									"frasEntries", \
									"contactsEntries" \
								}

								.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config, 5)

							end

							for config in { \
									"backgroundColor", \
									"itemBackgroundColor" \
								}

								.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config, "#e0e0e0")

							end

							for config in { \
									"threshold", \
									"responseLimit", \
									"familyAutoload", \
									"relationshipAutoload", \
									"accountAutoload", \
									"subaccountAutoload", \
									"contactAutoload", \
									"exceededMessage", \
									"emptyMessage", \
									"insufficientMessage" \
								}

								.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config)

							end

							return Undefined
						end

end
