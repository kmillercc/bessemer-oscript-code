package BESSEMER::ADMINRH::PERMISSIONS

public object configPermissions2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'cs_list', 2, '', FALSE, Undefined, TRUE }, { 'family_list', 5, '', FALSE, Undefined, TRUE }, '__LL_CheckPost__' }


	override function Dynamic Execute( \
									Dynamic		ctxIn, \
									Dynamic		ctxOut, \
									Record		request )
									
		Object		prgCtx	= .PrgSession()
		
		if length (request.field_list) != length (request.value_list) or \
				length (request.field_list) != length (request.group_list) or \
				length (request.field_list) != length (request.special_list)
			.fError = "Invalid arguments"
			return undefined
		end
	
		
		.StartTransaction()
		
		._Process (prgCtx, request)
		
		.EndTransaction()
	
		.fLocation = .URL() + "?func=admin.index"
		
		return undefined
	
	end
								
								
	private function Dynamic _Process (Object prgCtx, Record request)
	
		if not .CheckError ( \
				CAPI.Exec (prgCtx.fDBConnect.fConnection, "delete from BESSEMER_Permissions") \
			)
			return undefined		
		end
		
		Integer i
		for i = 1 to length (request.field_list)
		
			continueif not request.field_list[i]  // all will be blank
			
			Integer groupID
			
			switch request.group_list[i]
			
				case "F-*"
					groupID = -1
					end
					
				case "R-*"
					groupID = -2
					end
					
				case "A-*"
					groupID = -3
					end
					
				case "S-*"
					groupID = -4
					end
					
				case "C-*"
					groupID = -5
					end
					
				default
					RecArray groupInfo = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
						"select ID from KUAF where Name=:A1 and Type=:A2 and Deleted=0", request.group_list[i], UAPI.Group)
					if not .CheckError (groupInfo); return undefined; end
					
					if length (groupInfo) == 0
						.fError = Str.Format ("Unknown group: %1", request.group_list[i])
						return undefined
					end
					
					groupID = groupInfo[1][1]
				end
			end		
			
		
			.CheckError ( \
					CAPI.Exec (prgCtx.fDBConnect.fConnection, \
						"insert into BESSEMER_Permissions" \
						+ " (Field, Value, GroupID, Special, Ordering)" \
						+ " values (:A1, :A2, :A3, :A4, :A5)", \
						request.field_list[i], \
						request.value_list[i], \
						groupID, \
						request.special_list[i] ? request.special_list[i] : 0, \
						i \
					) \
				)
		end
		
		return undefined
	
	end
						
	override function void SetPrototype()
					
					.fPrototype =\
						{\
							{ 'field_list', StringType, '', FALSE, Undefined, TRUE }, \
							{ 'value_list', StringType, '', FALSE, Undefined, TRUE }, \
							{ 'group_list', IntegerType, '', FALSE, Undefined, TRUE }, \
							{ 'special_list', IntegerType, '', FALSE, Undefined, TRUE }, \
							$WebDsp.kCheckPost \
						}
				end

end
