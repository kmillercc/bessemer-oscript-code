package BESSEMER::ADMINRH::PERMISSIONS

public object configPermissions inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	data
	override	Boolean	fEnabled = TRUE


	
	override function Dynamic Execute( \
					Dynamic		ctxIn, \
					Dynamic		ctxOut, \
					Record		request )
					
					Object		prgCtx	= .PrgSession()
					
					.data = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
							"select Field, Value, GroupID, Special from BESSEMER_Permissions order by Ordering" \
						)
					if not .CheckError (.data); return undefined; end
					
					RecArray.AddField (.data, 'GroupName')
					
					Record row
					for row in .data
					
						String groupName
						
						switch row.GroupID
						
							case -1
								groupName = "F-*"
								end
							
							case -2
								groupName = "R-*"
								end
							
							case -3
								groupName = "A-*"
								end
							
							case -4
								groupName = "S-*"
								end
							
							case -5
								groupName = "C-*"
								end
							
							default
								RecArray groupInfo = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
									"select Name from KUAF where ID=:A1 and Type=:A2", row.GroupID, UAPI.Group)
								if not .CheckError (groupInfo); return undefined; end
								
								groupName = length (groupInfo) == 0 ? '(unknown)' : groupInfo[1][1]
							end
						end
						
						row.groupName = groupName
							
					end
					
					return Undefined
				end

end
