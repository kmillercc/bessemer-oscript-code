package BESSEMER::ADMINRH::STRUCTURE

public Object processFeed inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	public		Dynamic	log



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							Boolean verbose = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'feedProcessorVerbose', false)

							.log = $BESSEMER.FeedProcessor.ProcessFeed(prgCtx, verbose).messages

							return Undefined
						end

end
