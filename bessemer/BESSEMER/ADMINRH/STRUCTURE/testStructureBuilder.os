package BESSEMER::ADMINRH::STRUCTURE

public Object testStructureBuilder inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	data
	override	Boolean	fEnabled = TRUE
	public		Dynamic	table


	override Script Execute







												function Dynamic Execute( \
													Dynamic		ctxIn, \
													Dynamic		ctxOut, \
													Record		request )

													Object		prgCtx	= .PrgSession()

													if request.Operation

														RecArray sequence = CAPI.Exec (.fPrgCtx.fDBConnect.fConnection, \
															"select max (Sequence) from BESSEMER_FRASC_Feed" \
														)
														if not .CheckError (sequence); return undefined; end

														Integer inserted = CAPI.Exec (.fPrgCtx.fDBConnect.fConnection, \
															"insert into BESSEMER_FRASC_Feed ( " \
																+ "Sequence," \
																+ "Operation," \
																+ "Family,Relationship,Account,Subaccount,Contact," \
																+ "NewFamily,NewRelationship,NewAccount,NewSubaccount,NewContact," \
																+ "AddDate" \
																+ ") VALUES (" \
																+ ":A1,:A2,:A3,:A4,:A5,:A6,:A7,:A8,:A9,:A10,:A11,:A12,:A13" \
																+ ")", \
																sequence[1][1] ? 1 + sequence[1][1] : 1, \
																request.Operation, \
																request.Family, \
																request.Relationship, \
																request.Account, \
																request.Subaccount, \
																request.Contact, \
																request.newFamily, \
																request.newRelationship, \
																request.newAccount, \
																request.newSubaccount, \
																request.newContact, \
																Date.Now() \
															)
														if not .CheckError (inserted); return undefined; end

													end


													.data = Assoc.CreateAssoc()

													.data.families = Get (prgctx, "Families")
													.data.relationships = Get (prgctx, "Relationships")
													.data.accounts = Get (prgctx, "Accounts")
													.data.subaccounts = Get (prgctx, "Subaccounts")
													.data.contacts = Get (prgctx, "Contacts")


													.table = CAPI.Exec (.fPrgCtx.fDBConnect.fConnection, \
															"select Operation,Family,Relationship,Account,Subaccount,Contact,NewFamily,NewRelationship,NewAccount,NewSubaccount,NewContact" \
															+ " from BESSEMER_FRASC_Feed where CompleteDate IS NULL order by Sequence")
													if not .CheckError (.table); return undefined; end

													return Undefined
												end



												function List Get (Object prgCtx, String name)

													Integer nodeID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'my' + name)
													if not nodeID
														return Undefined
													end	

													List names

													DAPINode node 
													for node in DAPI.ListSubNodes (DAPI.GetNodeByID (prgCtx.DAPISess(), 0, nodeID))
														names = { @names, node.pName }
													end

													return names

												end












	endscript

end
