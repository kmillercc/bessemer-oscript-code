package BESSEMER::ADMINRH::STRUCTURE

public Object processFeedHistory inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	data
	override	Boolean	fEnabled = TRUE
	public		Dynamic	table



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							//Object		prgCtx	= .PrgSession()

							.table = CAPI.Exec (.fPrgCtx.fDBConnect.fConnection, \
									"select * from BESSEMER_FRASC_Feed order by Sequence")

							return Undefined
						end

end
