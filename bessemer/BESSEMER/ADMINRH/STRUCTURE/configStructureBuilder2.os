package BESSEMER::ADMINRH::STRUCTURE

public Object configStructureBuilder2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'afterDirectorySynchronization', 5, '', FALSE }, { 'everyFiveMinutes', 5, '', FALSE }, { 'repository_ID', 2, '', FALSE }, { 'familyFacet_ID', 2, '', FALSE }, { 'relationshipFacet_ID', 2, '', FALSE }, { 'accountFacet_ID', 2, '', FALSE }, { 'subaccountFacet_ID', 2, '', FALSE }, { 'contactFacet_ID', 2, '', FALSE }, { 'hierarchyRoot_ID', 2, '', FALSE }, { 'myFamilies_ID', 2, '', FALSE }, { 'myRelationships_ID', 2, '', FALSE }, { 'myAccounts_ID', 2, '', FALSE }, { 'mySubaccounts_ID', 2, '', FALSE }, { 'myContacts_ID', 2, '', FALSE }, { 'showSelectedLocation', 5, '', FALSE }, { 'showSelectedFacets', 5, '', FALSE }, { 'showLocationColumn', 5, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"repository_ID", \
									"familyFacet_ID", \
									"relationshipFacet_ID", \
									"accountFacet_ID", \
									"subaccountFacet_ID", \
									"contactFacet_ID", \
									"hierarchyRoot_ID", \
									"myFamilies_ID", \
									"myRelationships_ID", \
									"myAccounts_ID", \
									"mySubaccounts_ID", \
									"myContacts_ID", \
									"showSelectedLocation", \
									"showSelectedFacets", \
									"showLocationColumn" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config[-3:] == '_ID' ? config[:-4] : config, \
										request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							.fLocation = .URL() + "?func=admin.index"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'repository_ID', IntegerType, '', FALSE }, \
									{ 'familyFacet_ID', IntegerType, '', FALSE }, \
									{ 'relationshipFacet_ID', IntegerType, '', FALSE }, \
									{ 'accountFacet_ID', IntegerType, '', FALSE }, \
									{ 'subaccountFacet_ID', IntegerType, '', FALSE }, \
									{ 'contactFacet_ID', IntegerType, '', FALSE }, \
									{ "hierarchyRoot_ID", IntegerType, '', FALSE }, \
									{ "myFamilies_ID", IntegerType, '', FALSE }, \
									{ "myRelationships_ID", IntegerType, '', FALSE }, \
									{ "myAccounts_ID", IntegerType, '', FALSE }, \
									{ "mySubaccounts_ID", IntegerType, '', FALSE }, \
									{ "myContacts_ID", IntegerType, '', FALSE }, \
									{ 'showSelectedLocation', BooleanType, '', FALSE }, \
									{ 'showSelectedFacets', BooleanType, '', FALSE }, \
									{ 'showLocationColumn', BooleanType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
