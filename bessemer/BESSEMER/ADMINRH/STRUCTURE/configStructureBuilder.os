package BESSEMER::ADMINRH::STRUCTURE

public Object configStructureBuilder inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		String	accountFacetPath
	public		Integer	accountFacet_ID
	public		String	contactFacetPath
	public		Integer	contactFacet_ID
	override	Boolean	fEnabled = TRUE
	public		String	familyFacetPath
	public		Integer	familyFacet_ID
	public		String	hierarchyRootPath
	public		Integer	hierarchyRoot_ID
	public		String	myAccountsPath
	public		Integer	myAccounts_ID
	public		String	myContactsPath
	public		Integer	myContacts_ID
	public		String	myFamiliesPath
	public		Integer	myFamilies_ID
	public		String	myRelationshipsPath
	public		Integer	myRelationships_ID
	public		String	mySubaccountsPath
	public		Integer	mySubaccounts_ID
	public		String	relationshipFacetPath
	public		Integer	relationshipFacet_ID
	public		String	repositoryPath
	public		Integer	repository_ID
	public		Boolean	showLocationColumn = FALSE
	public		Boolean	showSelectedFacets = FALSE
	public		Boolean	showSelectedLocation = FALSE
	public		String	subaccountFacetPath
	public		Integer	subaccountFacet_ID



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"repository_ID", \
									"familyFacet_ID", \
									"relationshipFacet_ID", \
									"accountFacet_ID", \
									"subaccountFacet_ID", \
									"contactFacet_ID", \
									"hierarchyRoot_ID", \
									"myFamilies_ID", \
									"myRelationships_ID", \
									"myAccounts_ID", \
									"mySubaccounts_ID", \
									"myContacts_ID", \
									"showSelectedLocation", \
									"showSelectedFacets", \
									"showLocationColumn" \
								}

								if config[-3:] == '_ID'  // a setting with a node path

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config[:-4])
									continueif not .(config)

									DAPINode node = DAPI.GetNodeById (prgCtx.DAPISess(), 0, .(config))
									continueif IsError(node)

									.(config[:-4] + "Path") = $LLIAPI.LLNodeSubsystem.GetItem (node.pSubtype).NodePath (node).Path

								else

									.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config)

								end

							end

							return Undefined
						end

end
