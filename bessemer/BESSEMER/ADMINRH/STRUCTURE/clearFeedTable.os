package BESSEMER::ADMINRH::STRUCTURE

public Object clearFeedTable inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							//Object		prgCtx	= .PrgSession()

							.CheckError (CAPI.Exec (.fPrgCtx.fDBConnect.fConnection, "delete from BESSEMER_FRASC_Feed"))

							.fLocation = .URL() + "?func=bessemer.processFeedHistory"

							return Undefined
						end

end
