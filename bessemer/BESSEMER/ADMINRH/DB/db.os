package BESSEMER::ADMINRH::DB

public Object db inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Boolean	abortOnError = TRUE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object dbConnect = .fPrgCtx.fDBConnect

							Assoc substitutions = $DBWIZAPI.DBWizApiSubsystem.SetUpWizData().APIToAssoc()
							substitutions.IniSection = 'Module.VersionInfo'	
							substitutions.IniKeyword = 'bessemer'

							String statement = ""

							String line
							for line in Str.Elements ( \
									Str.Substitute ( \
										Compiler.Source (.dbscript), \
										Assoc.ToRecord (substitutions) \
									), \
									str.eol() \			
								)

								line = Str.Trim (Str.Compress (line))

								continueif line[1] == "#"
								continueif line == ""

								if line[1] != "/"
									statement += line + " "
									continue
								end

								statement = Str.Trim (statement)
								continueif statement == ""

								echo (statement)

								if not .abortOnError

									dbConnect.StartTrans()

									CAPI.Exec (dbConnect.fConnection, statement)

									dbConnect.EndTrans (true)	

								else
									if not dbConnect.StartTrans()
										.fError = "Cannot start db transaction"
										return undefined
									end

									if not .CheckError (CAPI.Exec (dbConnect.fConnection, statement))
										dbConnect.EndTrans (false)
										return undefined
									end

									if not dbConnect.EndTrans (true)	
										.fError = "Error ending db transaction"
										return undefined
									end
								end

								statement = ""
							end

							.fLocation = .URL() + "?func=bessemer.operationcomplete&operation=Database+operation+completed+successfully"

							return undefined
						end

	Script dbscript

















	endscript

end
