package BESSEMER::ADMINRH::DB

public Object dbCreate inherits BESSEMER::ADMINRH::DB::db

	override	Boolean	fEnabled = TRUE


	override Script dbscript






												#ifdef COMPILEME

												CREATE TABLE %prefix%BESSEMER_FRASC_Feed (
													Sequence		%type_int%		not null,
													Operation		char(1)			not null,
													Family			%type_char_64%	null,
													Relationship	%type_char_64%	null,
													Account			%type_char_64%	null,
													Subaccount		%type_char_64%	null,
													Contact			%type_char_64%	null,
													NewFamily		%type_char_64%	null,
													NewRelationship	%type_char_64%	null,
													NewAccount		%type_char_64%	null,
													NewSubaccount	%type_char_64%	null,
													NewContact		%type_char_64%	null,
													AddDate			%type_date%		null,
													CompleteDate	%type_date%		null,
													IssueMessage	%type_char_255%	null	
												)
												/

												ALTER TABLE %prefix%BESSEMER_FRASC_Feed ADD CONSTRAINT BESSEMER_FRASC_Feed_PK PRIMARY KEY (Sequence)
												/

												CREATE INDEX %prefix%BESSEMER_FRASC_CompleteDate ON %Prefix%BESSEMER_FRASC_Feed (CompleteDate)
												/

												CREATE TABLE %prefix%BESSEMER_FRAS_Contacts (
													Family			%type_char_64%	null,
													Relationship	%type_char_64%	null,
													Account			%type_char_64%	null,
													Subaccount		%type_char_64%	null,
													Contact			%type_char_64%	null
												)
												/

												CREATE INDEX %prefix%BESSEMER_FRAS_Contacts_FRAS ON %Prefix%BESSEMER_FRAS_Contacts (Family, Relationship, Account, Subaccount)
												/

												CREATE INDEX %prefix%BESSEMER_FRAS_Contacts_X ON %Prefix%BESSEMER_FRAS_Contacts (Contact)
												/

												CREATE TABLE %prefix%BESSEMER_CategorySubcategory (
													Category		%type_char_64%	not null,
													Subcategory		%type_char_64%	null,
													SystemOnly		%type_int%		null,
													FRAS			%type_int% 		null	
												)
												/

												CREATE TABLE %prefix%BESSEMER_MetadataAttributes (
													Attribute		%type_int% 	not null,
													Ordering		%type_int% 	not null,
													HeaderSheet		%type_int% 	null
												)
												/

												ALTER TABLE %prefix%BESSEMER_MetadataAttributes ADD CONSTRAINT BESSEMER_MetadataAttributes_PK PRIMARY KEY (Attribute)
												/

												CREATE TABLE %prefix%BESSEMER_MetadataCatSubcat (
													Attribute		%type_int% 		not null,
													Category		%type_char_64% 	not null,
													Subcategory		%type_char_64% 	null,
													Requiredness	%type_int%      null
												)
												/

												CREATE TABLE %prefix%BESSEMER_Permissions (
													Field			%type_char_64%	not null,
													Value			%type_char_64% 	not null,
													GroupID			%type_int% 		not null,
													Special			%type_int%		not null,
													Ordering		%type_int%		not null
												)
												/

												#endif











	endscript

end
