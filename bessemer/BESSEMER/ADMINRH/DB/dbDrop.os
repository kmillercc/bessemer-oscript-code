package BESSEMER::ADMINRH::DB

public Object dbDrop inherits BESSEMER::ADMINRH::DB::db

	override	Boolean	abortOnError = FALSE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { '__LL_CheckPost__' }



	override function void SetPrototype()

							.fPrototype =\
								{\
									$WebDsp.kCheckPost \
								}
						end

	override Script dbscript






												#ifdef COMPILEME

												DROP TABLE %prefix%BESSEMER_FRASC_Feed
												/

												DROP TABLE %prefix%BESSEMER_FRAS_Contacts
												/

												DROP TABLE %prefix%BESSEMER_CategorySubcategory
												/

												DROP TABLE %prefix%BESSEMER_MetadataAttributes
												/

												DROP TABLE %prefix%BESSEMER_MetadataCatSubcat
												/

												DROP TABLE %prefix%BESSEMER_Permissions
												/

												#endif











	endscript

end
