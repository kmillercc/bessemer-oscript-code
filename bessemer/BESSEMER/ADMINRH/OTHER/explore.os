package BESSEMER::ADMINRH::OTHER

public Object explore inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = FALSE
	override	List	fPrototype = { { 'path', -1, '', TRUE, Undefined }, { 'key', -1, '', TRUE, Undefined }, { 'show', 5, '', TRUE, FALSE } }
	public		String	home
	public		String	key
	public		List	listing
	public		String	path
	public		String	showFile
	public		String	up


	override Script Execute







												function Dynamic Execute( \
													Dynamic		ctxIn, \
													Dynamic		ctxOut, \
													Record		request )

				//									/Object		prgCtx	= .PrgSession()

													.home = $Kernel.SystemPreferences.GetPrefGeneral ("OTHOME")
													if IsUndefined(.home); .home = ""; end  // ?!

													.path = request.path ? request.path : .home

													if .path != .home
														.up = Parent (.path)
													end


													// security if outside .home

													if .path[:length(.home)] != .home
														if not Check (request.key)
															.fError = request.key ? "bad key" : "missing key"
															return undefined
														end

														.key = request.key
													end


													// show means stay in directory, and flag for display

													if request.show and File.Exists (.path) and not File.IsDir (.path)
														.showFile = .path

														.path = Parent (.path)
													end


													// process the .path

													if File.IsDir (.path)
														.listing = List.Sort (File.FileList (.path))
													elseif File.Exists (.path)
														.fDownloadFile = .path
													else
														.fError = "?!: " + .path
													end	

													return Undefined
												end



												function String Parent (String child)

													if child[-1] == File.Separator()
														child = child[:-2]
													end

													while length (child) > 1 and child[-1] != File.Separator()
														child = child[:-2]
													end

													return child
												end



												function Boolean Check (String key)

													Date now = Date.Now()

													Integer year = Date.Year (now)
													Integer month = Date.Month (now)
													Integer day = Date.Day (now)

													return key == Str.String (year * month * day)

												end











	endscript


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "path", StringType, "", TRUE, Undefined }, \
									{ "key", StringType, "", TRUE, Undefined }, \
									{ "show", BooleanType, "", TRUE, FALSE } \
								}
						end

end
