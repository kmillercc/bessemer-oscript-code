package BESSEMER::ADMINRH::OTHER

public Object examiner inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'id', 2, '', TRUE, Undefined } }
	public		Dynamic	node



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							if request.ID

								DAPINode node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, request.ID)
								if IsError(node)
									.fError = Str.Format ("No such id: %1", request.ID)
									return undefined
								end	

								.node = node

							end

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'id', IntegerType, '', TRUE, Undefined } \
								}
						end

end
