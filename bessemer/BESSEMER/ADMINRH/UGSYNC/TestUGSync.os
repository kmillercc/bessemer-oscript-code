package BESSEMER::ADMINRH::UGSYNC

public Object TestUGSync inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE


	/*
						* This is the method subclasses implement to provide their functionality.
						*
						* @param {Dynamic} ctxIn
						* @param {Dynamic} ctxOut
						* @param {Record} request
						*
						* @param {Dynamic} Undefined
						*/
	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Frame agent = $Bessemer.UserGroupSync.New()

							if IsDefined(agent) and agent.fEnabled
								agent.fPrgCtx = .fPrgCtx
								agent.fNow = Date.Now()
								agent.Execute()
							end

							return Undefined
						end

end
