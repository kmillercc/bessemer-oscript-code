package BESSEMER::ADMINRH::CORE

public Object bulkDelete2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String root
							for root in { "myFamilies", "myRelationships", "myAccounts", "mySubaccounts", "myContacts", "hierarchyRoot" }

								continueif not RecArray.IsColumn (request, root)

								Integer ID = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', root)
								if IsUndefined(ID)
									.fError = root + " not configured"
									return undefined
								end	

								DAPINode rootNode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, ID)
								if IsError(rootNode)
									.fError = "missing " + root
									return undefined
								end

								RecArray items = CAPI.Exec ( \
										prgCtx.fDBConnect.fConnection, \
										"select DataID from DTree where ParentID = :A1 and SubType = " + Str.String ($TypeVirtualFolder), \
										rootNode.pID \
									)
								if not .CheckError (items); return undefined; end

								Record item
								for item in items
									DAPINode node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, item.DataID)
									if node
										$LLIAPI.LLNodeSubsystem.GetItem (node.pSubType).NodeDelete (node)
									end
								end
							end


							.fLocation = .URL() + "?func=bessemer.bulkDelete"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									$WebDsp.kCheckPost \
								}
						end

end
