package BESSEMER::ADMINRH::CORE

public Object utilities inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Boolean	autoCreateGroup = FALSE
	override	Boolean	fEnabled = TRUE
	public		String	facetSource
	public		Boolean	feedProcessorVerbose = FALSE
	public		Boolean	forceSort = FALSE
	public		Boolean	ignoreUserPermissionErrors = FALSE
	public		Integer	maxFilerTime
	public		String	separator
	public		Boolean	standardSort = FALSE
	public		Boolean	timings = FALSE



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"autoCreateGroup", \
									"feedProcessorVerbose", \
									"forceSort", \
									"facetSource", \
									"timings", \
									"separator", \
									"ignoreUserPermissionErrors", \
									"maxFilerTime" \
								}

								.(config) = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', config)
							end

							.standardSort = $Kernel.SystemPreferences.GetPrefBoolean ("bessemer", "standardSort")

							return Undefined
						end

end
