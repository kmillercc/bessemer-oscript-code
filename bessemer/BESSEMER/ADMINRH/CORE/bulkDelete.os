package BESSEMER::ADMINRH::CORE

public Object bulkDelete inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	public		Dynamic	counts
	override	Boolean	fEnabled = TRUE
	public		Dynamic	paths



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							.paths = Assoc.CreateAssoc()
							.counts = Assoc.CreateAssoc()

							String root
							for root in { "myFamilies", "myRelationships", "myAccounts", "mySubaccounts", "myContacts", "hierarchyRoot" }

								// find myfrasch

								Integer ID = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', root)
								if IsUndefined(ID)
									.fError = root + " not configured"
									return undefined
								end	

								DAPINode rootNode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, ID)
								if IsError(rootNode)
									.fError = "missing " + root
									return undefined
								end

								.paths.(root) = $LLIAPI.LLNodeSubsystem.GetItem (rootNode.pSubType).NodePath (rootNode).path

								.counts.(root) = CAPI.Exec ( \
										prgCtx.fDBConnect.fConnection, \
										"select count(*) from DTree where ParentID = :A1 and SubType = " + Str.String ($TypeVirtualFolder), \
										rootNode.pID \
									)
								if .CheckError (.counts.(root))
									.counts.(root) = .counts.(root)[1][1]
								end
							end	

							return Undefined

						end

end
