package BESSEMER::ADMINRH::CORE

public Object processScannedItems inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	public		List	fLog

	public function Err (String x)

		.Log (x)

	end


	override function Dynamic Execute( \
									Dynamic		ctxIn, \
									Dynamic		ctxOut, \
									Record		request )

		Object x = OS.NewTemp($BESSEMER.ScannedItemsProcessor)

		Frame f = $Kernel.FrameWrapper.New(x)

		x.fPrgCtx = .fPrgCtx
		x.fController = this

		x.Execute()

		return Undefined
	end


	public function Log (String x)

		.fLog = { @.fLog, x }

	end

end
