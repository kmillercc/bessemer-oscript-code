package BESSEMER::ADMINRH::CORE

public Object utilities2 inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'autoCreateGroup', 5, '', FALSE }, { 'feedProcessorVerbose', 5, '', FALSE }, { 'standardSort', 5, '', FALSE }, { 'forceSort', 5, '', FALSE }, { 'facetSource', -1, '', FALSE, '' }, { 'timings', 5, '', FALSE, '' }, { 'separator', -1, '', FALSE, '' }, { 'ignoreUserPermissionErrors', 5, '', FALSE }, { 'maxFilerTime', 2, '', FALSE }, '__LL_CheckPost__' }



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							Object		prgCtx	= .PrgSession()

							String config
							for config in { \
									"autoCreateGroup", \
									"feedProcessorVerbose", \
									"forceSort", \
									"facetSource", \
									"timings", \
									"separator", \
									"ignoreUserPermissionErrors" \
								}

								Dynamic result = CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										config, \
										request.(config) \
									)
								if IsError (result)
									.fError = result
									return undefined
								end

							end	

							if request.maxFilerTime == -1
								CAPI.IniDelete ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										'maxFilerTime' \
									)
							else
								CAPI.IniPut ( \
										prgCtx.fDbConnect.fLogin, \
										'BESSEMER', \
										'maxFilerTime', \
										request.maxFilerTime \
									)
							end


							$Kernel.SystemPreferences.AddPref ("bessemer", "standardSort", request.standardSort ? 'true' : 'false')


							.fLocation = .URL() + "?func=admin.index"

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ 'autoCreateGroup', BooleanType, '', FALSE }, \
									{ 'feedProcessorVerbose', BooleanType, '', FALSE }, \
									{ 'standardSort', BooleanType, '', FALSE }, \
									{ 'forceSort', BooleanType, '', FALSE }, \
									{ 'facetSource', StringType, '', FALSE, '' }, \
									{ 'timings', BooleanType, '', FALSE, '' }, \
									{ 'separator', StringType, '', FALSE, '' }, \
									{ 'ignoreUserPermissionErrors', BooleanType, '', FALSE }, \
									{ 'maxFilerTime', IntegerType, '', FALSE }, \
									$WebDsp.kCheckPost \
								}
						end

end
