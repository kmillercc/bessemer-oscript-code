package BESSEMER::ADMINRH::CORE

public Object operationComplete inherits BESSEMER::ADMINRH::AdminLLRequestHandler

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'operation', -1, '', FALSE }, { 'nextFunc', -1, '', TRUE, 'admin.index' } }
	public		String	nextFunc
	public		String	operation



	override function Dynamic Execute( \
							Dynamic		ctxIn, \
							Dynamic		ctxOut, \
							Record		request )

							// Object		prgCtx	= .PrgSession()

							.operation = request.operation
							.nextFunc = request.nextFunc

							return Undefined
						end


	override function void SetPrototype()

							.fPrototype =\
								{\
									{ "operation", StringType, "", FALSE }, \
									{ "nextFunc", StringType, "", TRUE, "admin.index" } \
								}
						end

end
