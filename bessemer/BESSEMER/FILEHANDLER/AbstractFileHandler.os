package BESSEMER::FILEHANDLER

public Object AbstractFileHandler inherits BESSEMER::Root


	public function Assoc AddFile(Object prgCtx, Record request, Integer categoryID, Assoc attributes, Integer parentID, Frame timer)

				// abstract
				return .Problem('Not implemented')
			end


	public function Assoc UpdateFile(Object prgCtx, Record request, Integer categoryID, Assoc attributes, DAPINODE node)

				// abstract
				return .Problem('Not implemented')

			end


	private function Assoc _AttrSetValue (Frame attrData, Record request, Assoc attribute )

					String argumentName = Str.Format ("attribute%1", attribute.ID)


					// if it's not on the form we'll leave it be

					if not RecArray.IsColumn (request, argumentName) and attribute.type != 'Boolean'
						return .Success()
					end


					// single value or multiple values, whichever provided

					Dynamic values = RecArray.IsColumn (request, argumentName + "_list") ? \
							request.(argumentName + "_list") : { request.(argumentName) }


					// change incoming values to type

					Integer i
					for i = 1 to length (values)

						switch attribute.type
							case 'Date'
								values[i] = Date.StringToDate (values[i], '%m/%d/%Y')
							end

							case 'Real'
								values[i] = Str.StringToReal (values[i])
							end

							case 'Integer', 'IntegerPopup'
								values[i] = Str.StringToInteger (values[i])
							end

							case 'Boolean'
								values[i] = RecArray.IsColumn (request, argumentName) > 0
							end			
						end

					end

					// set
					if not attrData.SetValue(attribute.name, values)
						return .Problem(Str.Format('Unable to set value for attribute %1', attribute.name)) 
					end

					return .Success()
				end


	private function _IDName (Integer ID)

				String name = Str.Format ("%1", ID)
				while length (name) < 9
					name = "0" + name
				end

				return "3" + name

			end


	private function Assoc _ProcessAttributes (Object prgCtx, Record request, Integer categoryID, Assoc attributes, Integer nodeID, Integer versionNum = 0, Boolean doUpgrade = false)

				String nameFromAttr

				// first load the attributes
				Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, nodeID, versionNum)
				if !attrData.Load()
					return .Problem("Error accessing destination attributes")
				end

				// upgrade just in case one is needed		
				if doUpgrade
					attrData.attrData.Upgrade (categoryID)
				end


				// check config	
				String specialAttribute
				for specialAttribute in { "fras", "level", "protocol", "filedBy" }
					if not CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', specialAttribute)
						return .Problem(specialAttribute + " not configured")
					end

					if not attributes.(CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', specialAttribute))
						return .Problem("no attribute/set for " + specialAttribute)
					end
				end

				//--- process attributes
				Assoc attribute
				for attribute in attributes
					if attribute.name == "Name"

						String nameArgument = Str.Format ("attribute%1", attribute.ID)

						if not RecArray.IsColumn (request, nameArgument) or not request.(nameArgument)
							return .Problem("Name must be supplied")					
						end	

						nameFromAttr = request.(nameArgument)

					elseif attribute.name == "File"

						// handled later


					elseif attribute.name == CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'fras')

						// make lists of values for the subAttributes in this set

						Assoc values

						Integer child = 0

						String subAttr
						for subAttr in { "family", "relationship", "account", "subaccount" }

							String argumentName = Str.Format ("attribute%1-%2", attribute.ID, subAttr)

							child += 1
							if length (attribute.children) < child
								return .Problem("invalid fras definition")
							end

							values.(attribute.children[child]) = RecArray.IsColumn (request, argumentName + "_list") ? \
									request.(argumentName + "_list") : { request.(argumentName) }

						end

						attrData.SetInSet (attribute.name, values)

						// compute all the levels in this set of values
						Assoc levels
						Integer i
						for i = 1 to length (values[1])
							String thisLevel = ""

							String level
							for level in attribute.children
								breakif not values.(level)[i]
								thisLevel = level
							end

							levels.(thisLevel) = true
						end

						if not attrData.SetValue(CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'level'), Assoc.Keys (levels))
							return .Problem('missing Level attribute') 
						end


					elseif attribute.name == CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'protocol')

						// make lists of values for the subAttributes in this set

						if length (attribute.children) < 2
							return .Problem("invalid protocol definition")
						end	

						Assoc values

						String argumentName = Str.Format ("attribute%1-%2", attribute.ID, "category")

						values.(attribute.children[1]) = RecArray.IsColumn (request, argumentName + "_list") ? \
								request.(argumentName + "_list") : { request.(argumentName) }

						argumentName = Str.Format ("attribute%1-%2", attribute.ID, "subcategory")

						values.(attribute.children[2]) = \
								RecArray.IsColumn (request, argumentName + "_list") ? request.(argumentName + "_list") : \
								(RecArray.IsColumn (request, argumentName) ? { request.(argumentName) } : \
								{ '' })

						attrData.SetInSet (attribute.name, values)


					elseif attribute.name == CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'filedBy')

						attrData.SetValue(CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'filedBy'), { prgCtx.USession().fUsername })


					elseif attribute.name == CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'level')

						// handled above

					else

						Assoc status = ._AttrSetValue (attrData, request, attribute)
						if !status.ok
							return status
						end

					end


				end

				Assoc result = Assoc{'nameFromAttr': nameFromAttr, 'attrData': attrData}

				return .Success(result)

			end

end
