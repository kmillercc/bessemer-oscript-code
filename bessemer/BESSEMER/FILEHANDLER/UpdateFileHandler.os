package BESSEMER::FILEHANDLER

public Object UpdateFileHandler inherits BESSEMER::FILEHANDLER::AbstractFileHandler


	override function Assoc UpdateFile(Object prgCtx, Record request, Integer categoryID, Assoc attributesByName, DAPINODE node)

				// process the attributes
				Assoc	status = ._ProcessAttributes(prgCtx, request, categoryID, attributesByName, node.pID, node.pVersionNum)
				if !status.ok
					return status
				end			

				String nameFromAttr = status.result.nameFromAttr
				Frame attrData = status.result.attrData

				// get the llNode Object
				Object llNode = $LLIAPI.LLNodeSubsystem.GetItem (node.pSubtype)

				// set a flag to skipPermissionsCallback
				prgCtx.SetSessionProperty('skipPermAssignCallback', true)

				// update cats/atts
				status = llNode.NodeCategoriesUpdate(node, attrData.attrData)
				if !status.ok
					return Assoc{'ok': false, 'errMsg': "Error updating attributes: " + status.errMsg}
				end

				// set flag back to false
				prgCtx.SetSessionProperty('skipPermAssignCallback', false)

				// do some processing on the name
				String newName = ._GetName(nameFromAttr, node)

				// rename if the name changed
				if node.pName != newName
					status = llNode.NodeRename(node, newName)
					if !status.ok
						return Assoc{'ok': false, 'errMsg': "Error renaming: " + status.errMsg} 
					end
				end

				// Post update actions like permissions
				status = ._PostUpdateActions(prgCtx, node.pID, attributesByName, attrData)
				if !status.ok
					return status
				end			

				return Assoc{'ok': true}

			end


	private function String _GetName(String nameFromAttr, DAPINODE node)

				// rename if "name" attribute was changed
				// keep any [###] that was in the name
				Integer left = Str.RChr (node.pName, "[")
				Integer right = Str.RChr (node.pName, "]")

				String newName

				if left and right and right - left > 1
					newName = Str.Format ("%1 %2", nameFromAttr, node.pName[left : right])
				else
					newName = Str.Format ("%1 [%2]", nameFromAttr, ._IDName(node.pID))
				end

				return newName

			end


	private function Assoc _PostUpdateActions(Object prgCtx, Integer nodeID, Assoc attributesByName, Frame attrData)

				// final wrapup of new node; perform operations with elevated privs
				Assoc result = $LLIAPI.PrgSession.CreateNewNamed ( \
						$Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' ), \
						{ 'Admin', Undefined } )
				if !result.OK
					return .Problem("cannot obtain elevated privileges for final wrapup: " + result.errMsg)
				end

				Object adminCtx = result.pSession

				DAPINODE node = DAPI.GetNodeByID (adminCtx.DAPISess(), 0, nodeID)
				if IsError(node)
					return .Problem("Cannot reaccess node for _PostUpdateActions: " + Str.String (node))
				end		

				// update the document permissions				
				Assoc status = $Bessemer.DocPermissions.UpdatePermissions(adminCtx, node, attributesByName, attrData)				
				if !status.ok
					return status
				end				

				return .Success()

			end

end
