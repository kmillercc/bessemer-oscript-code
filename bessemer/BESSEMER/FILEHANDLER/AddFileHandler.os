package BESSEMER::FILEHANDLER

public Object AddFileHandler inherits BESSEMER::FILEHANDLER::AbstractFileHandler

	public		Integer	fSubtypeToCreate = 144

	override function Assoc AddFile(Object prgCtx, Record request, Integer categoryID, Assoc attributesByName, Integer parentID, Frame timer)

			DAPINODE node

			// get the parentNode
			DAPINode parentNode = DAPI.GetNodeById (prgCtx.DAPISess(), DAPI.BY_DATAID, parentID )
			if IsError(parentNode)
				return .Problem("ParentNode is not accessible: " + Str.String (parentNode))
			end	

			// process the attributes
			Assoc status = ._ProcessAttributes(prgCtx, request, categoryID, attributesByName, parentNode.pID)
			if !status.ok
				return status
			end

			String 	nameFromAttr = status.result.nameFromAttr
			Frame 	attrData = status.result.attrData

			// get the name
			String name = ._GetName(nameFromAttr)

			// now get the createInfo
			status = ._GetSetCreateInfo(prgCtx, attrData, request, attributesByName, parentNode)
			if !status.ok
				return status
			end

			Assoc createInfo = status.result

			// start timer
			timer.start ("create document")

				// set a flag to skipPermissionsCallback
				prgCtx.SetSessionProperty('skipPermAssignCallback', true)

					// allocate
					Object llnode = $LLIAPI.LLNodeSubsystem.GetItem (.fSubtypeToCreate)
					Assoc	result = llnode.NodeAlloc (parentNode, name)
					if !result.OK
						return .Problem(result.errMsg)
					end

					node = result.node

					// create
					result = llnode.NodeCreate (node, parentNode, createInfo)
					if !result.OK
						return .Problem(result.errMsg)
					end	

					// final wrap up
					status = .PostCreateActions(prgCtx, node.pID, nameFromAttr, attributesByName)
					if !status.ok
						return status
					end						

				// set flag back to false
				prgCtx.SetSessionProperty('skipPermAssignCallback', false)

			// stop timer				
			timer.stop()

			return .Success(node)
		end


	public function Assoc PostCreateActions(Object prgCtx, Integer nodeID, String nameFromAttr, Assoc attributesByName)

			// final wrapup of new node; perform operations with elevated privs
			Assoc result = $LLIAPI.PrgSession.CreateNewNamed ( \
					$Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' ), \
					{ 'Admin', Undefined } )
			if !result.OK
				return .Problem("cannot obtain elevated privileges for final wrapup: " + result.errMsg)
			end

			Object adminCtx = result.pSession

			DAPINODE node = DAPI.GetNodeByID (adminCtx.DAPISess(), 0, nodeID)
			if IsError(node)
				return .Problem("cannot reaccess node for wrapup: " + Str.String (node))
			end

			// get the attrdata fresh now
			Frame attrData = $BESSEMER.CategoryAttributes.New (adminCtx, node.pID, node.pVersionNum)
			attrData.Load()

			
			// update the document permissions
			Assoc status = ._SetPermissions(adminCtx, node, attributesByName, attrData)
			if !status.ok
				return status
			end
				
			// set ID into barcode attribute if present
			String barcodeAttribute = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'barcode')
			if barcodeAttribute
				if attrData.SetValue(barcodeAttribute, { ._IDName (node.pID) })
					attrData.Save()
				end
			end

			// Syntergy LTF 09/12/2016 - WE SHOULD BE ABLE TO USE THE ABOVE BARCODE LOGIC TO GET SYSTEM AND SET THE DOCUMENT ID 
			// Set ID into document ID attribute if present
			// String documentIDAttribute = CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'documentID')
			String documentIDAttribute = "Document ID"
			if documentIDAttribute
				if attrData.SetValue(documentIDAttribute, { ._IDName (node.pID) })
					attrData.Save()
				end
			end
			// Syntergy

			// rename with ID
			Integer renamed = DAPI.RenameNode (node, Str.Format ("%1 [%2]", nameFromAttr, ._IDName(node.pID)))
			if IsError (renamed)
				return .Problem("Error renaming: " + Str.String (renamed))
			end

			return .Success(node)
		end


	private function String _GetName(String nameFromAttr)

			// abstract must be overridden
			return undefined
		end


	private function Assoc _GetSetCreateInfo(Object prgCtx, Frame attrData, Record request, Assoc attributesByName, DAPINODE parentNode) 

			// setup the createInfo
			Assoc result = Assoc{'attrDataObj': attrData.attrData}

			return .Success(result)
		end

	private function Assoc _SetPermissions(Object adminCtx, 
											DAPINODE node, 
											Assoc attributesByName, 
											Frame attrData)
																				
		// update the document permissions				
		Assoc status = $Bessemer.DocPermissions.UpdatePermissions(adminCtx, node, attributesByName, attrData)				
		if !status.ok
			return status 
		end		
				
		return Assoc{'ok': true}
	end	
		
end
