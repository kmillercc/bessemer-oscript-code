package BESSEMER::FILEHANDLER

public Object AddRegularFile inherits BESSEMER::FILEHANDLER::AddFileHandler

	override	Integer	fSubtypeToCreate = 144



	override function Assoc PostCreateActions(Object prgCtx, Integer nodeID, String nameFromAttr, Assoc attributes)

				DAPINODE node

				Assoc status = .OSParent.PostCreateActions(prgCtx, nodeID, nameFromAttr, attributes)
				if !status.ok
					return status
				end	

				node = status.result

				// if not request.headerSheet
				// set ownership to Admin
				node.pUserID = 1000
				Integer updated = DAPI.UpdateNode (node)
				if IsError (updated)
					return .Problem("Error setting ownership: " + Str.String (updated))
				end

				return .Success()		

			end


	override function String _GetName( String newNameFromAttr )

				// not sure why this is done, but it's in the code
				return Str.String (Math.Random (10000000))

			end


	override function Assoc _GetSetCreateInfo( Object prgCtx, Frame attrData, Record request, Assoc attributes, DAPINODE parentNode )

				// setup the createInfo
				Assoc createInfo = Assoc{'attrDataObj': attrData.attrData}

				// regular file needs to inherit from repository 
				createInfo.inheritNode = parentNode

				if request.file

					String filePath = \
							CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'uploadFolderPath') + \
							File.Separator() + \
							request.file

					if not File.Exists (filePath)
						return .Problem(Str.Format ("File does not exist: %1", filePath))
					end

					Integer dotAt = Str.RChr (filePath, ".")
					String fileType = IsDefined (dotAt) ? filePath [dotAt + 1:] : ''

					createInfo.filename = File.GetName (filePath)
					createInfo.fileType = fileType
					createInfo.mimeType = $WebDsp.MimeTypePkg.GetFileExtMimeType (fileType)
					createInfo.platform = DAPI.PLATFORM_WINDOWS
					createInfo.versionFile = filePath


				// standard upload

				else  // no request.file

					String fileArgument = Str.Format ("attribute%1", attributes.File.ID)

					Assoc result = $WebDoc.WebDocUtils.GetFileUploadInfo (request, fileArgument)
					if not result.OK
						return .Problem(result.errMsg)
					end	

					createInfo.filename = result.filename
					createInfo.fileType = result.fileType
					createInfo.mimeType = result.mimeType
					createInfo.platform = DAPI.PLATFORM_WINDOWS
					createInfo.versionFile = result.versionFile

					// LTF - we need to copy to the protected upload directory to catch illeagal file types

					String filePath = Str.Format("%1%2_%3_%4", \
												CAPI.IniGet (prgCtx.GetCAPILogin(), 'BESSEMER', 'uploadFolderPath'), \
												File.Separator(), \
												System.ThreadIndex(), \
												createInfo.filename)


					if $Kernel.FileUtils.Copy(request.(fileArgument), filePath)
						// file type copy checked ok
						File.Delete(filePath)
					else
						return .Problem(Str.Format("Error copying file to path '%1.'", filePath))
					end	


				end		

				return .Success(createInfo)

			end

end
