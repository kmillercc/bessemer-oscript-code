package BESSEMER::FILEHANDLER

public Object AddHeaderSheet inherits BESSEMER::FILEHANDLER::AddFileHandler

	override	Integer	fSubtypeToCreate


	override function String _GetName( String newNameFromAttr )

			return newNameFromAttr

		end


	override function Assoc _GetSetCreateInfo(Object prgCtx, Frame attrData, Record request, Assoc attributes, DAPINODE parentNode) 

			// setup the createInfo
			Assoc result = Assoc{'attrDataObj': attrData.attrData}

			return .Success(result)
		end

	override function Assoc _SetPermissions( Object adminCtx,
												DAPINODE node,
												Assoc attributesByName,
												Frame attrData )

		// remove inherited permissions
		Assoc status = $BESSEMER.Utils.RemoveACLs(node)
		if !status.ok
			return status		
		end														
												
		// header sheet folder does not get new permissions	-- that comes later when
		// the scanned document is processed and moved into repository								
		return Assoc{'ok': TRUE }

	end

end
