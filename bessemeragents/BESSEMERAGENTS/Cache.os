package BESSEMERAGENTS

public Object Cache inherits BESSEMERAGENTS::Root

	public		Boolean	fBuilt = FALSE
	public		Assoc	fGroupIDs
	public		Assoc	fUserIDs



	public function Void BuildCache(Object prgCtx)

					// one time setup
					if !.fBuilt
						echo('Building cache of groupIDs...')

						String stmt = "select NAME, ID from KUAF where Name like 'RS-%' and Type=:A1 and Deleted=0"
						RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, UAPI.Group)

						Record rec
						for rec in recs
							.fGroupIDs.(rec.NAME) = rec.ID
						end

						.fBuilt = true
						echo('Finished building Cache.')
					end	
				end


	public function Assoc GetGroupID(Object prgCtx, String groupName, Boolean autoCreate = FALSE)

			// see if we have it in the cache
			Integer groupID = .fGroupIDs.(groupName)

			if IsUndefined(groupID)

			// Look for this group in KUAF
			String stmt = "select ID from KUAF where Name=:A1 and Type=:A2 and Deleted=0"
			RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, groupName, UAPI.Group)
				if IsError(recs) ||  Length(recs) == 0

			// didn't find it. check deleted?							


			// didn't find a deleted group with that name. AutoCreate?					
			if autoCreate
				echo(Str.Format("Group %1 does not exist. Creating it now.", groupName))

				Assoc status = $LLIAPI.UsersPkg.GroupNew(prgCtx.USession(), groupName)
				if !status.ok
					return status
						else
				groupID = status.groupID
				.fGroupIDs.(groupName) = groupID
			end	
					else
			return .Problem (Str.Format("Could not find group with name %1", groupName))
					end	
				else		
					groupID = recs[1].ID
					.fGroupIDs.(groupName) = groupID
		end
			end	
			return .Success(groupID)
		end

	public function Assoc GetUserID(Object prgCtx, String userName)

		// see if we have it in the cache
		Integer userID = .fUserIDs.(userName)

		if IsUndefined(userID)

			// Look for this group in KUAF
			String stmt = "select ID from KUAF where Name=:A1 and Type=:A2 and Deleted=0"
			RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, userName, UAPI.User)
			if IsError(recs) ||  Length(recs) == 0
				return .Problem (Str.Format("Could not find user with name %1", userName))
			
			// didn't find it. check for deleted?
			else		

				userID = recs[1].ID
				.fUserIDs.(userName) = userID
			end
		end			

		return .Success(userID)

		end


	public function Object New()

			Object o = OS.NewTemp( this )

			o.fGroupIDs = Assoc.CreateAssoc()
			o.fUserIDs = Assoc.CreateAssoc()

			return o

		end


	public function void TestGetGroupID()

			Object prgCtx = $BessemerAgents.Utils.GetAdminCtx().prgCtx

			echo(.GetGroupID(prgCtx, 'DefaultGroup'))

		end

end
