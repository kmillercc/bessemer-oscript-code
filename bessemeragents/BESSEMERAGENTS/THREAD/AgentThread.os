package BESSEMERAGENTS::THREAD

public Object AgentThread inherits BESSEMERAGENTS::Root

	public		Boolean	fEnabled = FALSE
	public		String	fThreadID = 'NONE'



	public function Assoc AgentsEnabled()

					Assoc rtn
					rtn.ok = true

					Object		sysPref = $Kernel.SystemPreferences
					List		iniSections = sysPref.ListSections()

					String		agentName = 'bessemer', section

					Boolean		hasAgentSection = false

					for section in iniSections 
						if Str.CmpI( section, agentName ) == 0
							hasAgentSection = true
							break
						end
					end

					if hasAgentSection
						Dynamic numberVal = sysPref.GetPref( agentName, "number", "0" )

						if Type( numberVal ) != IntegerType
							numberVal = Str.StringToInteger( Str.String( numberVal ) )
						end

						if IsDefined( numberVal )
							rtn.numThreads = numberVal
						end
					end

					String loaderValue = sysPref.GetPref( 'loader', 'load', '<null>' )

					List loadedMods = Str.Elements( loaderValue,';')
					String mod 

					Boolean found = false

					for mod in loadedMods
						if Str.CmpI( Str.Trim( mod ), agentName ) == 0
							found = true
							break
						end
					end

					if !found
						// check if it has its own section in [loader]
						String loaderSetting = sysPref.GetPref( 'loader', 'load_' + agentName, "<null>" )

						if Str.CmpI( loaderSetting, agentName ) == 0
							found = true
						end
					end

					rtn.result = found && hasAgentSection
					return rtn
				end


	public function Assoc ClearLocks( Object prgCtx = $BessemerAgents.Utils.GetAdminCtx().prgCtx, Integer threadIndex = System.ThreadIndex() )

					Assoc rtn
					rtn.ok = true

					if prgCtx.fDbConnect.StartTrans()
						Assoc status = .Query( prgCtx, "delete from BESSEMER_LOCK where WorkerID=:A1", { threadIndex } )	

						if !status.ok
							rtn = status
						end

						prgCtx.fDBConnect.EndTrans( rtn.ok )
					end

					return rtn
				End


	public function DisableAgents()

					/*
					 * Remove references to bessemer in the loader section of the opentext.ini
					 */

					List newLoaders 
					String val
					Object sysPref = $Kernel.SystemPreferences

					Dynamic loader_load = sysPref.GetPref( "loader", "load" )

					if IsDefined( loader_load )

						List loaders = Str.Elements( loader_load, ";" )

						for val in loaders
							if Str.CmpI( Str.Trim( val ), "bessemer" ) != 0
								newLoaders = { @newLoaders, val }	
							end 
						end

						// save prefs back without loader.
						sysPref.AddPref( "loader", "load", Str.Catenate( newLoaders, ";" )[:-2]	)
					end

					Dynamic load_bessemer = sysPref.GetPref( "loader", "load_bessemer" )

					if IsDefined( load_bessemer ) 
						sysPref.DelPref( "loader", "load_bessemer" )
					end

				End


	public function EnableAgents( Boolean ifAgentsEnabled = false )

					Assoc rtn
					rtn.ok = true

					Integer	numThreads = 1

					Object		sysPref = $Kernel.SystemPreferences
					List		iniSections = sysPref.ListSections()


					String		agentName = 'bessemer', section

					Boolean		hasAgentSection = false

					for section in iniSections 
						if Str.CmpI( section, agentName ) == 0
							hasAgentSection = true
							break
						end
					end

					//String modScriptsDir = $BessemerAgents.WebModule.PathPrefix() + 'scripts' + File.Separator()
					//String scriptsDir = $Kernel.ModuleUtils._OTHome() + 'scripts' + File.Separator()
					String scriptName = Str.Format('%1_run.e', Str.Lower( agentName ) )

					if !hasAgentSection
						sysPref.AddPref( agentName, 'lib', Str.Format( '.%1bin%1lljob.dll', File.Separator() ) )
						sysPref.AddPref( agentName, 'name', 'lljob' )
						sysPref.AddPref( agentName, 'prio', 'critical' )
						sysPref.AddPref( agentName, 'timeout', '5000' )
						sysPref.AddPref( agentName, 'info', Str.Format('.%2config%2opentext.ini;%1', agentName, File.Separator() ) )
						sysPref.AddPref( agentName, 'StartScript', Str.Format('.%1scripts%1llfull.lxe', File.Separator() ) )
						sysPref.AddPref( agentName, 'JobScript', Str.Format('.%2scripts%2%1', scriptName, File.Separator() ) )
						sysPref.AddPref( agentName, 'SleepIntervalSec', '30' )
					end

					sysPref.AddPref( agentName, "number", Str.String( numThreads ) )

					String loaderValue = sysPref.GetPref( 'loader', 'load', '<null>' )

					List loadedMods = Str.Elements( loaderValue,';')
					String mod 

					Boolean found = false

					for mod in loadedMods
						if Str.cmpI( Str.Trim( mod ), agentName ) == 0
							found = true
							break
						end
					end

					if !found
						// check if it has its own section in [loader]
						String loaderSetting = sysPref.GetPref( 'loader', 'load_' + agentName, "<null>" )

						if Str.CmpI( loaderSetting, agentName ) == 0
							found = true
						end
					end

					if !found	
						sysPref.AddPref( 'loader', 'load_' + agentName, agentName )
					end
				End


	public function Dynamic JobScript( \
					Assoc		params, \
					Dynamic		notCtxOut )

					if .fEnabled && $AgentEnabled
						.RunTasks()	
					end

					return 0
				end


	public function Assoc RemoveLock( Object prgCtx, Integer taskID )

					Assoc rtn
					rtn.ok = true

					if prgCtx.fDbConnect.StartTrans()
						Assoc status = .Query( prgCtx, "delete from BESSEMER_LOCK where WorkerID=:A1 and TaskID=:A2", { System.ThreadIndex(), taskID } )	

						if !status.ok
							rtn = status
						else
							rtn.result = status.result
						end

						prgCtx.fDBConnect.EndTrans( rtn.ok )
					end

					return rtn
				End


	public function Assoc ReserveTask( Object prgCtx, Integer taskID, Integer runCount, Date nextRun )
					Assoc rtn

					rtn.ok = true
					rtn.result = false

					String updateStr = "update BESSEMER_SCHEDULE set NextRun=:A1, RunCount=RunCount+1 where TaskID=:A2 and RunCount=:A3"

					if prgCtx.fDbConnect.StartTrans()	

						Assoc status = .Query( prgCtx, updateStr, { nextRun, taskID, runCount } ) 

						if !status.ok
							rtn = status
						elseif status.result == 1
							rtn.result = true
						end

						if rtn.result 

							// We have effectively reserved the task -- now lock it...

							Assoc lockResult = .Query( prgCtx, "insert into BESSEMER_LOCK values (:A1,:A2)", { taskID, System.ThreadIndex() } ) 

							if !lockResult.ok
								rtn.result = false
							end
						end

						prgCtx.fDBConnect.EndTrans( rtn.ok && rtn.result )
					end

					return rtn	
				End


	public function Assoc RunTask( Object prgCtx, Object agentTask, Assoc taskInfo )

					Assoc rtn

					Dynamic params

					if IsDefined( taskInfo.Params )
						params = Str.StringToValue( taskInfo.Params )
					end

					Echo("")
					Echo( "Running task: ", taskInfo.TaskType, " with params: ", params )	

					// determine what the stopTime is
					Date expireDate

					if IsDefined(taskInfo.Duration)
						expireDate = Date.Now() + (taskInfo.Duration/60)	
					end

					// Run the task ...
					rtn = agentTask.Execute( prgCtx, params, expireDate )

					if rtn.ok
						Assoc lastRunStatus = ._SetLastRunDate( prgCtx, taskInfo.TaskID, prgCtx.fDbConnect.Now() )

						if !lastRunStatus.ok
							// TODO: Log error
							EchoError( [BESSEMERAGENTS_ERRMSG.CouldNotUpdateLastRunDate], lastRunStatus )
						end
					end

					return rtn
				End


	public function Assoc RunTasks( Object prgCtx = $BessemerAgents.Utils.GetAdminCtx().prgCtx )

					Assoc rtn

					Assoc lockStatus = .ClearLocks(prgCtx)

					if !lockStatus.ok
						EchoError( [BESSEMERAGENTS_ERRMSG.CouldNotClearWorkerLocks], lockStatus )
					end

					Assoc tasks = ._GetScheduledTasks( prgCtx )

					if !tasks.ok
						return tasks
					end

					RecArray taskRecs = tasks.result
					Record task

					if Length(taskRecs)	> 0
						for task in taskRecs
							Object agentTask = $BessemerAgents.AgentTaskRegistry.Get( task.TaskType )

							if agentTask
								//echo('Time to run ', task.TaskType)

								//  Important -- get a new temporary object for this task
								agentTask = agentTask.New()

								Date runDate = prgCtx.fDBConnect.Now()
								Date nextRunDate = ._CalcNextRun( runDate, task.CronString )

								Assoc reserve = .ReserveTask( prgCtx, task.TaskID, task.RunCount, nextRunDate )

								if reserve.ok
									if reserve.result

										$BessemerAgents.Timer.Start( "runtask" )

										Assoc runResult = .RunTask( prgCtx, agentTask, Assoc.FromRecord( task ) )

										$BessemerAgents.Timer.StopPrint( Str.Format( "%1 execute time: ", task.TaskType ), "runtask" )

										if !runResult.ok
											EchoError( [BESSEMERAGENTS_ERRMSG.ErrorWhileRunningTask], runResult )
										end

										.RemoveLock( prgCtx, task.TaskID )
									end
								else
									// TODO: Log error...
									EchoError( [BESSEMERAGENTS_ERRMSG.CouldNotReserveTaskDueToError], reserve )
								end
							else
								// TODO: Can't find type in registry...
								EchoError( Str.Format( "Cannot find task type '%1' in registry.", task.TaskType ) )
							end
						end
					else
						echo('No tasks to run at this time from the BESSEMER_SCHEDULE table')
					end		

					rtn.ok = true
					return rtn	
				End


	public function SendTestEmail( Object prgCtx = $BessemerAgents.Utils.GetAdminCtx().prgCtx, String recipient = "kmiller@syntergy.com", Record violation = undefined )
					Object task = $BessemerAgents.AgentTaskRegistry.Get( "SendEmails" )

					if task
						task.SendTestEmail( prgCtx, recipient, violation )	
					end

				End


	private function Date _CalcNextRun( Date currentDate, String cronString )

					Date nextDate = Date.NextCronTime(currentDate, cronString)

					return nextDate 

				End

	/*
				* Gets a preference from the configuration file
				*
				* @param {FilePrefs} prefs
				* @param {String} section
				* @param {String} prefName
				* @param {String} defReturn
				*
				* @return {String}
				*
				* @private
				*/
	private function String _GetMyPref( \
					FilePrefs	prefs, \
					String		section, \
					String		prefName, \
					String		defReturn )

					String		retVal = defReturn


					if ( IsDefined( prefs ) && IsNotError( prefs ) )

						retVal = FilePrefs.GetPref( prefs, section, prefName )

						if ( IsError( retVal ) || IsUndefined( retVal ) )

							retVal = FilePrefs.GetPref( prefs, 'general', prefName )

							if ( IsError( retVal ) || IsUndefined( retVal ) )

								retVal = defReturn

							end

						end

					end

					return retVal
				end

	/*
				* Gets a preference from the configuration file
				*
				* @param {FilePrefs} prefs
				* @param {String} section
				* @param {String} prefName
				* @param {String} defReturn
				*
				* @return {String}
				*
				* @private
				*/
	private function String _GetMyPrefPath(\
					FilePrefs 	prefs,\
					String 		section,\
					String 		prefName,\
					String 		defReturn )

					String		retVal = defReturn


					if ( IsDefined( prefs ) && IsNotError( prefs ) )	

						retVal = FilePrefs.GetPath( prefs, section, prefName )

						if ( IsError( retVal ) || IsUndefined( retVal ) )

							retVal = FilePrefs.GetPath( prefs, 'general', prefName )

							if ( IsError( retVal ) || IsUndefined( retVal ) )

								retVal = defReturn

							end

						end

					end

					return retVal
				end


	private function Assoc _GetScheduledTasks( Object prgCtx = $BessemerAgents.Utils.GetAdminCtx().prgCtx )

					Assoc rtn, status

					rtn.ok = true

					String statement = "select TaskID, TaskType, CronString, Duration_Mins, NextRun, RunCount, Params from BESSEMER_SCHEDULE where Thread_ID=:A1 and Enabled=:A2 and NextRun <= :A3 order by NextRun, Priority"

					status = .Query( prgCtx, statement, { .fThreadID, "Y", prgCtx.fDBConnect.Now() } )

					if status.ok
						rtn.result = status.result		
					else
						rtn = $_CSErr.Wrap( status, [BESSEMERAGENTS_ERRMSG.CouldNotQueryScheduledTasks] )
					end

					return rtn

				End


	private function Assoc _SetLastRunDate( Object prgCtx, Integer taskID, Date lastRunDate )

					Assoc rtn
					rtn.ok = true

					String updateStr = "update BESSEMER_SCHEDULE set LastRun=:A1 where TaskID=:A2"

					if prgCtx.fDBConnect.StartTrans()

						Assoc status = .Query( prgCtx, updateStr, { lastRunDate, taskID } )

						if status.ok
							rtn.result = status.result
						else
							rtn = $_CSErr.Wrap( status, [BESSEMERAGENTS_ERRMSG.CouldNotUpdateTheLastRunDate] )
						end

						prgCtx.fDbConnect.EndTrans( rtn.ok )
					else
						rtn = $_CSErr.New( [BESSEMERAGENTS_ERRMSG.CouldNotStartTransaction] )
					end

					return rtn
				End


	private function Assoc _SetNextRunDate( Object prgCtx, Integer taskID, Date nextRunDate )

					Assoc rtn
					rtn.ok = true

					String updateStr = "update BESSEMER_SCHEDULE set NextRun=:A1 where TaskID=:A2"

					if prgCtx.fDBConnect.StartTrans()

						Assoc status = .Query( prgCtx, updateStr, { nextRunDate, taskID } )

						if status.ok
							rtn.result = status.result
						else
							rtn = $_CSErr.Wrap( status, [BESSEMERAGENTS_ERRMSG.CouldNotUpdateTheNextRunDate] )
						end

						prgCtx.fDbConnect.EndTrans( rtn.ok )
					else
						rtn = $_CSErr.New( [BESSEMERAGENTS_ERRMSG.CouldNotStartTransaction] )
					end

					return rtn
				End


	private function Assoc _SetRunDates( Object prgCtx, Integer taskID, Date nextRunDate, Date lastRunDate = undefined )

					Assoc rtn
					rtn.ok = true

					String updateStr = "update BESSEMER_SCHEDULE set NextRun=:A1, LastRun=:A2 where TaskID=:A3"

					if prgCtx.fDBConnect.StartTrans()

						Assoc status = .Query( prgCtx, updateStr, { nextRunDate, lastRunDate, taskID } )

						if status.ok
							rtn.result = status.result
						else
							rtn = $_CSErr.Wrap( status, [BESSEMERAGENTS_ERRMSG.CouldNotUpdateTheNextRunDate] )
						end

						prgCtx.fDbConnect.EndTrans( rtn.ok )
					else
						rtn = $_CSErr.New( [BESSEMERAGENTS_ERRMSG.CouldNotStartTransaction] )
					end

					return rtn
				End


	private function _TestCalcNextRun()

					Date now = Date.Now()

					echo(._CalcNextRun(now, "*/5 * * * *"))


				end

end
