package BESSEMERAGENTS

public Object Globals inherits KERNEL::Globals

	public		Object	AgentTaskRegistry = BESSEMERAGENTS::AgentTaskRegistry
	public		Object	Cache = BESSEMERAGENTS::Cache
	public		Object	Err = BESSEMERAGENTS::UTILITY::Err
	public		Object	MigrationThread = BESSEMERAGENTS::THREAD::MigrationThread
	public		Object	OperationRegistry = BESSEMERAGENTS::OperationRegistry
	public		Object	Prefs = BESSEMERAGENTS::Prefs
	public		Object	SyncProcessThread = BESSEMERAGENTS::THREAD::SyncProcessThread
	public		Object	Timer = BESSEMERAGENTS::UTILITY::Timer
	public		Object	Utils = BESSEMERAGENTS::UTILITY::Utils
	public		Object	WebModule = BESSEMERAGENTS::WebModule
	public		Dynamic	fAdminCtx

end
