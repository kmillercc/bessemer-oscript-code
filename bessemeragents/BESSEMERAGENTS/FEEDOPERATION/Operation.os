package BESSEMERAGENTS::FEEDOPERATION

public Object Operation inherits BESSEMER::Root

	public		Dynamic	fCode
	public		Boolean	requiredAccount = FALSE
	public		Boolean	requiredContact = FALSE
	public		Boolean	requiredFamily = FALSE
	public		Boolean	requiredNewAccount = FALSE
	public		Boolean	requiredNewContact = FALSE
	public		Boolean	requiredNewFamily = FALSE
	public		Boolean	requiredNewRelationship = FALSE
	public		Boolean	requiredNewSubaccount = FALSE
	public		Boolean	requiredRelationship = FALSE
	public		Boolean	requiredSubaccount = FALSE



	public function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				return .OK()

				// or

				//return .Problem ("Houston")

			end


	public function Assoc OK()

				Assoc ok

				ok.ok = true

				return OK

			end


	private function Integer _GetFacet ( Object prgCtx, String name )

				Integer facet = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', name + 'Facet')

				if not facet
					echo (Str.Format ("Warning: %1 facet not configured", name))
				end

				return facet

			end


	private function Assoc _GetHierarchyNode ( Object prgCtx, String ancestors = Undefined )

		Integer nodeID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'hierarchyRoot')
		if not nodeID
			return .Problem ("No configuration for hierarchy")
		end	

		DAPINode node = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, nodeID)
		if IsError(node)
			return .Problem ("Invalid hierarchy root configuration")
		end

		if IsNotError(node) && IsDefined(ancestors)

			List pathElements = Str.Elements(ancestors, ":")
			String name

			for name in pathElements
				Assoc status = ._GetNode(prgCtx, name, node)
				if !status.ok
					return status
			 	end
			 	node = status.node
			end
		end

		Assoc ok
		ok.node = node
		ok.ok = true

		return ok

	end


	private function DAPINode _GetMyMyMy (Object prgCtx, String name)

		Integer nodeID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'my' + name)
		if not nodeID
			return Undefined
		end	

		return DAPI.GetNodeByID (prgCtx.DAPISess(), 0, nodeID)

	end

	private function Assoc _GetNode(Object prgCtx, String name, DAPINode parentNode)

		Assoc status = $Bessemer.Utils.DAPIGetNode(prgCtx, parentNode, name)

		if !status.ok
			status = ._GetNodeByCode(prgCtx, name, parentNode)
			if !status.ok
				return status
			end	
		end

		return Assoc{'ok': true, 'node': status.result}

	end		


	private function Assoc _GetNodeOld(Object prgCtx, String name, DAPINode parentNode)

		Assoc status = $Bessemer.Utils.DAPIGetNode(prgCtx, parentNode, name)

		if !status.ok
			return .Problem ( \
						Str.Format ('No item named "%1" at: "%2" (%3)', \
								name, \
								$LLIAPI.LLNodeSubsystem.GetItem (parentNode.pSubtype).NodePath (parentNode).path, \
								parentNode.pID \
							) \
					)
		end

		return Assoc{'ok': true, 'node': status.result}

	end
	
	override function Void Scratch()
		
		Object prgCtx = $PrgSessions[1]
		
		String stmt = "select DataID, OwnerID, Name from DTreeCore where Name like '%[' || :A1 || ']' and ParentID=:A2" 
		Dynamic recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, stmt, 'ENT1', 2000)
		
		echo(recs)
		
	end

	private function Assoc _GetNodeByCode (Object prgCtx, String name, DAPINode parentNode)

		String codeInBrackets = ._IDFromName(name)
		if IsUndefined(codeInBrackets)
			return .Problem("Can not find code in brackets for name '" + name + "' Invalid call to _GetNodeByCode?")
		end	

		String stmt = "select DataID, OwnerID, Name from DTreeCore where Name like '%[' || :A1 || ']' and ParentID=:A2"
		Dynamic recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, stmt, codeInBrackets, parentNode.pID)

		if IsError(recs) || Length(recs) == 0
			return .Problem ( \
						Str.Format ('No item with code "%1" at: "%2" (%3)', \
								codeInBrackets, \
								$LLIAPI.LLNodeSubsystem.GetItem (parentNode.pSubtype).NodePath (parentNode).path, \
								parentNode.pID \
							) \
					)
		elseif Length(recs) > 1
			return .Problem ( \
						Str.Format ('More than one item with code "%1" at: "%2" (%3)', \
								codeInBrackets, \ 
								$LLIAPI.LLNodeSubsystem.GetItem (parentNode.pSubtype).NodePath (parentNode).path, \
								parentNode.pID \
							) \
					)
		end

		Integer dataID = recs[1].DataID
		Integer ownerID  = recs[1].OwnerID

		DAPINODE node = DAPI.GetNodeByID(prgCtx.DapiSess(), ownerID, dataID)
   		if IsError(node)
   			return .Problem(Str.Format("Found the node in folder %1 with code %2 and dataID %3 but could not get the DAPINode. Permissions?", parentNode.pID, codeInBrackets, dataID))
   	 	end

		return Assoc{'ok': true, 'result': node}

	end		


	private function String _IDFromname (String name)

		Integer left = Str.RChr (name, "[")
		Integer right = Str.RChr (name, "]")

		if left and right and right - left > 1
			return name[left + 1 : right - 1]
		end

		return Undefined

	end

	private function Assoc _NodeRename(DAPINODE node, String newName)

		if node.pName != newName

			Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)

			Assoc status = llNode.NodeRename(node, newName)
			if !status.ok
            	return .Problem(Str.Format("Error renaming node: %1", status.errMsg))
			end	

		end	

		return Assoc{'ok': true}
 	end


	private function Assoc _UpdateAttribute ( \
					Object prgCtx, \
					String attribute, \
					String old, \
					String new \
				)

				Assoc find
				find.(attribute) = old

				Assoc change
				change.(attribute) = new

				return ._UpdateSet (prgCtx, find, change)

			end


	private function Assoc _UpdateSet ( Object prgCtx, Assoc find, Assoc change )
		
		// check all docs that have and" name/value set;
		// optimize to the most specific

		String search

		if find.Subaccount
			search = find.Subaccount
		elseif find.Acount
			search = find.Account
		elseif find.Relationship
			search = find.Relationship
		else  // whatever
			search = find[1]
		end
		
		// KM 8/17/2021 search using like to get documents that match by code
		String codeInBrackets = ._IDFromName (search)
		if IsUnDefined(codeInBrackets)
			return .Problem ("Error parsing attribute search string. Cannot find code in brackets")
		end	
	
		Record doc
		for doc in CAPI.Exec (prgCtx.fDBConnect.fConnection, \
				"select distinct ID, VerNum, ValStr from LLAttrData" \
				+ " join DTree on ID = DataID and VerNum = VersionNum" \
				+ " where ValStr like '%[' || :A1 || ']'", \
				codeInBrackets)
				
			Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, doc.ID, doc.VerNum)
			if not attrData.Load()
				return .Problem ("Error loading attributes during set update")
			end
			
			if find.Subaccount
				find.Subaccount = doc.ValStr
			elseif find.Acount
				find.Account = doc.ValStr
			elseif find.Relationship
				find.Relationship = doc.ValStr
			elseif find.Family
				find.Family = doc.ValStr
			end

			if attrData.ChangeSet (find, change)
				if not attrData.Save()
					return .Problem ("Error saving attributes during set update")
				end
			end
		end
		

		return .OK()	
	end

	Script __Init



						if IsDefined(.fCode)

							$BESSEMERAGENTS.OperationRegistry.Register(this)

						end





	endscript

end
