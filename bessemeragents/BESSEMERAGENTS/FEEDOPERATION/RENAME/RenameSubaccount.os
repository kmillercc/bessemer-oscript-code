package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameSubaccount inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation

	override	String	fCode = 'R'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewAccount = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredNewSubaccount = TRUE
	override	Boolean	requiredRelationship = TRUE
	override	Boolean	requiredSubaccount = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result

		String ID = ._IDFromName (subaccount)
		if IsUndefined(ID)
			return .Problem (Str.Format ("Invalid subaccount name: %1", subaccount))
		end

		if ._IDFromName (newSubaccount) != ID
			return .Problem (Str.Format ("New subaccount name must not change ID: %1", newSubaccount))
		end

		if family != newFamily or relationship != newRelationship or account != newAccount
			return .Problem ("Renaming must not also move")
		end

		Assoc subaccountFacets
		subaccountFacets.(._GetFacet (prgCtx, 'subaccount')) = newsubAccount


		// hierarchy

		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship + ":" + account + ":" + subaccount)
		if not result.OK; return result; end
		DAPINode subaccountNode = result.node

		result = ._UpdateVFFacets(prgCtx, subaccountNode, subaccountFacets)
		if not result.OK; return result; end

		Assoc renamed = ._NodeRename(subaccountNode, newsubAccount)
		if not renamed.OK
			return renamed
		end


		// mymymy

		DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
		if IsError(mySubaccounts)
			return .Problem ("No My Subaccounts")
		end

		result = ._GetNode (prgCtx, subaccount, mySubaccounts)
		if not result.OK; return result; end
		subaccountNode = result.node

		result = ._UpdateVFFacets(prgCtx, subaccountNode, subaccountFacets)
		if not result.OK; return result; end

		renamed = ._NodeRename(subaccountNode, newsubAccount)
		if not renamed.OK
			return renamed
		end

		// associations

		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set SubAccount = :A1 where SubAccount = :A2", \
				newSubaccount, subaccount \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end


		// metadata
		// *** KM 5/26/2017 Changed Misspelling to Sub-Account because that's what matches the actual attribute name
		result = ._UpdateAttribute (prgCtx, "Sub-Account", subaccount, newSubaccount)
		if not result.OK; return result; end

		return .OK()
	end

end
