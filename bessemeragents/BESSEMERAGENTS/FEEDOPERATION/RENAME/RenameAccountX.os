package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameAccountX inherits BESSEMERAGENTS::FEEDOPERATION::RENAME::RenameAccount

	override	Boolean	requiredNewFamily = FALSE
	override	Boolean	requiredNewRelationship = FALSE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

							newFamily = family
							newRelationship = relationship

							return .OSParent.Execute ( \
									prgCtx, \
									family, \
									relationship, \
									account, \
									subaccount, \
									contact, \
									newFamily, \
									newRelationship, \
									newAccount, \
									newSubaccount, \
									newContact \
								)
						end

end
