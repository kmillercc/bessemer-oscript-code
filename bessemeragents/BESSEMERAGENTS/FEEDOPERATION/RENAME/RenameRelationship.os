package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameRelationship inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation

	override	String	fCode = 'R'
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredRelationship = TRUE



	override function Assoc Execute ( Object prgCtx, \
									String family, \
									String relationship, \
									String account, \
									String subaccount, \
									String contact, \
									String newFamily, \
									String newRelationship, \
									String newAccount, \
									String newSubaccount, \
									String newContact )

		Assoc result

		String ID = ._IDFromName (relationship)
		if IsUndefined(ID)
			return .Problem (Str.Format ("Invalid relationship name: %1", relationship))
		end

		if ._IDFromName (newRelationship) != ID
			return .Problem (Str.Format ("New relationship name must not change ID: %1", newRelationship))
		end

		if family != newFamily
			return .Problem ("Renaming must not also move")
		end

		Assoc relationshipFacets
		relationshipFacets.(._GetFacet (prgCtx, 'relationship')) = newRelationship

		// hierarchy

		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship)
		if not result.OK; return result; end
		DAPINode relationshipNode = result.node

		result = ._UpdateVFFacets(prgCtx, relationshipNode, relationshipFacets)
		if not result.OK; return result; end

		Assoc renamed = ._NodeRename(relationshipNode, newRelationship)
		if not renamed.OK
			return renamed
		end


		// account, subaccount children

		DAPINode accountNode
		for accountNode in DAPI.ListSubNodes (relationshipNode)

	//		Assoc accountFacets = Assoc.Copy (relationshipFacets)
	//		accountFacets.(._GetFacet (prgCtx, 'account')) = accountNode.pName

	//		result = ._UpdateVF (prgCtx, accountNode, accountFacets)
	//		if not result.OK; return result; end

			DAPINode subaccountNode
			for subaccountNode in DAPI.ListSubNodes (accountNode)

				// mysubaccount UP to relationship

				DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
				if IsError(mySubaccounts)
					return .Problem ("No My subaccounts")
				end

				result = ._GetNode (prgCtx, subaccountNode.pName, mySubaccounts)
				if not result.OK; return result; end
				subaccountNode = result.node

				result = ._GetNode (prgCtx, accountNode.pName, subaccountNode)
				if not result.OK; return result; end
				accountNode = result.node

				result = ._GetNode (prgCtx, relationship, accountNode)
				if not result.OK; return result; end
				relationshipNode = result.node

				result = ._UpdateVFFacets(prgCtx, relationshipNode, relationshipFacets)
				if not result.OK; return result; end

				renamed = ._NodeRename(relationshipNode, newRelationship)
				if not renamed.OK
					return renamed
				end

			end


			// myaccount UP to relationship

			DAPINode myAccounts = ._GetMyMyMy (prgCtx, 'Accounts')
			if IsError(myAccounts)
				return .Problem ("No My accounts")
			end

			result = ._GetNode (prgCtx, accountNode.pName, myAccounts)
			if not result.OK; return result; end
			accountNode = result.node

			result = ._GetNode (prgCtx, relationship, accountNode)
			if not result.OK; return result; end
			relationshipNode = result.node

			result = ._UpdateVFFacets(prgCtx, relationshipNode, relationshipFacets)
			if not result.OK; return result; end

			renamed = ._NodeRename(relationshipNode, newRelationship)
			if not renamed.OK
				return renamed
			end

		end


		// mymymy

		DAPINode myRelationships = ._GetMyMyMy (prgCtx, 'Relationships')
		if IsError(myRelationships)
			return .Problem ("No My Relationships")
		end

		result = ._GetNode (prgCtx, relationship, myRelationships)
		if not result.OK; return result; end
		relationshipNode = result.node

		result = ._UpdateVFFacets(prgCtx, relationshipNode, relationshipFacets)
		if not result.OK; return result; end

		renamed = ._NodeRename(relationshipNode, newRelationship)
		if not renamed.OK
			return renamed
		end


		// associations

		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set Relationship = :A1 where Relationship = :A2", \
				newRelationship, relationship \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end


		// metadata

		result = ._UpdateAttribute (prgCtx, "Relationship", relationship, newRelationship)
		if not result.OK; return result; end

		return .OK()
	end

end
