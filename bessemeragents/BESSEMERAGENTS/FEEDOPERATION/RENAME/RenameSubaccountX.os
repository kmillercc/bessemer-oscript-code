package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameSubaccountX inherits BESSEMERAGENTS::FEEDOPERATION::RENAME::RenameSubaccount

	override	Boolean	requiredNewAccount = FALSE
	override	Boolean	requiredNewFamily = FALSE
	override	Boolean	requiredNewRelationship = FALSE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

							newFamily = family
							newRelationship = relationship
							newAccount = account

							return .OSParent.Execute ( \
									prgCtx, \
									family, \
									relationship, \
									account, \
									subaccount, \
									contact, \
									newFamily, \
									newRelationship, \
									newAccount, \
									newSubaccount, \
									newContact \
								)
						end

end
