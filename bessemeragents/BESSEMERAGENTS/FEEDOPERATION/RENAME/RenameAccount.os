package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameAccount inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation

	override	String	fCode = 'R'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewAccount = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredRelationship = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result

		String ID = ._IDFromName (account)
		if IsUndefined(ID)
			return .Problem (Str.Format ("Invalid account name: %1", account))
		end

		if ._IDFromName (newAccount) != ID
			return .Problem (Str.Format ("New account name must not change ID: %1", newAccount))
		end

		if family != newFamily or relationship != newRelationship
			return .Problem ("Renaming must not also move")
		end

		Assoc accountFacets
		accountFacets.(._GetFacet (prgCtx, 'account')) = newAccount


		// hierarchy
		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship + ":" + account)
		if not result.OK; return result; end
		DAPINode accountNode = result.node

		result = ._UpdateVFFacets(prgCtx, accountNode, accountFacets)
		if not result.OK; return result; end

		Assoc renamed = ._NodeRename(accountNode, newAccount)
		if not renamed.OK
			return renamed
		end

		DAPINode subaccountNode
		for subaccountNode in DAPI.ListSubNodes (accountNode)

			// mysubaccount UP to account

			DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
			if IsError(mySubaccounts)
				return .Problem ("No My subaccounts")
			end

			result = ._GetNode (prgCtx, subaccountNode.pName, mySubaccounts)
			if not result.OK; return result; end
			subaccountNode = result.node

			result = ._GetNode (prgCtx, account, subaccountNode)
			if not result.OK; return result; end
			accountNode = result.node

			result = ._UpdateVFFacets(prgCtx, accountNode, accountFacets)
			if not result.OK; return result; end

			renamed = ._NodeRename(accountNode, newAccount)
			if not renamed.OK
				return renamed
			end

		end


		// mymymy

		DAPINode myAccounts = ._GetMyMyMy (prgCtx, 'Accounts')
		if IsError(myAccounts)
			return .Problem ("No My accounts")
		end

		result = ._GetNode (prgCtx, account, myAccounts)
		if not result.OK; return result; end
		accountNode = result.node

		result = ._UpdateVFFacets(prgCtx, accountNode, accountFacets)
		if not result.OK; return result; end

		renamed = ._NodeRename(accountNode, newAccount)
		if not renamed.OK
			return renamed
		end


		// associations

		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set Account = :A1 where Account = :A2", \
				newAccount, account \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end


		// metadata

		result = ._UpdateAttribute (prgCtx, "Account", account, newAccount)
		if not result.OK; return result; end

		return .OK()
	end

end
