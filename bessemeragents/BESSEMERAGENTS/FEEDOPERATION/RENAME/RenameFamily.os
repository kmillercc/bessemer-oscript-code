package BESSEMERAGENTS::FEEDOPERATION::RENAME

public Object RenameFamily inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation

	override	String	fCode = 'R'
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewFamily = TRUE



	override function Assoc Execute ( Object prgCtx, \
									String family, \
									String relationship, \
									String account, \
									String subaccount, \
									String contact, \
									String newFamily, \
									String newRelationship, \
									String newAccount, \
									String newSubaccount, \
									String newContact)

		Assoc result

		String ID = ._IDFromName (family)
		if IsUndefined(ID)
			return .Problem (Str.Format ("Invalid family name: %1", family))
		end

		if ._IDFromName (newFamily) != ID
			return .Problem (Str.Format ("New family name must not change ID: %1", newFamily))
		end

		Assoc familyFacets
		familyFacets.(._GetFacet (prgCtx, 'family')) = newFamily


		// hierarchy

		result = ._GetHierarchyNode (prgCtx, family)
		if not result.OK; return result; end
		DAPINode familyNode = result.node

		result = ._UpdateVFFacets(prgCtx, familyNode, familyFacets)
		if not result.OK; return result; end

		Assoc renamed = ._NodeRename(familyNode, newFamily)
		if not renamed.OK
			return renamed
		end


		// relationship, account, subaccount children

		DAPINode relationshipNode
		for relationshipNode in DAPI.ListSubNodes (familyNode)

			DAPINode accountNode
			for accountNode in DAPI.ListSubNodes (relationshipNode)

				DAPINode subaccountNode
				for subaccountNode in DAPI.ListSubNodes (accountNode)

					// mysubaccount UP to family

					DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
					if IsError(mySubaccounts)
						return .Problem ("No My subaccounts")
					end

					result = ._GetNode (prgCtx, subaccountNode.pName, mySubaccounts)
					if not result.OK; return result; end
					subaccountNode = result.node


					result = ._GetNode (prgCtx, accountNode.pName, subaccountNode)
					if not result.OK; return result; end				
					accountNode = result.node


					result = ._GetNode (prgCtx, relationshipNode.pName, accountNode)
					if not result.OK; return result; end				
					relationshipNode = result.node


					result = ._GetNode (prgCtx, family, relationshipNode)
					if not result.OK; return result; end
					familyNode = result.node


					result = ._UpdateVFFacets(prgCtx, familyNode, familyFacets)
					if not result.OK; return result; end

					renamed = ._NodeRename(familyNode, newFamily)
					if not renamed.OK
						return renamed
					end

				end


				// myaccount UP to family

				DAPINode myAccounts = ._GetMyMyMy (prgCtx, 'Accounts')
				if IsError(myAccounts)
					return .Problem ("No My accounts")
				end

				result = ._GetNode (prgCtx, accountNode.pName, myAccounts)
				if not result.OK; return result; end
				accountNode = result.node

				result = ._GetNode (prgCtx, relationshipNode.pName, accountNode)
				if not result.OK; return result; end
				relationshipNode = result.node


				result = ._GetNode (prgCtx, family, relationshipNode)
				if not result.OK; return result; end
				familyNode = result.node

				result = ._UpdateVFFacets(prgCtx, familyNode, familyFacets)
				if not result.OK; return result; end

				renamed = ._NodeRename(familyNode, newFamily)
				if not renamed.OK
					return renamed
				end

			end


			// myrelationship UP to family

			DAPINode myRelationships = ._GetMyMyMy (prgCtx, 'Relationships')
			if IsError(myRelationships)
				return .Problem ("No My relationships")
			end

			result = ._GetNode (prgCtx, relationshipNode.pName, myRelationships)
			if not result.OK; return result; end
			relationshipNode = result.node

			result = ._GetNode (prgCtx, family, relationshipNode)
			if not result.OK; return result; end
			familyNode = result.node

			result = ._UpdateVFFacets(prgCtx, familyNode, familyFacets)
			if not result.OK; return result; end

			renamed = ._NodeRename(familyNode, newFamily)
			if not renamed.OK
				return renamed
			end

		end


		// mymymy

		DAPINode myFamilies = ._GetMyMyMy (prgCtx, 'Families')
		if IsError(myFamilies)
			return .Problem ("No My Families")
		end

		result = ._GetNode (prgCtx, family, myFamilies)
		if not result.OK; return result; end
		familyNode = result.node

		result = ._UpdateVFFacets(prgCtx, familyNode, familyFacets)
		if not result.OK; return result; end

		renamed = ._NodeRename(familyNode, newFamily)
		if not renamed.OK
			return renamed
		end


		// associations

		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set Family = :A1 where Family = :A2", \
				newFamily, family \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end

		// metadata
		result = ._UpdateAttribute (prgCtx, "Family", family, newFamily)
		if not result.OK; return result; end

		return .OK()
	end

end
