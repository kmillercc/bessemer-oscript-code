package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object DisassociateFromAccount inherits BESSEMERAGENTS::FEEDOPERATION::CONTACT::ContactOperation

	override	String	fCode = 'D'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredContact = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredRelationship = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Integer deleted = CAPI.Exec ( \
						prgCtx.fDBConnect.fConnection, \
						"delete from BESSEMER_FRAS_Contacts where Family=:A1 and Relationship=:A2 and Account=:A3 and Contact=:A4", \
						family, relationship, account, contact \
					)

				if not prgCtx.fDBConnect.CheckError (deleted).OK
					return .Problem ("Error disassociating contact")
				end

				// Remove the contact perms
				Assoc result = .RemoveContactPerms(prgCtx, contact, family, relationship, account)
				if !result.ok
					return result
				end	

				return .OK()
			end

end
