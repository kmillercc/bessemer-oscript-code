package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object AssociateWithFamily inherits BESSEMERAGENTS::FEEDOPERATION::CONTACT::ContactOperation

	override	String	fCode = 'A'
	override	Boolean	requiredContact = TRUE
	override	Boolean	requiredNewFamily = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Integer inserted = CAPI.Exec ( \
						prgCtx.fDBConnect.fConnection, \
						"insert into BESSEMER_FRAS_Contacts (Family, Contact) VALUES (:A1, :A2)", \
						newFamily, contact \
					)

				if not prgCtx.fDBConnect.CheckError (inserted).OK
					return .Problem ("Error associating contact")
				end

				return .OK()
			end

end
