package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object AssociateWithAccount inherits BESSEMERAGENTS::FEEDOPERATION::CONTACT::ContactOperation

	override	String	fCode = 'A'
	override	Boolean	requiredContact = TRUE
	override	Boolean	requiredNewAccount = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Integer inserted = CAPI.Exec ( \
						prgCtx.fDBConnect.fConnection, \
						"insert into BESSEMER_FRAS_Contacts (Family, Relationship, Account, Contact) VALUES (:A1, :A2, :A3, :A4)", \
						newFamily, newRelationship, newAccount, contact \
					)

				if not prgCtx.fDBConnect.CheckError (inserted).OK
					return .Problem ("Error associating contact")
				end

				// Add the contact perms
				Assoc result = .AddContactPerms(prgCtx, contact, newRelationship)
				if !result.ok
					return result
				end		

				return .OK()
			end

end
