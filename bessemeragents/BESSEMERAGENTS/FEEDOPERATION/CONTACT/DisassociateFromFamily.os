package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object DisassociateFromFamily inherits BESSEMERAGENTS::FEEDOPERATION::CONTACT::ContactOperation

	override	String	fCode = 'D'
	override	Boolean	requiredContact = TRUE
	override	Boolean	requiredFamily = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

							Integer deleted = CAPI.Exec ( \
									prgCtx.fDBConnect.fConnection, \
									"delete from BESSEMER_FRAS_Contacts where Family=:A1 and Contact=:A2", \
									family, contact \
								)

							if not prgCtx.fDBConnect.CheckError (deleted).OK
								return .Problem ("Error disassociating contact")
							end

							return .OK()
						end

end
