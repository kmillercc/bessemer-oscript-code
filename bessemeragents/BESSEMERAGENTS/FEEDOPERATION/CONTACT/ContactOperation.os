package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object ContactOperation inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation


	public function Assoc AddContactPerms(Object prgCtx, String contactName, String relationshipName)

				Assoc result

				// now get the contact and add the relationship group to it
				DAPINODE myContactNode
				DAPINode myContacts = ._GetMyMyMy (prgCtx, 'Contacts')
				if IsError(myContacts)
					return .problem ("No My Contacts")
				else
					result = ._GetNode(prgCtx, contactName, myContacts)
					if !result.OK
						return result
					else
						myContactNode = result.node		
					end
				end

				// add the relationship to the contact node
				result = $Bessemer.ContactPermissions.AddRelationshipToNode(prgCtx, myContactNode, relationshipName) 
				if not result.OK
					return result
				end		

				// update all documents that have this contact -- add the rs group
				result = $Bessemer.DocPermissions.AddRelationshipToDocsWithContact(prgCtx, contactName, relationshipName) 
				if not result.OK
					return result
				end		


				return .Success()
			end

	public function Assoc RemoveContactPerms(Object prgCtx, String contactName, String familyName, String relationshipName, String accountName = undefined)

		// KM 2-9-21 make sure relationship is not needed for this contact by another element in the hierarchy
		if !._IsNeededForAntherElement(prgCtx, contactName, familyName, relationshipName)

			// now get the contact and add the relationship group to it
			DAPINODE myContactNode
			DAPINode myContacts = ._GetMyMyMy (prgCtx, 'Contacts')
			if IsError(myContacts)
				return .problem ("No My Contacts")
			else
				Assoc result = ._GetNode (prgCtx, contactName, myContacts)
				if !result.OK
					return result
				end

				myContactNode = result.node

			end

			// remove the relationship from the contact node
			Assoc nodeResult = $Bessemer.ContactPermissions.RemoveRelationshipFromNode(prgCtx, myContactNode, relationshipName) 
			if !nodeResult.OK
				return nodeResult
			end	
		end			

		// update all documents that have this contact -- remove the rs group
		Assoc docResult = $Bessemer.DocPermissions.RemoveRelationshipFromDocsWithContact(prgCtx, contactName, relationshipName) 
		if !docResult.OK
			return docResult
		end			

		return .Success()
	end

	private function _IsNeededForAntherElement(Object prgCtx, String contactName, String familyName, String relationshipName)

		RecArray recs = CAPI.Exec ( prgCtx.fDBConnect.fConnection,
								"select Count(*) from BESSEMER_FRAS_Contacts where Family=:A1 and Relationship=:A2 and Contact=:A3",
								familyName, relationshipName, contactName)

		if IsNotError(recs) && recs[1].Count > 0

			return true

		end

		return false	

	end		

end
