package BESSEMERAGENTS::FEEDOPERATION::CONTACT

public Object RenameContact inherits BESSEMERAGENTS::FEEDOPERATION::CONTACT::ContactOperation

	override	String	fCode = 'R'
	override	Boolean	requiredContact = TRUE
	override	Boolean	requiredNewContact = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result

		String ID = ._IDFromName (contact)
		if IsUndefined(ID)
			return .Problem (Str.Format ("Invalid contact name: %1", contact))
		end

		if ._IDFromName (newContact) != ID
			return .Problem (Str.Format ("New contact name must not change ID: %1", newContact))
		end

		Assoc facets
		facets.(._GetFacet (prgCtx, 'contact')) = newContact

		// mymymy
		DAPINode myContacts = ._GetMyMyMy (prgCtx, 'Contacts')
		if IsError(myContacts)
			return .Problem ("No My Contacts")
		end

		result = ._GetNode (prgCtx, contact, myContacts)
		if not result.OK; return result; end
		DAPINode contactNode = result.node

		result = ._UpdateVFFacets(prgCtx, contactNode, facets)
		if not result.OK; return result; end

		Assoc renamed = ._NodeRename(contactNode, newContact)
		if not renamed.OK
			return renamed
		end

		// associations
		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set Contact = :A1 where Contact = :A2", \
				newContact, contact \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end

		// metadata
		result = ._UpdateAttribute (prgCtx, "Contact", contact, newContact)
		if not result.OK; return result; end

		return .OK()
	end

end
