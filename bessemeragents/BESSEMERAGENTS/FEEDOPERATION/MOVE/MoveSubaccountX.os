package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveSubaccountX inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::MoveSubaccount

	override	Boolean	requiredNewSubaccount = FALSE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

							newSubaccount = subaccount

							return .OSParent.Execute ( \
									prgCtx, \
									family, \
									relationship, \
									account, \
									subaccount, \
									contact, \
									newFamily, \
									newRelationship, \
									newAccount, \
									newSubaccount, \
									newContact \
								)
						end

end
