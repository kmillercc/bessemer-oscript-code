package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveAccount inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::AbstractMove

	override	String	fCode = 'M'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewAccount = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredRelationship = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result, facets
		DAPINODE node
		RecArray recs
		//Record rec

		if account != newAccount
			return .Problem ("Moving must not also rename")
		end

		// hierarchy
		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship + ":" + account)
		if not result.OK; return result; end
		DAPINode accountNode = result.node

		result = ._GetHierarchyNode (prgCtx, newFamily + ":" + newRelationship)
		if not result.OK; return result; end
		DAPINode destinationNode = result.node

		// move it	
		Assoc moved = $LLIAPI.LLNodeSubsystem.GetItem (accountNode.pSubtype).NodeMove (accountNode, destinationNode)
		if not moved.OK
			return .Problem ("Error moving node")
		end

		// Update account facets and perms
		facets = Assoc.CreateAssoc()
		facets.(._GetFacet (prgCtx, 'account')) = account
		result = .UpdateAccountFacetsPerms(prgCtx, accountNode, facets, newRelationship, relationship)
		if not result.OK; return result; end

		// *** Get any up-folder with the old family name that is inside a folder with the name of the relationship which is inside a folder with name of account and update it to the new family name
		facets = Assoc.CreateAssoc()
		facets.(._GetFacet (prgCtx, 'family')) = newFamily
		String stmt = "SELECT fam.OWNERID,"+\
					  " fam.DATAID,"+\
					  " fam.NAME"+\
					" FROM DTREE rel,"+\
					  " DTREE fam,"+\
					  " DTREE acc"+\
					" WHERE fam.PARENTID=rel.DATAID"+\
					" AND rel.PARENTID=acc.DATAID"+\
					" AND rel.NAME      = :A1" +\
					" AND fam.NAME      = :A2" +\
					" and acc.NAME      = :A3" +\
					" AND fam.SUBTYPE   =899"+\
					" AND rel.subtype   =899"+\
					" AND acc.subtype   =899"

		recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, stmt, relationship, family, account)
		if IsNotError(recs)
			Record rec
			for rec in recs
				node = DAPI.GetNodeByID(prgCtx.DapiSess(), rec.OWNERID, rec.DATAID)

				if IsNotError(node)
					// rename
					result = ._NodeRename(node, newFamily)
					if !result.ok
						return .Problem("Error renaming family up-folder: ", undefined, result.errMsg)
					end		

					// update facet
					result = .UpdateFamilyFacets(prgCtx, node, facets)
					if !result.ok
						return .Problem("Error updating family up-folder", undefined, result.errMsg)
					end	

				else
					return .Problem(Str.Format("Error getting family up-folder with DATAID %1", rec.DATAID))
				end
			end
		else
			return .Problem(Str.Format("Error getting family up-folder recs: %1", Str.String(recs)))	
		end

		// *** Get any up-folder with the old relationship name that is inside a folder with the name of the account and update it to the new relationship name
		facets = Assoc.CreateAssoc()
		facets.(._GetFacet (prgCtx, 'relationship')) = newRelationship
		recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, "select rel.OWNERID, rel.DATAID from DTREE acc, DTREE rel where rel.PARENTID=acc.DATAID and acc.NAME=:A1 and rel.NAME=:A2 and acc.SUBTYPE=899 and rel.subtype=899", account, relationship)
		if IsNotError(recs)
			Record rec
			for rec in recs
				node = DAPI.GetNodeByID(prgCtx.DapiSess(), rec.OWNERID, rec.DATAID)

				if IsNotError(node)
					// rename
					result = ._NodeRename(node, newRelationship)
					if !result.ok
						return .Problem("Error renaming relationship up-folder: ", undefined, result.errMsg)
					end		

					// update facet
					result = .UpdateRelationshipFacetsPerms(prgCtx, node, facets, newRelationship, relationship)
					if !result.ok
						return .Problem("Error updating relationship up-folder", undefined, result.errMsg)
					end	

				else
					return .Problem(Str.Format("Error getting family up-folder node from DATAID %1", Str.String(recs)))
				end

			end
		end


		// metadata

		Assoc find
		find.Family = family
		find.Relationship = relationship
		find.Account = account

		Assoc change
		change.Family = newFamily
		change.Relationship = newRelationship

		result = ._UpdateSet (prgCtx, find, change)
		if not result.OK; return result; end

		return .OK()
	end

end
