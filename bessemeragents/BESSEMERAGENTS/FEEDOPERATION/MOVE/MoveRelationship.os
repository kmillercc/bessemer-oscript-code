package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveRelationship inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::AbstractMove

	override	String	fCode = 'M'
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredRelationship = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result, facets
		DAPINODE node
		RecArray recs

		if relationship != newRelationship
			return .Problem ("Moving must not also rename")
		end

		// hierarchy
		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship)
		if not result.OK; return result; end
		DAPINode relationshipNode = result.node

		result = ._GetHierarchyNode (prgCtx, newFamily)
		if not result.OK; return result; end
		DAPINode destinationNode = result.node

		// Move the relationship node
		Assoc moved = $LLIAPI.LLNodeSubsystem.GetItem (relationshipNode.pSubtype).NodeMove (relationshipNode, destinationNode)
		if not moved.OK
			return .Problem ("Error moving node")
		end

		// mymymy
		DAPINode myRelationships = ._GetMyMyMy (prgCtx, 'Relationships')
		if IsError(myRelationships)
			return .Problem ("No My Relationships")
		end

		result = ._GetNode (prgCtx, relationship, myRelationships)
		if not result.OK
			return result
		end
		relationshipNode = result.node

		// update the relationship		
		result = ._UpdateVFFacets(prgCtx, relationshipNode, facets)
		if not result.OK
			return result
		end

		// *** Get any up-folder with the old family name that is inside a folder with the name of the relationship and update it to the new family name
		facets = Assoc.CreateAssoc()
		facets.(._GetFacet (prgCtx, 'family')) = newFamily
		recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, "select fam.OWNERID, fam.DATAID from DTREE rel, DTREE fam where fam.PARENTID=rel.DATAID and rel.NAME=:A1 and fam.NAME=:A2 and rel.SUBTYPE=899 and fam.subtype=899", relationship, family)
		if IsNotError(recs)
			Record rec
			for rec in recs
				node = DAPI.GetNodeByID(prgCtx.DapiSess(), rec.OWNERID, rec.DATAID)

				if IsNotError(node)
					// rename
					result = ._NodeRename(node, newFamily)
					if !result.ok
						return .Problem("Error renaming family up-folder: ", undefined, result.errMsg)
					end		

					// update facet
					result = .UpdateFamilyFacets(prgCtx, node, facets)
					if !result.ok
						return .Problem("Error updating family up-folder", undefined, result.errMsg)
					end	

				else
					return .Problem(Str.Format("Error getting family up-folder with DATAID %1", rec.DATAID))
				end

			end
		end

		// metadata		
		Assoc find
		find.Family = family
		find.Relationship = relationship

		Assoc change
		change.Family = newFamily

		result = ._UpdateSet (prgCtx, find, change)
		if not result.ok; return result; end

		return .OK()
	end

end
