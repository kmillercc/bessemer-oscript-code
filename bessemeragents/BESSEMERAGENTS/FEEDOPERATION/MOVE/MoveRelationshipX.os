package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveRelationshipX inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::MoveRelationship

	override	Boolean	requiredNewRelationship = FALSE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

							newRelationship = relationship

							return .OSParent.Execute ( \
									prgCtx, \
									family, \
									relationship, \
									account, \
									subaccount, \
									contact, \
									newFamily, \
									newRelationship, \
									newAccount, \
									newSubaccount, \
									newContact \
								)
						end

end
