package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveSubaccount inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::AbstractMove

	override	String	fCode = 'M'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredNewAccount = TRUE
	override	Boolean	requiredNewFamily = TRUE
	override	Boolean	requiredNewRelationship = TRUE
	override	Boolean	requiredNewSubaccount = TRUE
	override	Boolean	requiredRelationship = TRUE
	override	Boolean	requiredSubaccount = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		Assoc result

		if subaccount != newSubaccount
			return .Problem ("Moving must not also rename")
		end

		// hierarchy
		result = ._GetHierarchyNode (prgCtx, family + ":" + relationship + ":" + account + ":" + subaccount)
		if not result.OK; return result; end
		DAPINode subaccountNode = result.node

		result = ._GetHierarchyNode (prgCtx, newFamily + ":" + newRelationship + ":" + newAccount)
		if not result.OK; return result; end
		DAPINode destinationNode = result.node

		// move the node
		Assoc moved = $LLIAPI.LLNodeSubsystem.GetItem (subaccountNode.pSubtype).NodeMove (subaccountNode, destinationNode)
		if not moved.OK
			return .Problem ("Error moving node")
		end

		// update subaccount facets and perms
		Assoc facets
		facets.(._GetFacet (prgCtx, 'subaccount')) = subaccount
		result = .UpdateSubAccountFacetsPerms(prgCtx, subaccountNode, facets, newRelationship, relationship)
		if not result.OK; return result; end

		// mymymy
		DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
		if IsError(mySubaccounts)
			return .Problem ("No My Subaccounts")
		end

		result = ._GetNode (prgCtx, subaccount, mySubaccounts)
		if not result.OK; return result; end
		subaccountNode = result.node

		// Update account facets and perms				
		result = .UpdateSubAccountFacetsPerms(prgCtx, subaccountNode, facets, newRelationship, relationship)
		if not result.OK; return result; end

		// KM 9/5/17 Update "Up" virtual folder inside this relationship
		Assoc accountFacets
		accountFacets.(._GetFacet (prgCtx, 'account')) = newAccount

		result = ._GetNode (prgCtx, account, subaccountNode)
		if result.OK
			DAPINODE accountNode = result.node

			result = .UpdateAccountFacetsPerms(prgCtx, accountNode, accountFacets, newRelationship, relationship)
			if result.OK	
				result = ._NodeRename(accountNode, newAccount)
				if !result.ok
					return .Problem("Error renaming up-folder folder: ", undefined, result.errMsg)
				end	
			else
				return .Problem("Error updating account up-folder", undefined, result.errMsg)	
			end
		else
			return .Problem("Error finding account up-folder in subaccount: ", undefined, result.errMsg)	
		end


		// associations
		Integer updated = CAPI.Exec ( \
				prgCtx.fDBConnect.fConnection, \
				"update BESSEMER_FRAS_Contacts set Subaccount = :A1 where Subaccount = :A2", \
				newSubaccount, subaccount \
			)

		if not prgCtx.fDBConnect.CheckError (updated).OK
			return .Problem ("Error updating contact associations")
		end


		// metadata

		Assoc find
		find.Family = family
		find.Relationship = relationship
		find.Account = account
		find.Subaccount = subaccount

		Assoc change
		change.Family = newFamily
		change.Relationship = newRelationship
		change.Account = newAccount

		result = ._UpdateSet (prgCtx, find, change)
		if not result.OK; return result; end

		return .OK()
	end

end
