package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object AbstractMove inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation

	public function Assoc UpdateAccountFacetsPerms(Object prgCtx, DAPINode node, Assoc facets, String relationshipName, String oldRelationshipName)

		// update facets
		Assoc status = ._UpdateVFFacets(prgCtx, node, facets)
		if !status.ok
			return status
		end

		// now update the permissions
		status = $Bessemer.AccountPermissions.ReplaceRelationshipGroup(prgCtx, node, relationshipName, oldRelationshipName)
		if !status.ok
			return status
		end

		return .OK()
	end

	public function Assoc UpdateFamilyFacets(Object prgCtx, DAPINode node, Assoc facets)

		// update facets
		Assoc status = ._UpdateVFFacets(prgCtx, node, facets)
		if !status.ok
			return status
		end	

		return .OK()
	end


	public function Assoc UpdateRelationshipFacetsPerms(Object prgCtx, DAPINode node, Assoc facets, String relationshipName, String oldRelationshipName)

		// update facets
		Assoc status = ._UpdateVFFacets(prgCtx, node, facets)
		if !status.ok
			return status
		end

		// now update the permissions
		status = $Bessemer.RelationshipPermissions.ReplaceRelationshipGroup(prgCtx, node, relationshipName, oldRelationshipName)
		if !status.ok
			return status
		end		

		return .OK()
	end


	public function Assoc UpdateSubAccountFacetsPerms(Object prgCtx, DAPINode node, Assoc facets, String relationshipName, String oldRelationshipName)

		// update facets
		Assoc status = ._UpdateVFFacets(prgCtx, node, facets)
		if !status.ok
			return status
		end

		// now update the permissions
		status = $Bessemer.SubAccountPermissions.ReplaceRelationshipGroup(prgCtx, node, relationshipName, oldRelationshipName)
		if !status.ok
			return status
		end		

		return .OK()
	end

end
