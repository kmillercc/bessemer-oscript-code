package BESSEMERAGENTS::FEEDOPERATION::MOVE

public Object MoveAccountX inherits BESSEMERAGENTS::FEEDOPERATION::MOVE::MoveAccount

	override	Boolean	requiredNewAccount = FALSE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

		newAccount = account

		return .OSParent.Execute ( \
				prgCtx, \
				family, \
				relationship, \
				account, \
				subaccount, \
				contact, \
				newFamily, \
				newRelationship, \
				newAccount, \
				newSubaccount, \
				newContact \
			)
	end

end
