package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object CreateRelationship inherits BESSEMERAGENTS::FEEDOPERATION::CREATION::NodeCreation

	override	String	fCode = 'C'
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredRelationship = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Assoc result
				Assoc facets
				facets.(._GetFacet (prgCtx, 'relationship')) = relationship

				// family node
				result = ._GetHierarchyNode (prgCtx, family)
				if not result.OK
					return result
				else
					DAPINode familyNode = result.node

					// create the relationship
					result = .CreateRelationshipNode(prgCtx, familyNode, relationship, facets)
					if not result.OK
						return result
					end
				end

				// mymymy	
				DAPINode myRelationships = ._GetMyMyMy (prgCtx, 'Relationships')
				if IsError(myRelationships)
					return .Problem ("No My Relationships")
				else
					// create relationship under myRelationships
					result = .CreateRelationshipNode(prgCtx, myRelationships, relationship, facets)
					if not result.ok
						return result
					else
						DAPINODE myRelNode = result.result

						// "up"
						Assoc upFacets
						upFacets.(._GetFacet (prgCtx, 'family')) = family

						// create family up folder
						result = .CreateUpNode(prgCtx, myRelNode, family, upFacets)
						if not result.ok
							return result

						else

							DAPINODE fgUpNode = result.result

							// remove inherited permissions
							Assoc status = $BESSEMER.Utils.RemoveACLs(fgUpNode)
							if !status.ok
								return status		
							end	

						end				

					end			
				end

				return .OK()
			end

end
