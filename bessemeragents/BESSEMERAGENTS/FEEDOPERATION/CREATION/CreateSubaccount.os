package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object CreateSubaccount inherits BESSEMERAGENTS::FEEDOPERATION::CREATION::NodeCreation

	override	String	fCode = 'C'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredRelationship = TRUE
	override	Boolean	requiredSubaccount = TRUE

	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Assoc result

				Assoc facets
				facets.(._GetFacet (prgCtx, 'subaccount')) = subaccount


				// get acount node as the parent
				result = ._GetHierarchyNode (prgCtx, family + ":" + relationship + ":" + account)
				if !result.OK
					return result
				else	
					// create the sub account
					DAPINode accountNode = result.node

					result = .CreateSubAccountNode(prgCtx, accountNode, subaccount, facets, relationship)
					if not result.OK
						return result
					end			
				end

				// mymymy
				DAPINode mySubaccounts = ._GetMyMyMy (prgCtx, 'Subaccounts')
				if IsError(mySubaccounts)
					return .Problem ("No My Subaccounts")
				else
					// create the subaccount in mySubAccounts
					result = .CreateSubAccountNode(prgCtx, mySubaccounts, subaccount, facets, relationship)
					if not result.OK
						return result
					else
						DAPINODE mySubAccountNode = result.result
						// "up"
						Assoc upFacets
						upFacets.(._GetFacet (prgCtx, 'account')) = account

						// create the account up folder
						result = .CreateUpNode(prgCtx, mySubAccountNode, account, upFacets)
						if not result.ok
							return result
						else
							DAPINODE accountUpNode = result.result
							upFacets = Assoc.CreateAssoc()
							upFacets.(._GetFacet (prgCtx, 'relationship')) = relationship

							// create the relationship up folder
							result = .CreateUpNode(prgCtx, accountUpNode, relationship, upFacets)
							if not result.ok
								return result
							else
								DAPINODE relUpNode = result.result
								upFacets = Assoc.CreateAssoc()
								upFacets.(._GetFacet (prgCtx, 'family')) = family

								// create the family up folder			
								result = .CreateUpNode(prgCtx, relUpNode, family, upFacets)
								if not result.ok
									return result
								end
							end
						end		
					end				
				end

				return .OK()
			end

end
