package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object CreateFamily inherits BESSEMERAGENTS::FEEDOPERATION::CREATION::NodeCreation

	override	String	fCode = 'C'
	override	Boolean	requiredFamily = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Assoc result

				Assoc facets
				facets.(._GetFacet (prgCtx, 'family')) = family

				// hierarchy root
				result = ._GetHierarchyNode (prgCtx)
				if !result.ok
					return result
				else

					DAPINode hierarchyRoot = result.node

					// create the FG folder under root		
					result = .CreateFamilyNode(prgCtx, hierarchyRoot, family, facets)
					if not result.ok
						return result
					end			

				end


				// mymymy
				DAPINode myFamilies = ._GetMyMyMy (prgCtx, 'Families')
				if IsError(myFamilies)
					return .Problem ("No My Families")
				else
					// create the FG under my families root
					result = .CreateFamilyNode(prgCtx, myFamilies, family, facets)
					if not result.ok
						return result
					end			
				end

				return .OK()
			end

end
