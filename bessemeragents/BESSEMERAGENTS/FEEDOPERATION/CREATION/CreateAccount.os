package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object CreateAccount inherits BESSEMERAGENTS::FEEDOPERATION::CREATION::NodeCreation

	override	String	fCode = 'C'
	override	Boolean	requiredAccount = TRUE
	override	Boolean	requiredFamily = TRUE
	override	Boolean	requiredRelationship = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Assoc result

				Assoc facets
				facets.(._GetFacet (prgCtx, 'account')) = account

				// get relationship node inside the family node	
				result = ._GetHierarchyNode (prgCtx, family + ":" + relationship)
				if not result.ok
					return result
				else
					DAPINode relationshipNode = result.node

					// now creat the account inside that relationship
					result = .CreateAccountNode(prgCtx, relationshipNode, account, facets, relationship)
					if not result.ok
						return result
					end				
				end

				// Create an account node under myAccounts
				DAPINode myAccounts = ._GetMyMyMy (prgCtx, 'Accounts')
				if IsError(myAccounts)
					return .Problem ("No My Accounts")
				else
					result = .CreateAccountNode (prgCtx, myAccounts, account, facets, relationship)
					if not result.ok
						return result
					else				
						DAPINODE myAccountNode = result.result
						// relationship "up" folder	facets	
						Assoc upFacets
						upFacets.(._GetFacet (prgCtx, 'relationship')) = relationship

						// create the relationship up folder
						result = .CreateUpNode(prgCtx, myAccountNode, relationship, upFacets)
						if not result.ok
							return result
						else
							DAPINODE reUpNode = result.result

							// family up folder facets
							upFacets = Assoc.CreateAssoc()
							upFacets.(._GetFacet (prgCtx, 'family')) = family

							// create family up folder inside the relationship up folder
							result = .CreateUpNode(prgCtx, reUpNode, family, upFacets)
							if not result.ok
								return result
							end					
						end			
					end			

				end

				return .OK()
			end

end
