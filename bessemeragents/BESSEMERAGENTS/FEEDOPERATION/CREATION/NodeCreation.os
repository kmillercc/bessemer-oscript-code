package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object NodeCreation inherits BESSEMERAGENTS::FEEDOPERATION::UpdateOperation


	public function Assoc CreateAccountNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets, String relationshipName )

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node	
				end			

				// set the permissions
				status = $Bessemer.AccountPermissions.SetPermissionsOnCreate(prgCtx, newNode, relationshipName)
				if !status.ok
					return status
				end		

				return .Success(newNode)		
			end


	public function Assoc CreateContactNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets )

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node			
				end			

				// set the permissions
				status = $Bessemer.ContactPermissions.SetPermissionsOnCreate(prgCtx, newNode)
				if !status.ok
					return status
				end		

				return .Success(newNode)		
			end


	public function Assoc CreateFamilyNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets, String relationshipName = undefined )

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node			
				end			

				// set the permissions
				status = $Bessemer.FamilyPermissions.SetPermissionsOnCreate(prgCtx, newNode, relationshipName)
				if !status.ok
					return status
				end		

				return .Success(newNode)		
			end


	public function Assoc CreateRelationshipNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets )

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node			
				end			

				// set the permissions
				status = $Bessemer.RelationshipPermissions.SetPermissionsOnCreate(prgCtx, newNode, newNode.pName)
				if !status.ok
					return status
				end		

				return .Success(newNode)		
			end


	public function Assoc CreateSubAccountNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets, String relationshipName )

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node			
				end			

				// set the permissions
				status = $Bessemer.SubAccountPermissions.SetPermissionsOnCreate(prgCtx, newNode, relationshipName)
				if !status.ok
					return status
				end		

				return .Success(newNode)		
			end


	public function Assoc CreateUpNode( Object prgCtx, DAPINode parentNode, String name, Assoc facets)

				DAPINODE newNode

				// create virtual folder
				Assoc status = ._CreateVF( prgCtx, parentNode, name, facets )
				if !status.ok
					return status
				else
					newNode = status.node	
				end			

				return .Success(newNode)		
			end


	private function Assoc _CreateVF ( Object prgCtx, DAPINode parentNode, String name, Assoc facets )

				// setup for create, using config

				Assoc nodeInfo

				nodeInfo.locationID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'repository')
				nodeInfo.showSelectedLocation = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'showSelectedLocation')
				nodeInfo.showSelectedFacets = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'showSelectedFacets')
				nodeInfo.showLocationColumn = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'showLocationColumn')


				// setup facets for VF
				nodeInfo.selectedFacets = Assoc.CreateAssoc()
				nodeInfo.selectedFacetsOrder = {}

				Dynamic facetID
				for facetID in Assoc.Keys (facets)
					String facetReference = Str.Format ("f_%1", facetID)

					nodeInfo.selectedFacets.(facetReference) = Type (facets.(facetID)) == ListType ? facets.(facetID) : { facets.(facetID) }
					nodeInfo.selectedFacetsOrder = { @nodeInfo.selectedFacetsOrder, facetReference }
				end


				// create

				Object LLNode = $LLIAPI.LLNodeSubsystem.GetItem ($TypeVirtualFolder)

				Assoc allocated = LLNode.NodeAlloc (parentNode, name, nodeInfo)
				if not allocated.ok; return .Problem (allocated.errMsg); end

				DAPINode node = allocated.Node
				Assoc created = LLNode.NodeCreate (node, parentNode, nodeInfo)	
				if not created.ok
					return .Problem (created.errMsg)
				else
					EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Created node named %1 with dataid %2 in parent container %3 [%4]", name, node.pID, parentNode.pName, parentNode.pID))	
				end

				// check the facets were fully used (!)		
				if not node.pExtendedData or \
						type (node.pExtendedData) != Assoc.AssocType or \
						not node.pExtendedData.selectedFacets or \
						length (node.pExtendedData.selectedFacets) != length (facets)

					LLNode.NodeDelete (node)	

					return .Problem ("Facets were not fully incorporated into vf node")
				end

				Assoc result
				result.ok = true
				result.node = node

				return result

			end

end
