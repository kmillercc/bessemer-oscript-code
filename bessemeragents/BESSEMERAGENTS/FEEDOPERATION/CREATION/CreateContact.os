package BESSEMERAGENTS::FEEDOPERATION::CREATION

public Object CreateContact inherits BESSEMERAGENTS::FEEDOPERATION::CREATION::NodeCreation

	override	String	fCode = 'C'
	override	Boolean	requiredContact = TRUE



	override function Assoc Execute ( \
								Object prgCtx, \
								String family, \
								String relationship, \
								String account, \
								String subaccount, \
								String contact, \
								String newFamily, \
								String newRelationship, \
								String newAccount, \
								String newSubaccount, \
								String newContact \
							)

				Assoc result

				Assoc facets
				facets.(._GetFacet (prgCtx, 'contact')) = contact

				// mymymy
				DAPINode myContacts = ._GetMyMyMy (prgCtx, 'Contacts')
				if IsError(myContacts)
					return .problem ("No My Contacts")
				end

				result = .CreateContactNode(prgCtx, myContacts, contact, facets)
				if not result.OK; return result; end

				return .OK()
			end

end
