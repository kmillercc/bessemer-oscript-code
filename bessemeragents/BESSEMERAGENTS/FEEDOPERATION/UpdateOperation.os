package BESSEMERAGENTS::FEEDOPERATION

public Object UpdateOperation inherits BESSEMERAGENTS::FEEDOPERATION::Operation


	private function Assoc _UpdateVFFacets ( Object prgCtx, DAPINode node, Assoc facets)

		Assoc extendedData = node.pExtendedData

		extendedData.selectedFacets = Assoc.CreateAssoc()
		extendedData.selectedFacetsOrder = {}

		Dynamic facetID
		for facetID in Assoc.Keys (facets)
			String facetReference = Str.Format ("f_%1", facetID)

			extendedData.selectedFacets.(facetReference) = Type (facets.(facetID)) == ListType ? facets.(facetID) : { facets.(facetID) }
			extendedData.selectedFacetsOrder = { @extendedData.selectedFacetsOrder, facetReference }
		end

		node.pExtendedData = extendedData

		// update the node
		// KM 1-29-21 call NodeUpdate so that we can force the Re-Index
		Assoc status = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType).NodeUpdate(node)	
		if !status.ok
			return .Problem (Str.Format("Error updating node facets for %1. %2", node.pID, status.errMsg))
		end

		return .OK()
	end

end
