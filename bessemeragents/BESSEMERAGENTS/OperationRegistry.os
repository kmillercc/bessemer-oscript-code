package BESSEMERAGENTS

public Object OperationRegistry inherits BESSEMERAGENTS::Root

	public		Assoc	fRegistry



	public function Object Get (\
									String code, \
									String family, \
									String relationship, \
									String account, \
									String subaccount, \
									String contact, \
									String newFamily, \
									String newRelationship, \
									String newAccount, \
									String newSubaccount, \
									String newContact \
								)

					List keyList = { code, \
									IsDefined(family), \
									IsDefined(relationship), \
									IsDefined(account), \
									IsDefined(subaccount), \
									IsDefined(contact), \
									IsDefined(newFamily), \
									IsDefined(newRelationship), \
									IsDefined(newAccount), \
									IsDefined(newSubaccount), \
									IsDefined(newContact)}

					String keyStr = Str.ValueToString (keyList)				

					return .fRegistry.(keyStr)
				end


	public function object New()
					Object t = OS.NewTemp( this )
					t.fRegistry = Assoc.CreateAssoc()
					return t
				end


	public function void Register (Object item)

					List keyList = { item.fCode, \
									item.requiredFamily, \
									item.requiredRelationship, \
									item.requiredAccount, \
									item.requiredSubaccount, \
									item.requiredContact, \
									item.requiredNewFamily, \
									item.requiredNewRelationship, \
									item.requiredNewAccount, \
									item.requiredNewSubaccount, \
									item.requiredNewContact }

					String keyStr = Str.ValueToString (keyList)		

					if .fRegistry.(keyStr)
						echoError("Bessemer OperationsSubsystem warning: duplicate")
					end

					.fRegistry.(keyStr) = item

				end

end
