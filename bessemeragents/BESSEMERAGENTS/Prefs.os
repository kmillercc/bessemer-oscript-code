package BESSEMERAGENTS

public Object Prefs inherits BESSEMERAGENTS::Root

	public		Assoc	fPrefs



	public function Dynamic GetIniPref( String moduleName, \
											 String section, \
											 String key, \
											 Boolean convertToValue = false, \
											 String dataType = "string", \
											 Dynamic defaultVal = undefined)

				if IsUndefined(.fPrefs)
					.fPrefs = Assoc.CreateAssoc()
				end

				String 		fullKey = Str.Format("%1.%2.%3", moduleName, section, key)
				Dynamic		pref = .fPrefs.(fullKey)

				if IsUnDefined(pref)
					FilePrefs	fprefs = ._LoadFilePrefs(moduleName)

					if ( !IsError( fprefs ) && IsDefined( fprefs ) )

						pref = Fileprefs.GetPref( fprefs, section, key )
						Fileprefs.Close( fprefs )		

						if IsDefined(pref) && IsNotError(pref)
							if dataType != "string" && convertToValue
								pref = $BESSEMERAGENTS.FormatPkg.ConvertStringToVal(pref, dataType)
							end	
						end

					end

					if IsUnDefined(pref) || IsError(pref)
						pref = defaultVal
					end

					.fPrefs.(fullKey) = pref

				end

				return pref
			end

	Script GetIniPrefs










																		//
																		//
																		// Syntergy
																		// Author: 			Kit Miller  
																		// Date: 			May 5, 2008
																		// Description:		Retrieves all preferences in the form of an Assoc from the module ini file.
																		// Modifications:
																		// Date           Name         Description
																		//
																		//////////////////////////////////////////////////////

																		function Assoc GetIniPrefs( 	String moduleName, \
																										String section, \
																								    	Boolean convertToValue = false)

																			if IsUndefined(.fPrefs)

																				FilePrefs	myFilePrefs = ._LoadFilePrefs(moduleName)

																				Assoc		prefs
																				Dynamic		eachKey
																				Dynamic		val

																				if ( IsDefined( myFilePrefs ) && !IsError( myFilePrefs ) )

																					prefs = FilePrefs.ListValues( myFilePrefs, section )
																					Fileprefs.Close( myFilePrefs )		

																					if IsDefined(prefs) && IsNotError(prefs)
																						if convertToValue
																							for eachKey in Assoc.Keys(prefs)
																								if !_IsAlpha(prefs.(eachKey))
																									val = $BESSEMERAGENTS.FormatPkg.ConvertStringToVal(prefs.(eachKey))
																									if IsDefined(val)
																										prefs.(eachKey) = val
																									end		
																								end	
																							end	
																						end	
																					else
																						prefs = Assoc.CreateAssoc()	
																					end
																				end

																				.fPrefs = prefs

																			end	

																			return .fPrefs

																		end


																		function Boolean _IsAlpha(String s)

																			List match = Pattern.Find(s, '[A-Za-z]+', TRUE)

																			if IsNotError(match) && IsDefined(match)
																				if match[3] == s
																					return true
																				end
																			end	

																			return false

																		end


















	endscript


	private function Dynamic _LoadFilePrefs(String moduleName)

										String 		globalName = moduleName + 'IniPrefs'
										FilePrefs	fPrefs = Global.Application(globalName) 

										if IsDefined(fPrefs)
											return fPrefs
										else	
											String		iniFile = $Kernel.ModuleUtils.ModuleIniPath(moduleName)
											fPrefs = Fileprefs.Open( iniFile )
											Fileprefs.Close( fprefs )

											Global.Application(globalName, fPrefs)
										end	

										return fPrefs

									end

end
