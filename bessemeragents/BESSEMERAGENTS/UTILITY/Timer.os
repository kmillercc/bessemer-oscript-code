package BESSEMERAGENTS::UTILITY

public Object Timer inherits BESSEMERAGENTS::Root

	public		String	_fLogClassName = 'syntergycore.timer'
	public		Dynamic	_fTimers
	public		Integer	fUniqueCounter



	public function Real Elapsed( Dynamic key = "_" )

					Integer endMicros = Date.MicroTick()
					Integer endTicks = Date.Tick()
					Dynamic rec = ._fTimers.( key )

					if IsUndefined( rec )
						Echo( "Invalid timer specified in Timer.Elapsed(): ", key, ". Timer must be started before it is stopped." )
						return undefined
					end

					return ._Diff( rec.startMicros, endMicros, rec.startTicks, endTicks )
				End

	Script GetDbMs











	endscript


	public function IsRunning( Dynamic key = "_" )

					return Assoc.IsKey( ._fTimers, key )

				End


	public function New()
					Dynamic obj = OS.NewTemp( this )	
					obj._fTimers = Assoc.CreateAssoc()	
					return obj
				End


	public function Start( Dynamic key = "_" )
					Assoc rec
					._fTimers.( key ) = rec

					rec.startTicks = Date.Tick()
					rec.startMicros = Date.MicroTick()
				End


	public function Integer StartUnique()
					Integer timerID = .fUniqueCounter
					.fUniqueCounter += 1
					.Start( timerID )
					return timerID
				End


	public function Real Stop( Dynamic key = "_" )

					Integer endTicks = Date.Tick()
					Integer endMicros = Date.MicroTick()
					Dynamic rec = Assoc.Delete( ._fTimers, key )

					if IsUndefined( rec )
						Echo( "Invalid timer specified in Timer.Stop(): ", key, ". Timer must be started before it is stopped." )
						return undefined
					end

					Integer startTicks = rec.startTicks		
					Integer startMicros = rec.startMicros

					return ._Diff( startMicros, endMicros, startTicks, endTicks ) 
				End


	public function Real StopPrint( String prefix = "Time: ", Dynamic key = "_" )
					Real t = .Stop( key )
					Echo( prefix, t, " ms" ) 
					return t
				End


	private function Real _Diff( Integer startMicros, \
									 Integer endMicros, \
									 Integer startTicks, \
									 Integer endTicks )

					Integer maxTick = 2147483647 	
					Integer diffTicks = endTicks - startTicks

					if diffTicks < 0
						// if it rolled over, we assume it only rolled once.
						diffTicks = maxTick - startTicks + endTicks
					end

					Real totalMS

					// use micros for short intervals ...	
					if diffTicks < 20000
						Integer diffMicros = endMicros - startMicros

						if diffMicros < 0
							diffMicros = maxTick - startMicros + endMicros
						end	

					 	totalMS = diffMicros / 1000.0
					else
						totalMS = diffTicks
					end

					return totalMS
				End

end
