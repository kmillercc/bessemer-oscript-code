package BESSEMERAGENTS::UTILITY

public Object Utils inherits BESSEMERAGENTS::Root

	public		Integer	MAX_SESSION_TTL = 20000
	public		String	fKINISection = 'Bessemer'



	public function FileToWeb( Object socketOut, String filename, String downloadName, Boolean delete = true, String contentType = "application/octet-stream" )
					Assoc rtn
					rtn.ok = true

					if File.Exists( fileName ) 

						File f = File.Open( fileName, File.ReadBinMode )

						if IsNotError( f )

							// build the headers for the document

							String headers = Str.Format( "Content-Type: %1%4" + \
														 "Content-Length: %2%4" + \
														 "Content-Disposition: attachment; filename=%3%4%4", \
														 contentType, f.pSize, downloadName, Web.CRLF )

							// write the headers
							Web.Write( socketOut, headers)

							Bytes b

							// write the file data
							for ( b = File.ReadBytes( f, 30720 ); b != File.E_Eof; b = File.ReadBytes( f, 30720 ) )
								Socket.Write( socketOut, b )
							end

							Socket.Close( socketOut )
							File.Close( f )

							// clean up the file

							if delete
								File.Delete( fileName )
							end

						else
							return $_CSErr.New( Str.Format( [BESSEMERAGENTS_ERRMSG.CouldNotOpenFile1], $Kernel.FileUtils.BaseName( fileName ) ) )
						end

					else
						return $_CSErr.New( Str.Format( [BESSEMERAGENTS_ERRMSG.FileDoesNotExist1], $Kernel.FileUtils.BaseName( fileName ) ) )
					end

					return rtn
				End


	public function GUID GenerateGUID()
					return GUID.FromString( JavaObject.InvokeStaticMethod( "java.lang.String", "valueOf", { JavaObject.InvokeStaticMethod( "java.util.UUID", "randomUUID", {} ) } ) )
				end


	public function Assoc GetAdminCtx()
					Assoc rtn
					rtn.ok = true

					String TICK_PROP = "__create_tick"

					Object cached = $BessemerAgents.fAdminCtx

					if IsDefined( cached ) 
						if OS.IsObject( cached ) && OS.IsFeature( cached, "fDBConnect" ) && CAPI.IsValid( cached.fDBConnect.fConnection )

							// Make sure it's not too old -- we want session properties to be 
							// periodically refreshed.

							Integer createTick = cached.GetSessionProperty( TICK_PROP )

							if IsDefined( createTick )
								if Date.Tick() - createTick < .MAX_SESSION_TTL
									// cached connection looks good.
									rtn.prgCtx = cached	
								else
									Echo( "Admin session too old -- refreshing." )
								end
							else
								cached.SetSessionProperty( TICK_PROP, Date.Tick() )
								rtn.prgCtx = cached
							end

						elseif OS.IsObject( cached )

							echo( "ERROR: Invalid connection was found, attempting to recover" )

							$LLIApi.DbConnect.DumpConnectInfo()
							$LLIApi.DbConnect.DumpLoginInfo()

							echo( "Deleting all existing program sessions" )

							$LLIApi.PrgSession.ClearAllPrgSessionsFromCache()

							$LLIApi.DbConnect.DumpConnectInfo()
							$LLIApi.DbConnect.DumpLoginInfo()

							/* 
							EchoError( "Trying to reset cached program session" )

							List sessions
							Dynamic sess

							for sess in $PrgSessions 
								if sess == cached
									cached.Delete()
								else	
									sessions = {@sessions, sess}
								end
							end		

							$PrgSessions = sessions	
							*/
						end
					end

					if !rtn.prgCtx
						$BessemerAgents.fAdminCtx = undefined 

						Echo( "Trying to generate new program session for admin because one is not cached" )

						rtn = ._NewAdminCtx()
						$BessemerAgents.fAdminCtx = rtn.prgCtx

						if IsDefined( rtn.prgCtx )
							rtn.prgCtx.SetSessionProperty( TICK_PROP, Date.Tick() )
						end
					end

					return rtn
				end


	public function Assoc GetLangView( Object prgCtx, String viewName )

					Assoc status = prgCtx.GetUserLanguage()

					if !status.ok
						return status
					end	

					Assoc rtn
					rtn.ok = true

					rtn.result = Str.Format( "%1_%2", viewName, status.userLanguage )

					return rtn	

				End


	public function String GetTempFilename( \
					String 		prefix = "cstemp", \
					String		extension = "tmp" )

					String retVal = Str.Format( '%1%2_%3_%4.%5', \
												$Kernel.FileUtils.GetTempDir(), \
												prefix, \
												System.ThreadIndex(), \
												Date.Microtick(), \
												extension )

					// File may exist, delete it
					File.Delete( retVal )

					return retVal
				end


	public function Dynamic IniGet( Object prgCtx, String section, String keyword, Dynamic defaultVal = undefined )

					if IsFeature( prgCtx, "IniGet" )
						return prgCtx.IniGet( section, keyword, defaultVal )
					else
						return CAPI.IniGet( prgCtx.fDbConnect.fLogin, section, keyword, defaultVal )
					end
				End


	public function Dynamic IniPut( Object prgCtx, String section, String keyword, Dynamic val )

					if IsFeature( prgCtx, "IniPut" )
						return prgCtx.IniPut( section, keyword, val )
					else
						return CAPI.IniPut( prgCtx.fDbConnect.fLogin, section, keyword, val )
					end
				End


	public function Boolean IsLLVersionGE( Integer major, Integer minor = 0, Integer revision = 0 )

					List appVer = System.AppVersionList()
					List checks = { major, minor, undefined, revision }
					Integer i

					for i = 1 to 4
						Integer check = checks[i]

						continueif IsUndefined( check )	

						Integer llVer = Str.StringToInteger( appVer[i] )

						if llVer < check
							return false
						elseif llVer > check
							return true
						end
					end

					return true
				End


	public function Boolean IsLLVersionLE( Integer major, Integer minor = 0, Integer revision = 0 )

					List appVer = System.AppVersionList()
					List checks = { major, minor, undefined, revision }
					Integer i

					for i = 1 to 4
						Integer check = checks[i]

						continueif IsUndefined( check )	

						Integer llVer = Str.StringToInteger( appVer[i] )

						if llVer > check
							return false
						elseif llVer < check
							return true
						end
					end

					return true
				End


	public function String JoinPath( ... )

					Integer numParams = NParameters()

					if numParams == 0 
						return ""
					elseif numParams == 1
						return Str.String( Parameters()[1] )
					end	

					String sep = File.Separator()
					String altSeparator = sep == '/' ? '\' : '/'

					String rtnPath = Str.ReplaceAll( Str.Catenate( Parameters(), sep ), altSeparator, sep )
					String lastPath 

					while lastPath != rtnPath
						lastPath = rtnPath
						rtnPath = Str.ReplaceAll( rtnPath, sep + sep, sep )
					end

					if rtnPath[-1] == sep
						rtnPath = rtnPath[:-2]
					end

					return rtnPath
				End


	public function String PaginateQuery( String query, String orderCol )
					return Str.Format( \
						"select * from ( select clientView.*, row_number() over ( order by %1 ) RN  from ( %2 ) clientView ) restrictedView where restrictedView.RN between :n1 and :n2", \
						orderCol, query ) 		
				End


	public function Object PrgCtx()

					// quick call to get admin program session -- mostly for testing.

					Assoc status = .GetAdminCtx()

					if status.ok
						return status.prgCtx
					end

					EchoError( Str.Format( [BESSEMERAGENTS_ERRMSG.CouldNotGetAdminProgramSession1], status.errMsg ) )

					return undefined
				End

	Script QueryToCSV




								Function Assoc QueryToCSV( Object prgCtx = .GetAdminCtx().prgCtx, String stmt = "select * From dtree where parentid=:A1", Dynamic bindVals = { 2000 }, String filename = "c:\temp\dtreetmp.csv") 

									Assoc rtn
									rtn.ok = true

									File f = File.Open( filename, File.WriteMode )

									if IsError( f )
										return $_CSErr.New( Str.Format( [BESSEMERAGENTS_ERRMSG.CouldNotOpenFileForWriting1], filename ) )
									end

									Object cursor = $LLIApi.DbCursor.New(\
																	prgCtx.fDbConnect,\
																	stmt,\
																	bindVals,\
																	SQL.CACHE | 10 )

									if IsError( cursor )
										rtn = $_CSErr.New( [BESSEMERAGENTS_ERRMSG.ErrorCreatingCursor] )
									else	
										Boolean header = true

										for(;;)

											Record rec = cursor.Fetch()

											if IsError( rec )
												if rec != SQL.ROLLBACK
													rtn = $_CSErr.Wrap( rec, [LLAPI_ERRMSG.ErrorFetchingFromCursor] )
												end
												break
											end

											if header
												WriteCSVRow( f, RecArray.FieldNames( rec ) )
												header = false
											end

											WriteCSVRow( f, rec )
										end

										cursor.Delete()	
									end

									File.Close( f )

									return rtn		
								End

								Function WriteCSVRow( File f, Dynamic vals )
									Dynamic v

									List cols 

									for v in vals
										Integer t = Type( v )
										switch t
										case StringType
											cols = { @cols, Str.ReplaceAll( Str.String( v ), '"', '""' ) } 
										end
										case DateType
											cols = { @cols, Date.DateToString( v, "%Y-%m-%d %I:%M:%S %p" ) } 
										end
										case IntegerType, LongType, RealType
											cols = { @cols, Str.String( v ) }
										end
										default
											cols = { @cols, "" }
										end
										end
									end

									File.Write( f, Str.Catenate( cols, ',' )[:-2] )
								End 







	endscript


	public function List RecArrayToAssocList( RecArray r, Boolean lowerNames = true )
					String fieldName
					List result 
					Record rec 
					Assoc origAssoc, tmp

					if lowerNames
						for fieldName in RecArray.FieldNames( r )
							origAssoc.( Str.Lower( fieldName ) ) = undefined
						end

						for rec in r
							tmp = Assoc.Copy( origAssoc )
							result = { @result, Assoc.Merge( tmp, Assoc.FromRecord( rec ) ) }
						end
					else
						for rec in r 
							result = { @result, Assoc.FromRecord( rec ) }
						end
					end

					return result
				End

	Script Testing




								Function X()
									Assoc result = .Query( $BessemerAgents.Utils.GetAdminCtx().prgCtx, "select asdf from dtree" )

									if !result.ok
										Assoc rtn = $_CSErr.Wrap( result, "Failed to exec query" )

										return rtn
									end
								End







	endscript


	private function Assoc _NewAdminCtx()

					Assoc rtn
					rtn.ok = true

					Assoc ctxStatus = $LLIAPI.AuthentPkg.GetProgramSession()

					Object prgCtx

					if ctxStatus.OK
						prgCtx = ctxStatus.prgCtx

						if IsDefined( ctxStatus.prgCtx ) && OS.IsObject( prgCtx.fDbConnect ) && !CAPI.IsValid( prgCtx.fDbConnect.fConnection )
							echo( "ERROR: Invalid connection was found, attempting to recover" )

							$LLIApi.DbConnect.DumpConnectInfo()
							$LLIApi.DbConnect.DumpLoginInfo()

							echo( "Deleting all existing program sessions" )

							for prgCtx in $prgSessions 							
								prgCtx.Delete()
							end

							$LLIApi.DbConnect.DumpConnectInfo()
							$LLIApi.DbConnect.DumpLoginInfo()

							ctxStatus = $LLIAPI.AuthentPkg.GetProgramSession()

							if ctxStatus.OK
								prgCtx = ctxStatus.prgCtx
							else
								echo( "Error recovering from lost connection" )

								rtn.ok = false
								rtn.errMsg = [Web_ErrMsg.DBCnctLost]

								$CnctLost = TRUE
								prgCtx = undefined
							end
						end
					else
						rtn = $_CSErr.Wrap( ctxStatus, [BESSEMERAGENTS_ERRMSG.CouldNotAllocateProgramSession] )
					end

					if IsDefined( prgCtx ) && OS.IsObject( prgCtx.fDBConnect )
						rtn.prgCtx = prgCtx
					else
						rtn.prgCtx = undefined
					end

					return rtn
				end


	public function List pageToRowRange( Integer page, Integer pageSize )

					return { ( page - 1 ) * pageSize + 1, page * pageSize }

				End


	public function toJSON( Dynamic any = 1, Boolean forHTML = false, Boolean maybeConvertNumericKeys = true )
				/*
					Assoc a, b
					Boolean needConvert = false

					if maybeConvertNumericKeys
						a.(1) = 1
						b.("1") = 1
						needConvert=Web.ToJSON(a) != Web.ToJSON(a)	
					end			

					Assoc test

					Integer i 
					for i = 1 to 100
						test.( i ) = i	
					end

					// Check fastest way to convert...

					Integer s = Date.MicroTick()

					if Str.Locate( Str.ValueToString( Assoc.Keys( test ) ), '"' )

					Integer t = Date.MicroTick() - s
				*/
				End


	public function String toJSONForHTML( Dynamic obj ) 
					return Str.ReplaceAll( Web.ToJSON( obj ), "</", "<\/" )
				End

end
