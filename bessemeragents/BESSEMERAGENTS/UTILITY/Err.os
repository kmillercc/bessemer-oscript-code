package BESSEMERAGENTS::UTILITY

public Object Err inherits BESSEMERAGENTS::Root

	public		Boolean	fEnabled = TRUE


	Script AllMessages




								Function List AllMessages( Dynamic e )
									List msgList = { ExtractMessage( e ) }

									while IsFeature( e, "__cause" )
										e = e.__cause

										String msg = ExtractMessage( e ) 

										if msg 
											msgList = { @msgList, msg }
										end
									end

									return msgList
								End

								Function String ExtractMessage( Dynamic anything )

									switch type( anything )
									case ErrorType
										return Str.String( anything )
									end
									case Assoc.AssocType
										if IsFeature( anything, "errMsg" ) 
											return Str.String( anything.errMsg )
										elseif IsFeature( anything, "err" ) 
											return Str.String( anything.err )
										elseif IsFeature( anything, "error" )
											return Str.String( anything.( "error" ) )
										end		
									end
									end 

									return ""	
								End







	endscript


	public function Cause( Dynamic e )

					while IsDefined( e )
						if Type( e ) == Assoc.AssocType && Assoc.IsKey(e, "__cause" )
							e = e.( "__cause" )
						else
							break
						end
					end

					return e
				End


	public function String ErrStack( Assoc wrappedErr ) 
					if IsFeature( wrappedErr, "__stack" )
						return wrappedErr.__stack
					end

					return "<empty>"
				end


	public function Assoc Fields( Dynamic vals )

					Assoc rtn
					Integer i

					switch type( vals )
					case Assoc.AssocType
						rtn = Assoc.Copy( vals )
					end
					case ListType
						// these should be pairs, otherwise just include the value.
						if Length( vals ) % 2 == 1
							rtn._fields = vals
						else				
							for( i = 2; i <= Length( vals ); i += 2 )
								rtn.( vals[i-1] ) = vals[i]
							end
						end
					end
					case StringType
						List parts = Str.Elements( vals, "," )

						for( i = 1; i <= Length( parts ); i += 1 )
							List pairs = Str.Elements( parts[i], "=" )

							if Length( pairs ) == 2
								rtn.( Str.Trim( pairs[1] ) ) = Str.Trim( pairs[2] )
							elseif Length( pairs ) > 0
								rtn.( i ) = pairs
							end
						end
					end
					default
						rtn._fields = vals
					end	
					end

					return rtn 
				end

	Script GetDBError




								Function String GetDBError( Dynamic e )
									String output
									Dynamic reason = .Cause( e )

									if IsFeature( reason, "ErrorList" )
										List msgs
										Assoc a

										for a in reason.ErrorList 
											if a.detail && a.errMsg
												String eMsg = a.errMsg
												if eMsg[-1] == "."
													eMsg = eMsg[:-2]
												end
												msgs = { @msgs, Str.Format( "%1:%2", eMsg, a.detail ) }
											elseif a.errMsg
												msgs = { @msgs, a.errMsg }
											end			
										end

										output = compress( Str.Join( msgs, "; " ) )
									elseif IsFeature( reason, "errMsg" )	
										output = reason.errMsg
									end

									return Compress( output )
								End

								Function String Compress( String input ) 
									List replacements = { { ".;", ";" }, { " ;", ";" }, { Str.LF, " " }, { Str.CR, "" } }, pair
									String output = input

									for pair in replacements
										output = Str.ReplaceAll( output, pair[1], pair[2] )
									end

									return output
								end







	endscript


	public function GetFailedQuery( Dynamic e )
					Dynamic reason = .Cause( e )

					Assoc rtn

					if IsFeature( reason, "__db" )
						rtn.statement = reason.__statement
						rtn.params = reason.__params
					end	

					return rtn
				End


	public function Boolean IsDBError( Dynamic e )

					if Type( e ) == Assoc.AssocType
						if Assoc.IsKey( e, "__cause" )
							return .IsDBError( .Cause( e ) ) 
						elseif Assoc.IsKey( e, "__db" )
							return true
						end	
					end

					return false
				End

	Script IsError




								Function Boolean _IsError( Dynamic e )
									switch type( e ) 
										case ErrorType
											return true
										end
										case Assoc.AssocType
											return e.ok == false	
										end
									end

									return false
								end







	endscript


	public function Assoc New( String msg, Dynamic fields = undefined )

					Assoc rtn

					if IsDefined( fields ) 
						if Type( fields ) != Assoc.AssocType
							fields = .Fields( fields )
						end
						if Type( fields ) == Assoc.AssocType
							rtn = fields
						end
					end

					rtn.ok = false
					rtn.errMsg = msg
					rtn.__stack = ._StackTrace(1)

					return rtn
				End


	public function Assoc WithMessage( Dynamic e, String msg )
					Assoc rtn

					rtn.ok = false
					rtn.errMsg = msg
					rtn.__cause = e

					if IsFeature( e, "__stack" )
						rtn.__stack = e.__stack
					end 

					return rtn
				End


	public function Assoc WithStack( Dynamic e, String s = ._StackTrace( 1 ) )
					if type( e ) == Assoc.AssocType
						if !Assoc.IsKey( e, "__stack" ) 
							e.__stack = s		
						end
					end

					return e
				End


	public function Assoc Wrap( Dynamic e, String errDetail )
					return .WithStack( .WithMessage( e, errDetail ), ._StackTrace( 1 ) )
				End


	private function String _GetObjPath( Object obj )	
					Boolean			done = true
					Object			thisParent = obj
					String 			ospaceName = OS.FileName( obj )
					List			retList = {}

					while( done )
						retList = { thisParent.OSName, @retList }
						thisParent = OS.Parent( thisParent )
						done = ( OS.FileName( thisParent) == ospaceName )
					end

					retList = { Str.Upper( ospaceName ), @retList }

					return Str.Join( retList, "/" )
				End

	Script _NullStackTrace




								Function String StackTrace(Integer ignoreTopN = 0)
									return "Unsupported"
								End







	endscript


	private function String _ParseFrame( String val = "FuncName (2)" )
					List results = Str.Elements( Str.Trim( val ), " " )

					switch Length( results )
					case 2
						return results[1]
					end
					case 3
						return results[2]
					end
					end	 

					return "<unknown>"
				End


	private function String _ParseObjRef( String val = "#3200018d (ospace::ObjName).'T1'" )

					String rtn = "<unknown>"

					PatFind oRefPat = Pattern.CompileFind( "%<#[0-9a-zA-Z]+>[!.]*@.<?+>" )

					List results = Pattern.Find( Str.Trim( val ), oRefPat )

					if IsDefined( results )
						Dynamic obj = Str.StringToValue( results[4] )
						Dynamic feature = Str.StringToValue( results[5] )

						String objStr = "<unknown>", featureName = "<unknown>"

						if obj
							objStr = ._GetObjPath( obj )
						end

						if type( feature ) == StringType
							featureName = feature
						end

						return Str.Format( "%1.%2", objStr, featureName )
					end

					return rtn
				End

	Script _StackTrace




								Function String StackTrace(Integer ignoreTopN = 0)

									String st = System.ThreadStackCrawl()

									List callers, frameLines
									List lines = Str.Elements( st, Str.LF )
									String line
									Assoc curFrame

									for line in lines
										if Str.Locate( line, " Frame:" ) == 1

											curFrame = Assoc.CreateAssoc()
											curFrame.func = ._ParseFrame( line[8:] )
											callers = { @callers, curFrame }

										elseif IsDefined( curFrame )

											if Str.Locate( line, "   ObjRef:" ) == 1

												curFrame.obj = ._ParseObjRef( line[11:] )

											elseif Str.Locate( line, "   Line:" ) == 1

												curFrame.line = Str.Trim( line[9:] )

												if Str.CmpI( curFrame.obj[-Length(curFrame.func):], curFrame.func ) == 0
													frameLines = { @frameLines, Str.Format( "%1 [%2]", curFrame.obj, curFrame.line ) }
												else
													frameLines = { @frameLines, Str.Format( "%1 (%2) [%3]", curFrame.obj, curFrame.func, curFrame.line ) }
												end

												curFrame = undefined
											end
										end
									end

									return Str.Catenate( frameLines[ignoreTopN+2:], Str.LF )
								End







	endscript


	public function __Init()
					if this == $BESSEMERAGENTS.Err && OS.ReadOnly( this ) && $BESSEMERAGENTS.Utils.IsLLVersionLE( 10 )
						._StackTrace = ._NullStackTrace	
					end
				End

end
