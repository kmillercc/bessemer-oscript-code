package BESSEMERAGENTS

public Object WebModule inherits WEBDSP::WebModule

	override	Boolean	fEnabled = TRUE
	override	String	fModuleName = 'bessemeragents'
	override	String	fName = 'bessemeragents'
	override	List	fOSpaces = { 'bessemeragents' }
	override	String	fSetUpQueryString = 'func=bessemeragents.configure&module=bessemeragents&nextUrl=%1'
	override	List	fVersion = { '16', '2', 'r', '4' }

end
