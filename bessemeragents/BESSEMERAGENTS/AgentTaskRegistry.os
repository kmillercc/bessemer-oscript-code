package BESSEMERAGENTS

public Object AgentTaskRegistry inherits BESSEMERAGENTS::Root

	public		Assoc	fRegistry



	public function Object Get( String typeName )
					return .fRegistry.( typeName )
				End


	public function object New()
					Object t = OS.NewTemp( this )
					t.fRegistry = Assoc.CreateAssoc()
					return t
				End


	public function Register( String typeName, Object task )
					.fRegistry.( typeName ) = task
				End

end
