package BESSEMERAGENTS

/**
 * 
 *  This is a good place to put documentation about your OSpace.
 *  
 *  
 *  
 */
public Object Root

	public		Object	Globals = BESSEMERAGENTS::Globals




	public function Assoc Problem (String message, Dynamic result = undefined, String errDetail = Undefined)

					Assoc rtn

					rtn.OK = false
					rtn.errMsg = message
					if IsDefined (errDetail)
						rtn.errMsg += Str.Format(" (%1)", errDetail)
					end

					if IsDefined(result)
						if Type(result) == ErrorType
							rtn.apiError = result
						else
							rtn.errMsg += Str.Format(" (%1)", Str.String(result))
						end

					end

					return rtn

				end


	public function Assoc Query( Object prgCtx, String stmt, ... )

					Assoc rtn
					rtn.ok = true

					Dynamic subs
					Boolean execN = false
					Dynamic result

					switch NParameters()
						case 3
							subs = Parameters()[3]

							switch Type( subs )
								case ListType
								end
								case RecArray.RecArrayType
									execN = true
								end
								default
									subs = { subs }
								end 
							end
						end
						case 2
							subs = {}
						end
						default
							subs = Parameters()[3:]
						end
					end

					if execN
						result = CAPI.ExecN( prgCtx.fDBConnect.fConnection, stmt, subs)
					elseif Length( subs ) == 0
						result = CAPI.Exec( prgCtx.fDBConnect.fConnection, stmt )	
					else
						result = CAPI.Exec( prgCtx.fDBConnect.fConnection, stmt, subs)	
					end

					if IsError(result)

						Assoc dbErr = prgCtx.fDBConnect.CheckError( result, FALSE )
						Assoc.Delete( dbErr, "ErrList" )

						// Annotate the error with some info about the query.
						dbErr.__db = true
						dbErr.__statement = stmt
						dbErr.__params = subs

						rtn = $_CSErr.New( Str.String( result ), dbErr )
					else
						rtn.result = result
					end

					return rtn
				end

	/**
				 *  Content Server Startup Code
				 */
	public function Void Startup()

					//
					// Initialize globals object
					//

					Object	globals = $Bessemeragents = .Globals.Initialize()

					globals.AgentTaskRegistry = globals.AgentTaskRegistry.New()
					globals.OperationRegistry = globals.OperationRegistry.New()				
					globals.Prefs = OS.NewTemp(globals.Prefs)	
					globals.Cache = globals.Cache.New()

					//
					// Initialize objects with __Init methods
					//

					$Kernel.OSpaceUtils.InitObjects( globals.f__InitObjs )

					$_CSErr = globals.Err


					globals.Timer = globals.Timer.New()
					//globals.GeneralConfig = globals.GeneralConfig.New()
					//globals.ScheduleConfig = globals.ScheduleConfig.New()
					//globals.MailUtils = globals.MailUtils.New()
					//globals.Clock = globals.Clock.New()
					//globals.DateUtils = globals.DateUtils.New()
					//globals.ValueCache = globals.ValueCache.New()

					Echo( "Finished Bessemer Agents Startup" )
					Echo( "*********************************************************************************************************************************" )		


				end


	public function Assoc Success(Dynamic result = undefined)
					Assoc rtn
					rtn.ok = true
					rtn.result = result
					rtn.errMsg = undefined
					rtn.apiError = undefined

					return rtn
				end


	public function void TestMigrationThread()

				Object prgCtx =  $BessemerAgents.Utils.GetAdminCtx().prgCtx

				$BessemerAgents.MigrationThread.RunTasks(prgCtx)

			end


	public function void TestSyncThread()
				Object prgCtx =  $BessemerAgents.Utils.GetAdminCtx().prgCtx
				$BessemerAgents.SyncProcessThread.RunTasks(prgCtx)
			end

end
