package BESSEMERAGENTS::TASKS

public Object UGSyncOld inherits BESSEMERAGENTS::TASKS::AgentTask

	override	Boolean	fEnabled = FALSE
	override	String	fName = 'UGSync'



	override function Void SubclassExecute(Object prgCtx, Date expireDate)

				.LogInfo("Bessemer UGSync starting")

				Dynamic 	dbStatus
				Assoc		result
				Record		feedRec
				RecArray 	feedRecs = CAPI.Exec(prgCtx.fDBConnect.fConnection, "select * from BESSEMER_GROUPS_FEED where COMPLETEDATE is null order by SEQUENCE")

				if !IsError(feedRecs)

					for feedRec in feedRecs

						//result = ._ValidateFeedRecFormat(feedRec)
						result.ok = true

						if result.OK

							if prgCtx.fDBConnect.StartTrans()

								switch feedRec.OPERATION

									case 'C'

										// create (a group) 
										result = ._CreateGroup(prgCtx, feedRec)


									end	


									case 'A'

										// add user or group to ownergroup 
										result = ._AddToOrRemoveFromGroup(prgCtx, feedRec, true)


									end	


									case 'R'

										// remove user or group from ownergroup 
										result = ._AddToOrRemoveFromGroup(prgCtx, feedRec, false)


									end	


									default

										result.ErrMsg = Str.Format("Invalid OPERATION code of '%1'", feedRec.OPERATION)

									end

								end	

								// transfer any error to the feedrec
								feedRec.ISSUEMESSAGE = result.ErrMsg ? result.ErrMsg[1:255] : undefined


								// update feedrec with completedate and any issuemessage generated
								dbStatus = CAPI.Exec(prgCtx.fDBConnect.fConnection, "update BESSEMER_GROUPS_FEED set ISSUEMESSAGE=:A1, COMPLETEDATE=:A2 where SEQUENCE=:A3", feedRec.ISSUEMESSAGE, Date.Now(), feedRec.SEQUENCE)

								prgCtx.fDBConnect.EndTrans(!IsError(dbStatus) && dbStatus == 1)

							end


						end



					end
				end			


			end


	private function Assoc _AddToOrRemoveFromGroup(Object prgCtx, Record feedRec, boolean AddTo=true)

				Assoc 	result
				Record	ownerGroupRec, addUGRec


				// make sure both A USER and GROUP are NOT specified
				if feedRec.USER && feedRec.GROUP
					result.ErrMsg = Str.Format("USER and GROUP both specified on Add operation.")

				// make SURE either USER or GROUP IS specified
				elseif !feedRec.GROUP  && !feedRec.USER 

					result.ErrMsg = Str.Format("no USER or GROUP specified on %1 operation.", AddTo ? "Add" : "Replace")


				// make sure OwnerGroup is defined and exists
				elseif !feedRec.OWNERGROUP 

					result.ErrMsg = Str.Format("no OWNERGROUP specified on Add operation.")

				else

					// see if OWNERGROUP exisits and get its ID

					result = ._GetUserOrGroupRec(prgCtx, feedRec.OWNERGROUP, UAPI.GROUP)

					if !result.ugRec

						result.ErrMsg = Str.Format("OWNERGROUP '%1' does not exist.", feedRec.OWNERGROUP)

					else

						ownerGroupRec = result.ugRec

					end	 

				end

				// now check the GROUP or USER to be added
				if !result.ErrMsg

					if feedRec.USER

						result = ._GetUserOrGroupRec(prgCtx, feedRec.USER, UAPI.USER)

						if !result.ugRec

							result.ErrMsg = Str.Format("USER '%1' does not exist.", feedRec.USER)	

						else

							addUGRec = result.ugRec

						end

					end


					if feedRec.GROUP

						result = ._GetUserOrGroupRec(prgCtx, feedRec.GROUP, UAPI.GROUP)

						if !result.ugRec

							result.ErrMsg = Str.Format("GROUP '%1' does not exist.", feedRec.USER)	

						else

							addUGRec = result.ugRec

						end

					end

				end

				// add the user or group to ownerGroupID membership if all is well

				if !result.ErrMsg && ownerGroupRec && addUGRec

					if addTo

						result = $LLIAPI.UsersPkg.GroupUpdate(\
															prgCtx.USession(),\
															ownerGroupRec.ID,\
															ownerGroupRec.NAME,\
															{},\
															{addUGRec.ID},\
															{},\
															undefined \
															)	


					else

						result = $LLIAPI.UsersPkg.GroupUpdate(\
															prgCtx.USession(),\
															ownerGroupRec.ID,\
															ownerGroupRec.NAME,\
															{},\
															{},\
															{addUGRec.ID},\
															undefined \
															)	

					end




				end	

				return result

			end


	private function Assoc _CreateGroup(Object prgCtx, Record FeedRec)

				Assoc status

				// make sure we have a GROUP field and that it doesn't exist
				if !feedRec.GROUP 

					status.ErrMSg = Str.Format("no GROUP name specified")

				else

					status = ._GetUserOrGroupRec(prgCtx, feedRec.GROUP, UAPI.GROUP)

					if !status.ugRec

						// ok - group doesn't yet exist	

						status = $LLIAPI.UsersPkg.GroupNew(prgCtx.USession(), feedRec.GROUP)

					else

						status.ErrMsg = Str.Format("GROUP '%1' already exists.", feedRec.GROUP)	
					end

				end	


				return status

			end


	private function Assoc _GetUserOrGroupRec(Object prgCtx, String ugName, Integer ugType, Integer scope = UAPI.System)

				//We don't want to diddle with UAPI.PowerList - just do SQL. Scope is where to look - all recs or a starting groupID
				String typeStr = (ugType == UAPI.User ? "User" : "Group") 
				Assoc status	 
				RecArray ugRecs = CAPI.Exec(prgCtx.fDBConnect.fConnection, "select * from KUAF where NAME=:A1 and TYPE=:A2 and DELETED=0 and SPACEID=0", \
															ugName,ugType)

				if IsError(ugRecs)

					status.ErrMSg = Str.Format("Error fetching %1 record for %2.", typeStr, ugName)

				elseif !length(ugRecs) 

					status.ErrMSg = Str.Format("%1 '%2' doesn't exist.", typeStr, ugName)

				end

				status.OK = !status.ErrMsg

				if status.ok

					status.ugrec = ugRecs[1]		

				end

				return status

			end


	private function Assoc _RemoveFromGroup(Object prgCtx, Record feedRec)

				Assoc 	result
				Record	ownerGroupRec, addUGRec


				// make user both A USER and GROUP are not specified
				if feedRec.USER && feedRec.GROUP
					result.ErrMsg = Str.Format("Can't specify both a USER and a GROUP for be Added on a single Add operation.")


				// make sure OwnerGroup is defined and exists
				elseif !feedRec.OWNERGROUP 

					result.ErrMsg = Str.Format("no OWNERGROUP specified on Add operation.")

				else

					// see if OWNERGROUP exisits and get its ID

					result = ._GetUserOrGroupRec(prgCtx, feedRec.OWNERGROUP, UAPI.GROUP)

					if !result.ugRec

						result.ErrMsg = Str.Format("OWNERGROUP '%1' does not exist.", feedRec.OWNERGROUP)

					else

						ownerGroupRec = result.ugRec

					end	 

				end

				// now check the GROUP or USER to be added
				if !result.ErrMsg

					if feedRec.USER

						result = ._GetUserOrGroupRec(prgCtx, feedRec.USER, UAPI.USER)

						if !result.ugRec

							result.ErrMsg = Str.Format("USER '%1' does not exist.", feedRec.USER)	

						else

							addUGRec = result.ugRec

						end

					end


					if feedRec.GROUP

						result = ._GetUserOrGroupRec(prgCtx, feedRec.GROUP, UAPI.GROUP)

						if !result.ugRec

							result.ErrMsg = Str.Format("GROUP '%1' does not exist.", feedRec.USER)	

						else

							addUGRec = result.ugRec

						end

					end

				end

				// add the user or group to ownerGroupID membership if all is well

				if !result.ErrMsg && ownerGroupRec && addUGRec

					result = $LLIAPI.UsersPkg.GroupUpdate(\
														prgCtx.USession(),\
														ownerGroupRec.ID,\
														ownerGroupRec.NAME,\
														{},\
														{},\
														{addUGRec.ID},\
														undefined \
														)	







				end	

				return result

			end


	private function Assoc _ValidateFeedRec(Object prgCtx, Record feedRec)

				Assoc status, result

				// make sure we have a user or a group name and not both
				if feedRec.USER && feedRec.GROUP 
					status.ErrMsg = Str.Format("Both USER and GROUP fields were defined.")

				// make sure we have an OWNERGROUP	
				elseif !feedRec.OWNERGROUP 
					status.ErrMsg = Str.Format("OWNERGROUP is not defined.")


				// make sure we have a valid OPERATION code
				elseif !(feedRec.OPERATION in {'A','C','R'}) 
					status.ErrMsg = Str.Format("Invalid OPERAION code.")


				// if this is a C (create) then we must have a GROUP (we don't create USERS)
				elseif feedRec.OPERATION == 'C' && !feedRec.GROUP 
					status.ErrMsg = Str.Format("A Create (C) OPERATION requires a GROUP entry.")

				end

				if !status.ErrMsg

					// make sure the OWNERGROUP exists
					result = ._GetUserOrGroupRec(prgCtx, feedRec.OWNERGROUP, UAPI.GROUP)
					if !result.ok
						status.ErrMsg = Str.Format("OWNERGROUP '%1'doesn't exist.", feedRec.OWNERGROUP)
					end	

				end

				status.OK = !status.ErrMsg

				return status


			end

end
