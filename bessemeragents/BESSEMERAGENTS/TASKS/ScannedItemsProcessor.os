package BESSEMERAGENTS::TASKS

public Object ScannedItemsProcessor inherits BESSEMERAGENTS::TASKS::AgentTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'ScannedItemsProcessor'

	override function Void SubclassExecute( Object prgCtx, Date expireDate )

		.LogInfo("*** Bessemer Scanned Item Processor starting")

		// first get some things we'll need
		// header sheet root folder
		Integer bucketID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'bucket', 0)
		DAPINode headerSheetRootFolder = DAPI.GetNodeById (prgCtx.DAPISess(), 0, bucketID)
		if IsNotError(headerSheetRootFolder)

			// repository
			Integer repositoryID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'repository', 0)
			DAPINode repository = DAPI.GetNodeById (prgCtx.DAPISess(), 0, repositoryID)
			if IsNotError(repository)

				Assoc result = ._IterateHeaderSheets(prgCtx, headerSheetRootFolder, repository)
				if result.OK
					.LogInfo (Str.Format ("*** Bessemer Scanned Item Processor ended"))
				else
					.LogErr(Str.Format ("*** Bessemer Scanned Item Processor aborted with error: %1", result.errMsg))
				end				

			else
				.LogErr("Repository not defined")

			end
		else		
			.LogErr("Header sheets folder not defined")
		end
	end

	private function void _DeleteBucket(DAPINODE bucket)

			// delete the bucket if we emptied it
			.LogInfo(Str.Tab() + Str.Format('Deleting folder %1 [%2]', bucket.pName, bucket.pID))

			if Length(DAPI.ListSubNodes (bucket)) == 0
				Assoc deleted = $LLIAPI.LLNodeSubsystem.GetItem(bucket.pSubtype).NodeDelete(bucket)
				if not deleted.OK
					.LogErr (Str.Format ("Error deleting bucket %1: %2", bucket.pID, deleted.errMsg))
				end
			else
				.LogErr(Str.Format ("Error. Trying to delete bucket that is not empty. Not deleting bucket #%1", bucket.pID))
			end		

		end


	private function Assoc _GetSubaccount (Object prgCtx, Assoc attributesByName, Frame attrData)


			// we have to get the name of the fras attribute set, then the name of the 4th attribute in that set

			String fras = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "fras", undefined)
			if IsUndefined(fras)
				return .Problem ("fras not configured")
			end

			/*
			Assoc result = $BESSEMER.Utils.GetAttributes (prgCtx)
			if not result.OK
				return result
			end
			Assoc attributes = result.attributesByName
			 */

			if not attributesByName.(fras) or length (attributesByName.(fras).children) != 4
				return .Problem ("unexpected fras configuration")
			end

			String subaccount = attributesByName.(fras).children[4]

			List subaccountValues = attrData.GetFromSet (fras, { subaccount } )

			Assoc ok
			ok.ok = true
			ok.subaccount = subaccountValues and length (subaccountValues) ? subaccountValues[1].(subaccount) : Undefined

			return ok
		end


	private function _HandleVault(Object prgCtx, DAPINODE docNode, String bucketName, Assoc attributesByName, Frame attrData)

			.LogInfo(Str.Tab() + Str.Tab() + 'Document is a vault item. Handling physical object stuff.')

			String homeLocation = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'POLocation', '')
			String locatorType = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'LocatorType', '')

			// type of Physical Object to make
			Integer physicalItemTypeID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'physicalItemType', 0)
			DAPINode physicalItemType = DAPI.GetNodeById (prgCtx.DAPISess(), 0, physicalItemTypeID)
			if IsError(physicalItemType)
				return .Problem ("physical item type not defined")
			elseif Type (physicalItemType.pExtendedData) != Assoc.AssocType
				return .Problem ("invalid physical item type")	
			end

			Integer physObjSubType

			if IsFeature (physicalItemType.pExtendedData,'Box') and physicalItemType.pExtendedData.Box
				physObjSubType = 424
			elseif IsFeature (physicalItemType.pExtendedData ,'Container') and physicalItemType.pExtendedData.Container
				physObjSubType =  412
			else
				physObjSubType = 411
			end


			// where to make Physical Object
			Integer physicalItemsFolderID = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'physicalItemsFolder', 0)
			DAPINode physicalItemsFolder = DAPI.GetNodeById (prgCtx.DAPISess(), 0, physicalItemsFolderID)
			if IsError(physicalItemsFolder)
				return .Problem ("physical items folder not defined")
			end		

			// create Physical Object

			Assoc nodeInfo

			nodeInfo.request = Assoc.CreateAssoc()
			nodeInfo.request.poLocation = homeLocation
			nodeInfo.request.selectedMedia = physicalItemTypeID
			nodeInfo.request.LocatorType = locatorType


			// get barcode for uniqueID.  in: "Name [300001234]" it's "1234"

			Integer left = Str.RChr (bucketName, "[")
			Integer right = Str.RChr (bucketName, "]")

			if left and right and right - left > 1
				nodeInfo.request.pouniqueid = bucketName[left + 1 : right - 1]
			end

			// get subaccount for a po property
			Assoc result = ._GetSubaccount (prgCtx, attributesByName, attrData)
			if not result.OK
				return result
			end

			String subaccount = result.subaccount
			if subaccount
				String property = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', "subaccountProperty")
				if property
					nodeInfo.request.("mtField_" + property) = subaccount
				end
			end

			// Allocate a physical object
			Object llnode = $LLIAPI.LLNodeSubsystem.GetItem (physObjSubType)
			result = llnode.NodeAlloc (physicalItemsFolder, bucketName)
			if not result.OK
				return .Problem ("error allocating physical item", result.errMsg)
			end

			// Creat the physical object
			.LogInfo(Str.Tab() + Str.Tab() + 'Creating the physical object')
			result = llnode.NodeCreate (result.node, physicalItemsFolder, nodeInfo)
			if not result.OK
				return .Problem ("error creating physical item", result.errMsg)
			end

			// resulting node	
			DAPINode PONode = result.node

			// create cross reference
			Assoc xRefInfo
			xRefInfo.Node = docNode
			xRefInfo.xNode = PONode
			.LogInfo(Str.Tab() + Str.Tab() + 'Creating the XReference.')
			result = $RECMAN.RIMSUtils.CreateXReference (prgCtx,xRefInfo)
			if not result.OK
				return result
			end

			// set permissions same as document
			.LogInfo(Str.Tab() + Str.Tab() + 'Setting permissions on the Physical Object (same as document)')
			Record nodeRight
			for nodeRight in DAPI.GetNodeRights (PONode)
				if IsError (DAPI.DeleteNodeRight (PONode, nodeRight.RightID))
					return .Problem (Str.Format ("error removing right %1 from node %2", nodeRight.RightID, PONode.pID))
				end
			end

			for nodeRight in DAPI.GetNodeRights (docNode)
				if IsError (DAPI.AddNodeRight (PONode, nodeRight.RightID, nodeRight.Permissions))
					return .Problem (Str.Format ("error adding right to node %1", PONode.pID))
				end
			end		

			.LogInfo(Str.Tab() + Str.Tab() + 'Finished with Vault handling')
			return .Success()
		end


	private function _IterateHeaderSheets(Object prgCtx, DAPINODE headerSheetRootFolder, DAPINODE repository)

			// look into each bucket
			DAPINode bucket
			for bucket in DAPI.ListSubNodes (headerSheetRootFolder)

				.LogInfo('--------')
				.LogInfo(Str.Tab() + Str.Format('Starting Processing of folder %1 [%2]', bucket.pName, bucket.pID))			

				if bucket.pChildCount > 0 

					if Str.Upper(bucket.pName) != "NO FOLDER" // special case

						// Wrap this whole thing in a transaction
						prgCtx.fDbConnect.StartTrans()

							if ._ProcessBucket(prgCtx, bucket, repository)
								.LogInfo(Str.Tab() + 'Folder processed successfully. Will now attempt to delete.')
								._DeleteBucket(bucket)

							else
								.LogErr(Str.Tab() + 'Skipping folder deletion because not all documents were processed.')
							end				

						// Wrap this whole thing in a transaction
						prgCtx.fDbConnect.EndTrans(true)

					else 	
						.LogInfo(Str.Tab() + 'Skipping NO FOLDER')						
					end 

				elseif bucket.pCreateDate < Date.Now() - ( 365 * 86400 ) 				

					.LogInfo(Str.Tab() + Str.Format('No items in folder %1 [%2], and it is over 1 year old. Deleting.', bucket.pName, bucket.pID))

					// ignore empty folders (not yet scanned into) - (LTF Syntergy) unless six months old, then delete
					// KM 6-3-20. Changed this to 365 days as per specification from Lynn and Lenny
		        	// if bucket.pCreateDate < Date.Now()-(365/2*86400)

					._DeleteBucket(bucket)

				else
					.LogInfo(Str.Tab() + Str.Format('No items in folder %1 [%2], but it is too new to delete.', bucket.pName, bucket.pID))		
				end	


				.LogInfo(Str.Tab() + Str.Format('Finished Processing of folder %1 [%2]', bucket.pName, bucket.pID))
				.LogInfo('--------')
				.LogInfo('')

			end

			return .Success()

		end


	private function Boolean _ProcessBucket(Object prgCtx, DAPINODE bucket, DAPINODE repository)

			.LogInfo(Str.Tab() + Str.Format('Folder has items %1 documents', bucket.pChildCount))

			// change owner to Admin, just to shield these folders from the creating users
			if bucket.pUserID != 1000 and Date.Now() - bucket.pCreatedate > 10 * 60  // at least 10 minutes being created
				.LogInfo(Str.Tab() + 'Making Admin the owner of this folder')
				bucket.pUserID = 1000
				DAPI.UpdateNode (bucket)
			end

			DAPINode scannedItem
			for scannedItem in DAPI.ListSubNodes (bucket)

				.LogInfo(Str.Tab() + Str.Tab() + Str.Format('Processing scanned item %1 [%2]', scannedItem.pName, scannedItem.pID))

				// only pdf's
				DAPIVersion version = DAPI.GetVersion (scannedItem, 0)
				if IsError(version) or version.pMIMEType != 'application/pdf'
					.LogWarn(Str.Tab() + Str.Tab() + Str.Format ("Ignoring document #%1", scannedItem.pID))

				else

					// process the item
					Assoc result = ._ProcessItem (prgCtx, scannedItem, bucket.pName, repository)
					if not result.ok 
						.LogErr(result.errMsg)
					end					
				end

			end	

			// a successfully processed bucket will be empty
			Boolean isEmpty = Length(DAPI.ListSubNodes(bucket)) == 0

			.LogInfo(Str.Tab() + 'Finished Processing of scanned items')
			if isEmpty			
				.LogInfo(Str.Tab() + "Folder is now empty")
			else
				.LogErr(Str.Tab() + "But folder is not empty. Some item(s) failed")
			end		

			return isEmpty
		end


	private function Assoc _ProcessItem ( Object prgCtx, DAPINode docNode, String bucketName, DAPINode repository)

			// move from bucket to repository
			.LogInfo(Str.Tab() + Str.Tab() + Str.Format('Moving Scanned Item to repository and renaming to %1', bucketName))
			Assoc result = $LLIAPI.LLNodeSubsystem.GetItem (docNode.pSubtype).NodeMove(docNode, repository, bucketName)
			if not result.OK
				return result
			end

			// cats atts values

			Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, docNode.pID, docNode.pVersionNum)
			if not attrData.Load()
				return .Problem (Str.Format ("Error loading attributes for node %1", docNode.pID))
			end

			// defined attributes by name
			Assoc attributesByName
			Assoc status = $BESSEMER.AttributesConfig.GetAttributes (prgCtx, true)
			if !status.OK
				return .Problem("Error loading attributes configuration.")
			end

			attributesByName = status.attributesByName		

			// remove inherited permissions
			.LogInfo(Str.Tab() + Str.Tab() + 'Now Removing inherited perms.')
			status = $BESSEMER.Utils.RemoveACLs(docNode)
			if !status.ok
				return status		
			end
			
			// set appropriate permissions in repository
			.LogInfo(Str.Tab() + Str.Tab() + 'Now updating permissions.') 
			result = $BESSEMER.DocPermissions.UpdatePermissions (prgCtx, docNode, attributesByName, attrData)
			if not result.OK
				return result
			end		

			// only Vault items do all the PO stuff
			if attrData.Get("Vault") and attrData.Get("Vault")[1]

				Assoc vaultStatus = ._HandleVault(prgCtx, docNode, bucketName, attributesByName, attrData)	
				if !vaultStatus.ok
					return vaultStatus
				end
			end	

			// success
			.LogInfo(Str.Tab() + Str.Tab() + Str.Format ("Finished processing document %1 [%2]", docNode.pName, docNode.pID))	

			return .Success()

		end

end
