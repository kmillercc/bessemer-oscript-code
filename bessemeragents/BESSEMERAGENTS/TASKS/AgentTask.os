package BESSEMERAGENTS::TASKS

public Object AgentTask inherits BESSEMERAGENTS::Root

	public		Dynamic	fCacheObject
	public		Boolean	fEnabled = FALSE
	public		Dynamic	fExpireDateTime
	public		String	fName = 'NotSet'



	public function Assoc Execute( Object prgCtx, Dynamic params, Date expireDate)

				String 	thisName = .fName

				if ._IsTimeToStop(expireDate)

					.LogErr(Str.Format("*** %1 should not be running at this time. Time is now %2.", Date.DateToString(Date.Now(), '%H:%M'), thisName))			

				else

					// *** call the init function
					.PreExecuteInit(prgCtx)

					// let subclass do its thing
					.SubclassExecute(prgCtx, expireDate)		
				end

				return Assoc{'ok': true}

			end


	public function Void LogDebug( String s )
					EchoDebug( s )
				end


	public function Void LogErr( String s )
					EchoError( s )
				end


	public function Void LogInfo( String s )
					EchoInfo( s )
				end


	public function Void LogWarn( String s )
					EchoWarn( s )
				end


	public function Object New()

					Object o = OS.NewTemp( this )

					o.fCacheObject = $BessemerAgents.Cache

					return o

				end


	public function void PreExecuteInit( Dynamic prgCtx )


				end


	public function Void SubclassExecute(Object prgCtx, Date expireDate)


				end


	private function Boolean _IsTimeToStop(Date expireDate = .fExpireDateTime)

					if Date.Now() > expireDate
						return TRUE
					else
						return FALSE	
					end

					/*
					Date 	now = Date.Now()
					String 	day = Date.DateToString(now, '%A')
					String	hour = Date.DateToString(now, '%H')
					List 	defaultVal = {'00','01','02','03','04','05','21','22','23','24'}
					Boolean	doStop = false		

					Dynamic	hours = $BessemerAgents.Prefs.GetIniPref('bessemeragents', 'AgentHours', day, true, "list", defaultVal)

					// test the return value just in case there was an error in the ini file and we didn't get back a proper list
					if IsUndefined(hours) || Type(hours) != ListType
						hours = defaultVal
					end

					if (hour in hours) == 0
						doStop = TRUE		
					end

					return doStop
					*/
				end


	public function __Init()
					if .fEnabled
						$BessemerAgents.AgentTaskRegistry.Register( .fName, OS.NewTemp( this ) )
					end
				End

end
