package BESSEMERAGENTS::TASKS

public Object UGSync inherits BESSEMERAGENTS::TASKS::AgentTask

	public		Dynamic	fDbCursor
	override	Boolean	fEnabled = TRUE
	public		Assoc	fLLNodeCache
	override	String	fName = 'UGSync'
	public		Integer	fNumberToKeep = 1000
	public		String	fQuery = 'select * from %1 where COMPLETEDATE is null order by SEQUENCE'
	public		Dynamic	fRecs
	public		List	fSubs
	public		String	fTableName = 'BESSEMER_GROUPS_FEED'
	public		Dynamic	fUsersPkg



	override function Object New()

					Object o = OS.NewTemp( this )
					o.fCacheObject = $BessemerAgents.Cache
					o.fUsersPkg = $LLIAPI.UsersPkg

					return o
				end


	public function Assoc ProcessRow( Object prgCtx, String operation, String userName, String groupName, String ownerGroupName)

					switch operation

						// Create group
						case 'C'

							return ._OperationCreateGroup(prgCtx, groupName)

						end	   

						// Delete group
						case 'D'

							return ._OperationDeleteGroup(prgCtx, groupName)

						end												 

						// Add to group
						case 'A'

							return ._OperationAdd(prgCtx, ownerGroupName, groupName, userName)

						end

						// Remove from group	
						case 'R'

							return ._OperationRemove(prgCtx, ownerGroupName, groupName, userName)

						end


						default
							return .Problem(Str.Format("Invalid OPERATION code of '%1'", operation))
						end

					end								   

					return Assoc{'ok': true}
				end


	public function Void ProcessRows(Object prgCtx, Date expireDate)

						Assoc status

						// get the total
						RecArray recs = .fRecs		
						Boolean	ok

						// iterate through the records
						Record rec
						for rec in recs
							Integer startTicks = Date.Tick()

							String issueMsg = undefined
							ok = TRUE

							prgCtx.fDbConnect.StartTrans()

								Echo(Str.Format(Str.Tab() + "Processing row [%1]", rec))

								status = .ProcessRow(prgCtx, rec.OPERATION, rec.USER, rec.GROUP, rec.OWNERGROUP)
								if !status.ok
									issueMsg = status.errMsg
									ok = false
								end				

							prgCtx.fDbConnect.EndTrans(ok)

							if IsDefined(issueMsg)
								EchoError(Str.Format(Str.Tab() + Str.Tab() + "%1", issueMsg))
							end		

							Integer diffTicks = Date.Tick() - startTicks		
							Real 	seconds = ( diffTicks / 1000.0 )

							// update the row with success or complete
							._UpdateRow(prgCtx, rec.SEQUENCE, issueMsg, seconds)
						end

					end


	override function Void SubclassExecute(Object prgCtx, Date expireDate)

						String 	thisName = .fName

						// set the expire date
						.fExpireDateTime = expireDate

						// make sure we have our cache of groupIDs
						$Bessemeragents.Cache.BuildCache(prgCtx)

						Echo(Str.Format("*** %1 starting", thisName))

						Assoc status = ._GetNewCursor(prgCtx)
						if status.ok

							while (._GetRows() > 0)
								// *** query the table to see if there are any records
								RecArray	recs = .fRecs

								Integer numRows = Length(recs)

								// Start our timer
								$SyntergyCore.Stats.Start( "UGSync", thisName )

								.ProcessRows(prgCtx, expireDate)

								//Echo( Str.Format("*** %1 stopped", thisName))
								Real elapsed = $SyntergyCore.Stats.Stop( "UGSync", thisName )

								Echo( Str.Format("*** Timing: %1 processed %2 rows in %3 seconds at %4 rows per second.", thisName, numRows, elapsed, (numRows/elapsed) ))

							end

							Echo(Str.Format("*** %1 stopped. No more rows to process.", thisName))

							// Make sure we delete cursor			
							.fDbCursor.Delete()
						else	
							.LogErr(status.errMsg)
						end


					end


	private function Integer _GetGroup(Object prgCtx, String groupName)

			/*
			Integer ugType = UAPI.GROUP
			String stmt = "select ID from KUAF where NAME=:A1 and TYPE=:A2 and DELETED=0 and SPACEID=0"

			RecArray ugRecs = CAPI.Exec(prgCtx.fDBConnect.fConnection, 
										stmt, \
										groupName,
										ugType)

			if Length(ugRecs)
				return ugRecs[1].ID
			end
			*/

			Assoc status = .fCacheObject.GetGroupID(prgCtx, groupName)

			if status.ok
				return status.result
			end

			return undefined							

		end


	private function Assoc _GetNewCursor(Object prgCtx)

						String query = 	.fQuery

						// get the stmt
						String  stmt = Str.Format(query, .fTableName)

						Object	cursor = $LLIApi.DbCursor.New(prgCtx.fDbConnect, \
															stmt, \
															.fSubs, \
															SQL.CACHE | 10, \
															.fNumberToKeep )


						if ( IsNotError( cursor ) )
							.fDbCursor = cursor
						else
							return .Problem("SQL Error. Could not get cursor.")	
						end				

						return .Success()

					end


	private function Integer _GetRows()

						if ._IsTimeToStop()
							Echo( "*** Need to stop. Task should not be running at this time. Time is now ", Date.DateToString(Date.Now(), '%H:%M'))
							return 0
						end	

						Object cursor = .fDbCursor

						Integer	numRowsFetched = cursor.LoadResultSet() 


						if numRowsFetched > 0

							.fRecs = cursor.GetResultSetCopy()

						end		

						return numRowsFetched

					end


	private function Integer _GetUser(Object prgCtx, String userName)

						/*		
						Integer ugType = UAPI.USER
						String stmt = "select ID from KUAF where NAME=:A1 and TYPE=:A2 and DELETED=0 and SPACEID=0"

						RecArray ugRecs = CAPI.Exec(prgCtx.fDBConnect.fConnection, 
													stmt, \
													userName,
													ugType)

						if Length(ugRecs)
							return ugRecs[1].ID
						end

						return undefined	
						 */					

						Assoc status = .fCacheObject.GetUserID(prgCtx, userName)

						if status.ok
							return status.result
						end

						return undefined			 

					end


	private function Assoc _GetUserOrGroupID(Object prgCtx, String addOrRemove, String groupName, String userName)

					Integer childID

					if IsDefined(userName)
						childID = ._GetUser(prgCtx, userName)

						if IsUndefined(childID)
							return .Problem(Str.Format("Can not %1 user. Specified USER '%2' does not exist.", addOrRemove, userName))
						end	   

					elseif IsDefined(groupName)

						childID = ._GetGroup(prgCtx, groupName)

						if IsUndefined(childID)
							return .Problem(Str.Format("Can not %1 group. Specified Child GROUP '%2' does not exist.", addOrRemove, groupName))
						end	   

					else

						return .Problem(Str.Format("No Child USER or GROUP specified for %1 operation.", addOrRemove))					  

					end

					return Assoc{'ok': true, 'result': childID}
				end


	private function Assoc _OperationAdd(Object prgCtx, String ownerGroupName, String groupName, String userName)	

					Integer parentGroupID

					if IsDefined(ownerGroupName)

						parentGroupID = ._GetGroup(prgCtx, ownerGroupName)

						if IsUndefined(parentGroupID)
							return .Problem(Str.Format("GROUP '%1' does not exist.", ownerGroupName))
						end

						Assoc status = ._GetUserOrGroupID(prgCtx, "add", groupName, userName)	

						if status.ok

							Object  uapiCtx = prgCtx.USession()
							Integer childID = status.result
							return .fUsersPkg.GroupUpdate(uapiCtx, parentGroupID, undefined, {}, {childID}, {}, undefined)

						else	
							return status
						end

					else

						return .Problem("Missing OWNERGROUP for Add operation")

					end 


					return Assoc{'ok': true}
				end


	private function Assoc _OperationCreateGroup(Object prgCtx, String groupName)

					Integer groupID

					if IsDefined(groupName)

						groupID = ._GetGroup(prgCtx, groupName)

						if IsDefined(groupID)
							return .Problem(Str.Format("GROUP '%1' already exists.", groupName))

						else

							Object  uapiCtx = prgCtx.USession()

							return .fUsersPkg.GroupNew(uapiCtx, groupName)		

						end		

					else

						return .Problem("Missing GROUP for Create operation")		

					end


					return Assoc{'ok': true}
				end


	private function Assoc _OperationDeleteGroup(Object prgCtx, String groupName)

					Integer groupID

					if IsDefined(groupName)

						groupID = ._GetGroup(prgCtx, groupName)

						if IsDefined(groupID)

							Object  uapiCtx = prgCtx.USession()

							return .fUsersPkg.GroupDelete(uapiCtx, groupID)		  

						else
							return .Problem(Str.Format("GROUP '%1' does not exist.", groupName))

						end		
					else

						return .Problem("Missing GROUP for Delete operation")		

					end


					return Assoc{'ok': true}
				end


	private function Assoc _OperationRemove(Object prgCtx, String ownerGroupName, String groupName, String userName)	

			Integer parentGroupID

			if IsDefined(ownerGroupName)

				parentGroupID = ._GetGroup(prgCtx, ownerGroupName)

				if IsUndefined(parentGroupID)
					return .Problem(Str.Format("GROUP '%1' does not exist.", ownerGroupName))
				end

				Assoc status = ._GetUserOrGroupID(prgCtx, "remove", groupName, userName)

				if status.ok

					Object  uapiCtx = prgCtx.USession()
					Integer childID = status.result
					return .fUsersPkg.GroupUpdate(uapiCtx, parentGroupID, undefined, {}, {}, {childID}, undefined)

				else	
					return status
				end

			else

				return .Problem("Missing OWNERGROUP for Add operation")

			end 


					return Assoc{'ok': true}

				end


	private function Void _UpdateRow( Object prgCtx, Integer id, String issueMsg, Real seconds )

						// Update the record
						prgCtx.fDbConnect.StartTrans()

							String updateStmt = Str.Format('UPDATE %1 SET COMPLETEDATE=:A1, ISSUEMESSAGE=:A2 where "SEQUENCE"=:A3', .fTableName )
							Dynamic updateResult = CAPI.EXEC(prgCtx.fDBConnect.fConnection,\ 
														updateStmt, \
														Date.Now(), \
														issueMsg, \
														id )
							if IsError( updateResult )
								.LogErr( Str.Format( Str.Tab() + "Error updating %1 table.", .fTableName ) )
							end

						// commit
						prgCtx.fDbConnect.EndTrans( TRUE )

					end

end
