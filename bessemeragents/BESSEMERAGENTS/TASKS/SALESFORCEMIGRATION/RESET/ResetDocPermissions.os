package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::RESET

public Object ResetDocPermissions inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::RESET::ResetPermsTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'ResetDocPermissions'
	override	String	fTableName = 'BESS_SF_MIGRATE_DOCS'

end
