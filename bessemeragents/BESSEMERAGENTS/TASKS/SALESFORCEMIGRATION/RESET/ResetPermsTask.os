package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::RESET

public Object ResetPermsTask inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::MigrationTask

	override	String	fQuery = 'select * from %1 where STATUS=:A1 and STAGED=:A2 ORDER BY ID'
	override	List	fSubs = { 'COMPLETED', 'Y' }



	override function Assoc ProcessRow( Object prgCtx, Record rec, DAPINODE node )

				Assoc 	status

				// add change owner group to our changeList
				List	changeList
				status = ._GetOwnerChange( prgCtx, node, rec )
				if !status.ok
					return status
				else
					changeList = status.result
				end				

				// get the assigned groups from bess_sf_old_mnodel_perms table
				status = ._GetOldAcls( prgCtx, node )
				if !status.ok
					return status
				else
					changeList = List.SetUnion(changeList, status.result)
				end

				Assoc options = Assoc{'clearACLs': true }

				// make the call to NodeRightsUpdate
				Object llNode = IsFeature(.fLLNodeCache, node.pSubType) ?  .fLLNodeCache.(node.pSubType) : $LLIAPI.LLNodeSubsystem.GetItem( node.pSubType )
				status = llNode.NodeRightsUpdate( node, changeList, options )
				if !status.ok
					return status
				end		

				return Assoc{'ok': true}

			end


	override function Void UpdateRow( Object prgCtx, Record rec, String issueMsg, String statusStr, Real seconds )

				// Update the record
				prgCtx.fDbConnect.StartTrans()

					// in this case, the status string should be "RESET"
					statusStr = "RESET"

					String updateStmt = Str.Format( "UPDATE %1 SET ISSUEMESSAGE=:A1, STATUS=:A2, STAGED=:A3, SECONDS=:A4 where ID=:A5", .fTableName )
					Dynamic updateResult = CAPI.EXEC( prgCtx.fDBConnect.fConnection, updateStmt, issueMsg, statusStr, "N", seconds, rec.ID )

					if IsError( updateResult )
						.LogErr( Str.Format( Str.Tab() + "Error updating %1 table.", .fTableName ) )
					end

				// commit
				prgCtx.fDbConnect.EndTrans( TRUE )

			end


	private function Assoc _GetOldAcls(Object prgCtx, DAPINODE node)

				List changeList

				// get the records from the table
				String stmt = "select * from BESS_SF_OLD_MODEL_PERMS where DATAID=:A1 and ACLTYPE=:A2"
				RecArray recs = CAPI.Exec ( prgCtx.fDBConnect.fConnection, stmt, node.pID, DAPI.PERMTYPE_ACL)

				if IsError(recs)
					return .Problem(Str.Format('Could not get records from BESS_SF_OLD_MODEL_PERMS for DATAID %1', node.pID))
				end

				Record rec
				for rec in recs

					Assoc changeVals = Assoc.CreateAssoc()
					changeVals.Type = .fUpdateRightAddReplace // $LLIApi.UpdateRightAddReplace
					changeVals.permType = rec.ACLTYPE
					changeVals.RightID = rec.RIGHTID
					changeVals.Permissions = rec.PERMISSIONS

					changeList = {@changeList, changeVals}

				end

				return Assoc{'ok': true, 'result': changeList}
			end


	private function Assoc _GetOwnerChange(Object prgCtx, DAPINODE node, Record rec)

				Integer ownerGroupID

				Assoc status = .fCacheObject.GetGroupID(prgCtx, rec.OLD_OWNER_GROUP)
				if !status.ok
					return status
				else
					ownerGroupID = status.result
				end	

				Assoc changeVals = Assoc.CreateAssoc()
				changeVals.Type = .fUpdateRightReplace // $LLIApi.updateRightReplace
				changeVals.permType = DAPI.PERMTYPE_GROUP
				changeVals.Permissions = rec.NEW_OWNER_GROUP_PERMS
				changeVals.RightID = ownerGroupID

				// our return value
				List	changeList = {changeVals}

				echo(Str.Format(Str.Tab() + "Setting Owner Group to %1", rec.NEW_OWNER_GROUP))

				return Assoc{'ok': true, 'result': changeList}

			end

end
