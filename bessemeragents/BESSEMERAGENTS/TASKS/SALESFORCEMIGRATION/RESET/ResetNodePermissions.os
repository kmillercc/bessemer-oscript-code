package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::RESET

public Object ResetNodePermissions inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::RESET::ResetPermsTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'ResetNodePermissions'
	override	String	fTableName = 'BESS_SF_MIGRATE_NODES'

end
