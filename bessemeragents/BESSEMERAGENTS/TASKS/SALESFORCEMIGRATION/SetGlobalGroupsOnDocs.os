package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION

public Object SetGlobalGroupsOnDocs inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::MigrationTask

	override	Boolean	fEnabled = FALSE
	override	String	fName = 'SetGlobalGroupsOnDocs'
	override	String	fQuery = 'select DATAID from %1 where PARENTID=:A1'
	override	List	fSubs = { 112333 }
	override	String	fTableName = 'DTREECORE'



	override function Assoc ProcessRow( Object prgCtx, Record rec, DAPINODE node )

				Integer permsBitMask = $PSee | $PSeeContents | $PModify | $PEditAtts
				Assoc attributesByName

				Assoc status = $BESSEMER.AttributesConfig.GetAttributes (prgCtx, true)
				if status.OK	
					attributesByName = status.attributesByName
				else
					return status
				end

				Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, node.pID, node.pVersionNum)
				attrData.Load()

				// *** now get the global groups
				status = $BESSEMER.PermissionsConfig.GetGlobalGroups(prgCtx, attributesByName, attrData)
				if !status.ok
					return status
				end

				// now loop through the groups that need to be added
				Assoc group
				for group in status.result
					status = .AddUpdateRight(prgCtx, node, group.Name, group.ID, permsBitMask)
					if !status.ok
						return status
					end			
				end

				return .Success()

			end

end
