package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION

public Object MigrationTask inherits BESSEMERAGENTS::TASKS::AgentTask

	public		Dynamic	fDbCursor
	public		Assoc	fLLNodeCache
	public		Integer	fNumberToKeep = 1000
	public		String	fQuery = 'select * from %1 where STAGED=:A1 and (STATUS IS NULL OR STATUS != ''ERROR'') ORDER BY ID'
	public		Dynamic	fRecs
	public		List	fSubs = { 'Y' }
	public		String	fTableName
	public		String	fType = 'NotSet'
	public		Integer	fUpdateRightAddReplace
	public		Integer	fUpdateRightRemove
	public		Integer	fUpdateRightReplace



	public function Assoc AddUpdateRight(Object prgCtx, DAPINODE node, String groupName, Integer groupID, Integer permissions)

				// *** does this node already have this user/group in the ACL
				String	stmt = 'select PERMISSIONS from DTreeACL where DataID=:A1 and RightID=:A2 and ACLType=:A3'
				RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pId, groupID, DAPI.PERMTYPE_ACL)

				if Length(recs) == 0
					if IsError(DAPI.AddNodeRight (node, groupID, permissions))
						return .Problem("Unable to add node right %1 to node %2", groupID, node.pID)
					else
						EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Added Group %1 to ACL with permissions %2", groupName, permissions))
					end
				else
					Integer oldPerms = recs[1].PERMISSIONS

					if oldPerms != permissions
						if IsError(DAPI.UpdateNodeRight(node, groupID, permissions))
							return .Problem("Unable to update node right %1 on node %2", groupID, node.pID)
						else
							EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Updated %1's permissions to %2", groupName, permissions))
						end					
					end

				end

				return Assoc{'ok': true}
			end

	/*  table names are BESS_SF_MIGRATE_DOCS
					and BESS_SF_MIGRATE_NODES

					columns are

					ID
					DATAID
					OWNER_GROUP
					OWNER_GROUP_PERMS
					NEW_ASSIGNED_GROUPS
					OFFICE_CODES
					ADDDATE
					COMPLETEDATE
					ISSUEMESSAGE
					STAGED

				*/
	public function Assoc GetNewCursor(Object prgCtx)

					String query = 	.fQuery

					// get the stmt
					String  stmt = Str.Format(query, .fTableName)

					Object	cursor = $LLIApi.DbCursor.New(prgCtx.fDbConnect, \
														stmt, \
														.fSubs, \
														SQL.CACHE | 10, \
														.fNumberToKeep )


					if ( IsNotError( cursor ) )
						.fDbCursor = cursor
					else
						return .Problem("SQL Error. Could not get cursor.")	
					end				

					return .Success()

				end


	override function Object New()

				Object o = OS.NewTemp( this )

				o.fCacheObject = $BessemerAgents.Cache

				Assoc	llNodeCache
				llNodeCache.($TypeDocument) = $LLIAPI.LLNodeSubsystem.GetItem( $TypeDocument )
				llNodeCache.($TypeVirtualFolder) = $LLIAPI.LLNodeSubsystem.GetItem( $TypeVirtualFolder )


				.fUpdateRightRemove = $LLIApi.UpdateRightRemove
				.fUpdateRightReplace = $LLIApi.updateRightReplace
				.fUpdateRightAddReplace = $LLIApi.UpdateRightAddReplace

				o.fLLNodeCache = llNodeCache

				return o

			end


	override function void PreExecuteInit( Dynamic prgCtx )

					// abstract - override in subclasses

				end


	public function Assoc ProcessRow(Object prgCtx, Record rec, DAPINODE node)

					// abstract -- implement in subclasses

					return .Problem("not implemented")	

				end


	public function Void ProcessRows(Object prgCtx, Date expireDate)

					Assoc status
					Record rec
					String issueMsg, statusStr

					// get the total
					RecArray recs = .fRecs		
					Boolean	ok

					// iterate through the records
					for rec in recs
						Integer startTicks = Date.Tick()

						statusStr = 'COMPLETED'

						issueMsg = undefined
						ok = TRUE

						// get the dapinode
						DAPINODE node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.DATAID)

						if IsError(node)
							issueMsg = Str.Format("Error retrieving DAPINODE from id %1", rec.DATAID)
						else

							prgCtx.fDbConnect.StartTrans()

								Echo(Str.Format(Str.Tab() + "Modifying perms on node %1 [%2]", node.pName, node.pID))

								status = .ProcessRow(prgCtx, rec, node)
								if !status.ok
									statusStr = 'ERROR'
									issueMsg = status.errMsg
									ok = false
								elseif IsDefined(status.statusStr)
									statusStr	= status.statusStr
								end				

							prgCtx.fDbConnect.EndTrans(ok)

						end	

						if IsDefined(issueMsg)
							echo(Str.Format(Str.Tab() + "%1 [%2] : %3", rec.NAME, rec.ID, issueMsg))
						end		

						Integer diffTicks = Date.Tick() - startTicks		
						Real 	seconds = ( diffTicks / 1000.0 )

						// update the row with success or complete
						.UpdateRow(prgCtx, rec, issueMsg, statusStr, seconds)
					end

				end


	override function Void SubclassExecute(Object prgCtx, Date expireDate)

					String 	thisName = .fName

					// set the expire date
					.fExpireDateTime = expireDate

					// make sure we have our cache of groupIDs
					$Bessemeragents.Cache.BuildCache(prgCtx)

					Echo(Str.Format("*** %1 starting", thisName))

					Assoc status = .GetNewCursor(prgCtx)
					if status.ok

						while (._GetRows() > 0)
							// *** query the table to see if there are any records
							RecArray	recs = .fRecs

							Integer numRows = Length(recs)

							// Start our timer
							$SyntergyCore.Stats.Start( "SalesForceMigration", thisName )

							.ProcessRows(prgCtx, expireDate)

							//Echo( Str.Format("*** %1 stopped", thisName))
							Real elapsed = $SyntergyCore.Stats.Stop( "SalesForceMigration", thisName )

							Echo( Str.Format("*** Timing: %1 processed %2 rows in %3 seconds at %4 rows per second.", thisName, numRows, elapsed, (numRows/elapsed) ))

						end

						Echo(Str.Format("*** %1 stopped. No more rows to process.", thisName))

						// Make sure we delete cursor			
						.fDbCursor.Delete()
					else	
						.LogErr(status.errMsg)
					end



				end


	public function void UpdateRow(Object prgCtx, Record rec, String issueMsg, String statusStr, Real seconds)

					// override in subclasses

				end


	private function Integer _GetRows()

					if ._IsTimeToStop()
						Echo( "*** Need to stop. Task should not be running at this time. Time is now ", Date.DateToString(Date.Now(), '%H:%M'))
						return 0
					end	

					Object cursor = .fDbCursor

					Integer	numRowsFetched = cursor.LoadResultSet() 


					if numRowsFetched > 0

						.fRecs = cursor.GetResultSetCopy()

					end		

					return numRowsFetched

				end

end
