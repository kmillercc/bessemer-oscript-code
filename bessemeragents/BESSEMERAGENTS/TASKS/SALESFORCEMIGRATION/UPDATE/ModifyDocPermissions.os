package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::UPDATE

public Object ModifyDocPermissions inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::UPDATE::UpdatePermsTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'ModifyDocPermissions'
	override	String	fTableName = 'BESS_SF_MIGRATE_DOCS'



	override function Assoc GetFSGroups(Object prgCtx, DAPINODE node)

	           Integer permsBitMask = $PSee | $PSeeContents | $PModify | $PEditAtts
	           List    groupNames
	           Assoc    attributesByName
	           List    changeList

	           Assoc status = $BESSEMER.AttributesConfig.GetAttributes(prgCtx, true)
	           if status.OK    
	             attributesByName = status.attributesByName
	           else
	             return status
	           end

	           Frame attrData = $BESSEMER.CategoryAttributes.New(prgCtx, node.pID, node.pVersionNum)
	           attrData.Load()

	           // get any families that the doc may be filed at
	           status = $Bessemer.DocPermissions.GetDocFSGroups(prgCtx, node, attributesByName, attrData, true)
	           if status.OK    
	             groupNames = status.result
	           else
	             return status
	           end       

	           String    groupName
	           Assoc    changeVals
	           for groupName in groupNames
	             changeVals = Assoc.CreateAssoc()
	             changeVals.Type = .fUpdateRightAddReplace //$LLIApi.UpdateRightAdd      // resolves to 2, which means to Add to ACL
	             changeVals.permType = DAPI.PERMTYPE_ACL
	             changeVals.groupName = groupName
	             changeVals.Permissions = permsBitMask
	             // get the groupID
	             status = .fCacheObject.GetGroupID(prgCtx, groupName)
	             if !status.ok
	                   return status
	             else
	                   changeVals.RightID = status.result
	             end                  

	             // add to the changeList we will pass to llNode.NodeRightsUpdate     
	             changeList = {@changeList, Assoc.Copy(changeVals)}
	           end
	           return .Success(changeList)

	      end


	override function Assoc SubclassPostExecute( Object prgCtx, Record rec, DAPINODE node )

	           if IsFeature(rec, 'DO_GLOBAL') && rec.DO_GLOBAL == 'Y'

	             Integer permsBitMask = $PSee | $PSeeContents | $PModify | $PEditAtts
	             Assoc attributesByName

	             Assoc status = $BESSEMER.AttributesConfig.GetAttributes (prgCtx, true)
	             if status.OK    
	                   attributesByName = status.attributesByName
	             else
	                   return status
	             end

	             Frame attrData = $BESSEMER.CategoryAttributes.New (prgCtx, node.pID, node.pVersionNum)
	             attrData.Load()

	             // *** now get the global groups
	             status = $BESSEMER.PermissionsConfig.GetGlobalGroups(prgCtx, attributesByName, attrData)
	             if !status.ok
	                   return status
	             end

	             // now loop through the groups that need to be added
	             Assoc group
	             for group in status.result
	                   status = .AddUpdateRight(prgCtx, node, group.Name, group.ID, permsBitMask)
	                   if !status.ok
	                     return status
	                   end           
	             end
	           end       

	           return .Success()
	      end

end
