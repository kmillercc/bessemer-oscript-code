package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::UPDATE

public Object UpdatePermsTask inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::MigrationTask

	public		Integer	fGlobalGroupID
	public		String	fGlobalGroupName = 'G-Dorado-Global-Group'
	override	String	fQuery = 'select * from %1 where STAGED=:A1 and (STATUS IS NULL OR STATUS != :A2) ORDER BY ID'
	override	List	fSubs = { 'Y', 'ERROR' }



	public function Assoc GetFSGroups(Object prgCtx, DAPINODE node)

	           List emptyList = {}

	           return .Success(emptyList)

	      end


	override function void PreExecuteInit( Dynamic prgCtx )

	             // make the G-Dorado-Global-Group the owner group
	             String groupName = CAPI.IniGet (prgCtx.fDbConnect.fLogin, 'BESSEMER', 'globalGroupName', .fGlobalGroupName)

	             if groupName != .fGlobalGroupName
	                   .fGlobalGroupName = groupName
	             end
	             // Look for this group in KUAF
	             RecArray recs = CAPI.Exec (prgCtx.fDBConnect.fConnection, "select ID from KUAF where NAME=:A1 and Type=:A2", groupName, UAPI.Group)
	             if IsNotError(recs) &&  Length(recs) == 1         
	                   .fGlobalGroupID = recs[1].ID
	             end

	           end


	override function Assoc ProcessRow( Object prgCtx, Record rec, DAPINODE node )

	           Assoc    status

	           // first save the existing perms to the BESS_SF_OLD_MODEL_PERMS table
	           ._SaveExistingPerms( prgCtx, node )

	           // add change owner group to our changeList
	           List    changeList
	           status = ._GetOwnerChange( prgCtx, node, rec.NEW_OWNER_GROUP, rec.NEW_OWNER_GROUP_PERMS )
	           if !status.ok
	             return status
	           else
	             changeList = status.result
	           end         

	           // get the non-global groups that we will remove
	           status = ._GetGroupsToRemove(prgCtx, node)
	           if !status.ok
	             return status
	           else
	             changeList = {@changeList, @status.result}
	           end                   

	           // add new RS groups
	           if IsDefined( rec.NEW_ASSIGNED_GROUPS )
	             status = ._ParseNewAssignedGroups( prgCtx, rec.NEW_ASSIGNED_GROUPS )
	             if !status.ok
	                   return status
	             else
	                   changeList = {@changeList, @status.result}
	             end
	           end

	           // look for FS groups
	           status = .GetFSGroups(prgCtx, node) 
	           if !status.ok
	             return status
	           else
	             changeList = {@changeList, @status.result}
	           end           

	           // Assoc options
	           /* llNode function is too slow
	           // make the call to NodeRightsUpdate
	           Object llNode = IsFeature(.fLLNodeCache, node.pSubType) ?  .fLLNodeCache.(node.pSubType) : $LLIAPI.LLNodeSubsystem.GetItem( node.pSubType )
	           status = llNode.NodeRightsUpdate( node, changeList, options )
	           if !status.ok
	             return status
	           end         
	           */

	        // Use our own function which should be much faster
	           status = ._ProcessChangeList(prgCtx, node, changeList)
	           if !status.ok
	            return status
	           end

	           // apply same permissions to physical object, if any
	           RecArray xRecs = CAPI.Exec (prgCtx.fDBConnect.fConnection, "select XDataID from RM_DocXRef where DataID = :A1", node.pID)
	           if IsNotError(xRecs) and Length (xRecs) > 0
	             DAPINode PONode = DAPI.GetNodeByID (prgCtx.DAPISess(), 0, xRecs[1].XDataID)
	             if IsNotError(PONode) 
	                   status = ._ProcessChangeList(prgCtx, PONode, changeList)
	                 if !status.ok
	                   return status
	                 end
	             end
	           end       

	           // do anything else in subclasses
	           status = .SubclassPostExecute(prgCtx, rec, node)
	           if !status.ok
	             return status
	           end

	           return Assoc{'ok': true}

	      end


	public function Assoc SubclassPostExecute(Object prgCtx, Record rec, DAPINODE node)

	           return Assoc{'ok': true}

	      end


	override function Void UpdateRow( Object prgCtx, Record rec, String issueMsg, String statusStr, Real seconds )

	             // Update the record
	             prgCtx.fDbConnect.StartTrans()

	                   String updateStmt = Str.Format("UPDATE %1 SET COMPLETEDATE=:A1, ISSUEMESSAGE=:A2, STATUS=:A3, STAGED=:A4, SECONDS=:A5 where ID=:A6", .fTableName )
	                   Dynamic updateResult = CAPI.EXEC(prgCtx.fDBConnect.fConnection, 
	                                           updateStmt, 
	                                           Date.Now(), 
	                                           issueMsg, 
	                                           statusStr, 
	                                           "N", 
	                                          seconds,
	                                           rec.ID )
	                   if IsError( updateResult )
	                     .LogErr( Str.Format( Str.Tab() + "Error updating %1 table.", .fTableName ) )
	                   end

	             // commit
	             prgCtx.fDbConnect.EndTrans( TRUE )

	           end


	private function Assoc _GetGroupsToRemove(Object prgCtx, DAPINODE node)

	             List removeList

	             String stmt = "select dta.RIGHTID, DTA.ACLTYPE, k.NAME from dtreeacl dta, kuaf k" +\
	                        " where dta.RIGHTID = k.ID" +\
	                        " and dta.DATAID = :A1" +\
	                        " and DTA.ACLTYPE = :A2" +\
	                        " and k.type = :A3" +\
	                        " and (k.name not like 'G-Dorado%' or k.name=:A4)"

	             RecArray recs = CAPI.Exec ( prgCtx.fDBConnect.fConnection, \
	                                     stmt, \
	                                     node.pID, \
	                                     DAPI.PERMTYPE_ACL, \
	                                     UAPI.GROUP, \
	                                     .fGlobalGroupName)

	             if IsError(recs)
	                   return .Problem(Str.Format('Could not get records to remove for DATAID %1', node.pID))
	             end

	             Record rec
	             for rec in recs
	                   Assoc changeVals = Assoc.CreateAssoc()
	                   changeVals.Type = .fUpdateRightRemove //$LLIApi.UpdateRightRemove
	                   changeVals.permType = rec.ACLTYPE
	                   changeVals.RightID = rec.RIGHTID
	                   changeVals.groupName = rec.NAME

	             removeList = {@removeList, Assoc.Copy(changeVals)}

	             end

	             return Assoc{'ok': true, 'result': removeList}                    

	           end


	private function Assoc _GetOwnerChange(Object prgCtx, DAPINODE node, String groupName, Integer permissions)

	           List    changeList
	           Integer ownerGroupID

	           if groupName != .fGlobalGroupName
	             Assoc status = .fCacheObject.GetGroupID(prgCtx, groupName)
	             if !status.ok
	                   return status
	             else
	                   ownerGroupID = status.result
	             end       
	           else       
	             ownerGroupID = .fGlobalGroupID
	             //return .Problem(Str.Format("Unknown OWNER_GROUP %1. This does not match the global group name %2 configured in the KINI table.", rec.OWNER_GROUP, .fGlobalGroupName))     
	           end     

	           Assoc changeVals = Assoc.CreateAssoc()
	           changeVals.Type = .fUpdateRightReplace // $LLIApi.updateRightReplace
	           changeVals.permType = DAPI.PERMTYPE_GROUP
	           changeVals.Permissions = permissions
	           changeVals.groupName = groupName
	           changeVals.RightID = ownerGroupID
	           changeList = {changeVals}

	           return Assoc{'ok': true, 'result': changeList}    

	      end


	private function Assoc _ParseNewAssignedGroups(Object prgCtx, String newAssignedGroupStr)

	           List    changeList
	           Assoc    changeVals
	           String    fFormattingErrMsg = "Invalid string in NEW_ASSIGNED_GROUPS column. Should be a comma-separated list of GROUPNAME|PERMISSIONS elements"

	           List    groupStrings = Str.Elements(newAssignedGroupStr, ',')

	           if IsUndefined(groupStrings) || Length(groupStrings) == 0
	             return .Problem(fFormattingErrMsg)     
	           end

	           String    groupStr

	           for groupStr in groupStrings

	             changeVals = Assoc.CreateAssoc()
	             changeVals.Type = .fUpdateRightAddReplace //$LLIApi.UpdateRightAdd      // resolves to 2, which means to Add to ACL
	             changeVals.permType = DAPI.PERMTYPE_ACL

	             List parts = Str.Elements(groupStr, '|')

	             if IsUndefined(parts) || Length(parts) != 2
	                   return .Problem(fFormattingErrMsg)
	             end

	             // get groupName       
	             String groupName = parts[1]
	             changeVals.groupName = groupName

	             // get the groupID
	             Assoc status = .fCacheObject.GetGroupID(prgCtx, groupName)
	             if !status.ok
	                   return status
	             else
	                   changeVals.RightID = status.result
	             end       

	             // permissions mask is in the second element 
	             changeVals.Permissions = Str.StringToInteger(parts[2])



	             // add to the changeList we will pass to llNode.NodeRightsUpdate     
	             changeList = {@changeList, Assoc.Copy(changeVals)}
	           end


	           return Assoc{'ok': true, 'result': changeList}    

	      end


	private function Assoc _ProcessChangeList(Object prgCtx, DAPINODE node, List changeList, Boolean changeModifyDate = false)

	           Assoc      change
	           Boolean        needNodeUpdate = false
	           Assoc      status    
	           Integer       oldOwnerGroupID      
	           Object    dbConnect = prgCtx.fDbConnect

	           String    dbMutexName = Str.Format( 'LLNode_NodeRightsUpdate_%1', node.pID )         

	           if ( !dbConnect.StartTrans( 0, dbMutexName ) )
	             return .Problem(Str.Format( [LLAPI_LLNodeErr.ErrorStartTransToChangePermsOf1], node.pName ))
	           end       

	           // Prepare permission data for update. This must be done inside a transaction to 
	           // prevent bad data from being injected in the DTreeACL table.

	           for change in changeList

	             // if the type is $LLIApi.updateRightNone then there is nothing to do
	             if ( change.type != $LLIApi.updateRightNone )

	                   switch( change.permType )

	                     case DAPI.PERMTYPE_GROUP

	                      String logStr

	                      if ( IsDefined( change.rightID ) && change.rightID != node.pGroupID )

	                        oldOwnerGroupID = node.pGroupID

	                        node.pGroupID = change.rightID
	                        logStr = Str.Format(Str.Tab() + Str.Tab() + 'Setting Owner Group to %1', change.groupName)
	                        needNodeUpdate = TRUE
	                      end

	                      if ( IsDefined( change.permissions ) && change.permissions != node.pGroupPerm )
	                        String s = Str.Format('Setting Owner Group perms to %1', change.permissions)
	                        logStr = Length(logStr) ? Str.Format('%1. %2', logStr, s) : Str.Tab() + Str.Tab() + s
	                        node.pGroupPerm = change.permissions
	                        needNodeUpdate = TRUE
	                      end      

	                      if Length(logStr)
	                        echoInfo(logStr)
	                      end

	                     end

	                     case DAPI.PERMTYPE_ACL
	                      if change.Type == .fUpdateRightAddReplace 
	                        status = .AddUpdateRight(prgCtx, node, change.groupName, change.rightID, change.permissions)
	                        if !status.ok
	                             dbConnect.EndTrans(false)
	                             return .Problem(status.errMsg)
	                        end
	                      elseif change.Type == .fUpdateRightRemove
	                        status = ._RemoveRight(prgCtx, node, change.groupName, change.rightID)
	                        if !status.ok
	                             dbConnect.EndTrans(false)
	                             return .Problem(status.errMsg)
	                        end                               
	                      else
	                        dbConnect.EndTrans(false)
	                        return .Problem(Str.Format('Invalid Change Type %1 passed to _ProcessChangeList', change.Type))
	                      end                        
	                     end

	                     default
	                       dbConnect.EndTrans(false)
	                       return .Problem(Str.Format('Invalid PermType %1 passed to _ProcessChangeList', change.permType))
	                     end
	                   end
	             end
	           end

	           if needNodeUpdate
	             if IsError(DAPI.UpdateNode( node, changeModifyDate ))

	                   // for some reason, DTREEACL may already be changed with new owner
	                   String stmt = "select 1 from DTREEACL where DataID=:A1 and RightID=:A2 and AclType=:A3"
	                   RecArray recs = CAPI.Exec (dbConnect.fConnection, stmt, node.pID, oldOwnerGroupID, 2)

	                   if Length(recs) > 0
	                     // OK, that's not the problem. roll back transaction and return the error
	                     dbConnect.EndTrans(false)
	                     return .Problem(Str.Format( [LLAPI_LLNodeErr.ErrorChangingPermsOf1], node.pName ))                 
	                   end       
	             end
	           end

	           if !dbConnect.EndTrans(true)
	             return .Problem(Str.Format( [LLAPI_LLNodeErr.ErrorEndTransToChangePermsOf1], node.pName ))
	           end           

	        return Assoc{'ok': true}

	      end


	private function Assoc _RemoveRight(Object prgCtx, DAPINODE node, String groupName, Integer groupID)
	             String    stmt = 'select RightID from DTreeACL where DataID=:A1 and RightID=:A2 and ACLType=:A3'
	             RecArray recs =  CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt, node.pId, groupID, DAPI.PERMTYPE_ACL)

	             if Length(recs) == 1
	                   if IsError(DAPI.DeleteNodeRight(node, groupID))
	                     return .Problem("Unable to remove node right %1 from node %2", groupID, node.pID)
	                   else
	                     EchoInfo(Str.Tab() + Str.Tab() + Str.Format("Removed Group %1 from ACL", groupName))
	                   end
	             end

	             return Assoc{'ok': true}
	           end


	private function Void _SaveExistingPerms( Object prgCtx, DAPINODE node )

	           //String stmt

	           prgCtx.fDBConnect.StartTrans()

	             // first delete any rows for this DATAID
	             CAPI.Exec ( prgCtx.fDBConnect.fConnection, "delete from BESS_SF_OLD_MODEL_PERMS where DATAID=:A1", node.pID )

	             CAPI.Exec ( 
	                   prgCtx.fDBConnect.fConnection, 
	                     "insert into BESS_SF_OLD_MODEL_PERMS (select sysdate, DATAID, RIGHTID, PERMISSIONS, ACLTYPE from DTREEACL where ACLTYPE in (0,2) and DATAID=:A1)", 
	                     node.pID )

	           prgCtx.fDBConnect.EndTrans( TRUE )


	      end

end
