package BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::UPDATE

public Object ModifyNodePermissions inherits BESSEMERAGENTS::TASKS::SALESFORCEMIGRATION::UPDATE::UpdatePermsTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'ModifyNodePermissions'
	override	String	fTableName = 'BESS_SF_MIGRATE_NODES'

end
