package BESSEMERAGENTS::TASKS

public Object FeedProcessor inherits BESSEMERAGENTS::TASKS::AgentTask

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'FeedProcessor'



	public function Assoc ProcessFeeds(Object prgCtx)

				Frame single = $Kernel.Mutex.New( "Bessemer::FeedProcessor" )
				Assoc result	

				if IsUndefined(single)
					return .Problem("cannot execute multiple Feed Processors")
				end	

				// get the rows in the table
				String stmt = "select * from BESSEMER_FRASC_Feed where CompleteDate is null order by Sequence"
				RecArray feeds = CAPI.Exec (prgCtx.fDBConnect.fConnection, stmt)
				if IsError (feeds)
					return .Problem("cannot access feed", feeds)
				end

				// iterate through the rows
				Integer successCount = 0
				Integer failureCount = 0
				Record feed	
				for feed in feeds
					String issueMessage = undefined

					// hand off to the row processor	
					result = ._ProcessRow(prgCtx, feed)
					if result.ok
						successCount += 1
					else
						failureCount += 1
						issueMessage = result.errMsg
					end	

					// update the feed table
					result = ._UpdateRow(prgCtx, feed, issueMessage)
					if !result.ok
						.LogErr(result.errMsg)
					end

				end	

				if Length(feeds)		
					.LogInfo(Str.Format ("successes: %1; failures: %2", successCount, failureCount))
				end	

				return .Success()

			end


	override function Void SubclassExecute( Object prgCtx, Date expireDate )

					.LogInfo("Bessemer Feed Processor starting")

					// hand off to the routine that will process the feeds
					Assoc result = .ProcessFeeds(prgCtx)
					if result.OK
						.LogInfo(Str.Format ("Bessemer Feed Processor ended"))
					else
						.LogErr(result.errMsg)	
						.LogErr(Str.Format ("Bessemer Feed Processor aborted"))
					end		

				end


	private function Assoc _ProcessRow(Object prgCtx, Record feed)

				.LogInfo(Str.Format(Str.Tab() + "Processing Row: %1", Str.String(feed)))

				// find the operation and call it				
				Object operation = $BESSEMERAGENTS.OperationRegistry.Get(feed.Operation, \
																		 feed.Family, \
																		 feed.Relationship, \
																		 feed.Account, \
																		 feed.Subaccount, \
																		 feed.Contact, \
																		 feed.NewFamily, \
																		 feed.NewRelationship, \
																		 feed.NewAccount, \
																		 feed.NewSubaccount, \
																		 feed.NewContact)

				if IsUnDefined(operation)
					return .Problem("Invalid feed table entry")
				end

					// KM 5-21-21 Should we manage the transaction inside the operation so that we can tolerate certain less important errors?
					prgCtx.fDBConnect.StartTrans()

						Assoc result = operation.Execute (prgCtx, \
														feed.Family, \
														feed.Relationship, \
														feed.Account, \
														feed.Subaccount, \
														feed.Contact, \
														feed.NewFamily, \
														feed.NewRelationship, \
														feed.NewAccount, \
														feed.NewSubaccount, \
														feed.NewContact)

					prgCtx.fDBConnect.EndTrans (result.OK)

					return result				

			end


	private function Assoc _UpdateRow(Object prgCtx, Record feed, String issueMessage)

				// update the row to mark it completed with result status
				prgCtx.fDBConnect.StartTrans()

					String stmt = "UPDATE BESSEMER_FRASC_Feed SET CompleteDate = :A1, IssueMessage = :A2 where Sequence = :A3"		

					Integer updated = CAPI.Exec (prgCtx.fDBConnect.fConnection, \
												stmt, \
												CAPI.Now (prgCtx.fDBConnect.fConnection), \
												issueMessage, 
												feed.Sequence)

					prgCtx.fDBConnect.EndTrans (not IsError (updated))
					if IsError (updated)
						return .Problem("Error updating feed table: %1", updated)
					end

				return .Success()	

			end

end
